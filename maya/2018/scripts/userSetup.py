# import module
import maya.cmds as mc
import sys
import maya.utils


BUILD = 'mayaTools'
sys.path.append(r'D:\True_Axion\Tools\{0}\python'.format(BUILD))
# import mayaEnviron as ENV



# Append path
# sys.path.append(r'{0}\\python'.format(ENV.PROJECT_PATH))
# Append bodyRig path ( cancle for now )
# sys.path.append(r'D:\True_Axion\Tools\riggerTools\python\axionTools\rigging\autoRig\bodyRig')

from axionMenu import axionMenu2018 as axm
reload(axm)


# for Zv
# import axionTools.animation 

'''
check to see if we re in batch or interactive mode. 
In interactive it runs maya.utils.executeDeferred  
and if we re in batch mode  it just executes the function
run menu
'''

maya.utils.executeDeferred('axm.runMenu()') 
print 'Create Axion menu...'


# install-link
# https://fredrikaverpil.github.io/2013/07/15/send-mel-python-code-from-sublime-text-to-maya/#:~:text=You%20can%20also%20just%20hit,been%20executed%20inside%20of%20Maya.
# Active port 7002 to recived message from Sublime to Maya

try:
	mc.commandPort(name=":7002", sourceType="python")
except :
	mc.warning('Could not active port 7005 (maybe it already opened.)')

