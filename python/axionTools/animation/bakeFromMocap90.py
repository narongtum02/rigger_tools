import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om

class function:

	def timeLine(self,*args):
		# TimeLine Query 
		startTime = mc.playbackOptions(min = True,q = True)
		endTime = mc.playbackOptions(max = True,q = True)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)
		return [ startTime , endTime ]

	def keyTime(self,*args):
		# Hips.tx Query
		minKey = mc.keyframe('Hips.tx',index=(0,0),query=True)
		maxKey =  mc.keyframe('Hips.tx',iv = True,query=True)
		startTime = minKey[0]
		endTime = startTime + (len(maxKey)-1)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)    
		return [ startTime , endTime ]


	def bakeMocap(self,*args):
		print "Bake it Bro "
		startText =  mc.textField('startTexFld', tx = True, q = True)
		endText = mc.textField('endTexFld', tx = True, q = True)
		mc.playbackOptions(min = startText)
		mc.playbackOptions(max = endText)
		
		sel = mc.ls(sl=True)
		giveName = ['footLFT_FK_ctrl', 'footRGT_FK_ctrl', 'lowerLegRGT_FK_ctrl', 'lowerLegLFT_FK_ctrl', 'upperLegLFT_FK_ctrl', 'upperLegRGT_FK_ctrl', 'spine_ctrl', 'chest_ctrl', 'cog_ctrl', 'upperChest_ctrl', 'upperArmLFT_FK_ctrl', 'lowerArmLFT_FK_ctrl', 'handLFT_FK_ctrl', 'upperArmRGT_FK_ctrl', 'lowerArmRGT_FK_ctrl', 'handRGT_FK_ctrl', 'head_ctrl', 'shoulderRGT_FK_ctrl', 'shoulderLFT_FK_ctrl', 'neck_ctrl']
		giveNameParent = [ 'cog_ctrl','legRGT_pov_ctrl','legLFT_pov_ctrl','footRGT_IK_ctrl','footLFT_IK_ctrl' ]
		parentCtrl = []
		rotCtrl = []
		
		if len(sel) > 0:
			splitName = sel[0].split(':')
			ns = splitName[0] + ':'
		else:
			ns = 'Male_rig_hero:'
			#print '>>>> Pls Select Reference Control <<<<'
		for i in range (len(giveName)):
			name = giveName[i]
			newName = ns + name
			rotCtrl.append(newName)
			#print rotCtrl[i]
		for i in range (len(giveNameParent)):
			name = giveNameParent[i]
			newName = ns + name
			parentCtrl.append(newName)
			#print posCtrl[i]
			
		time = ( startText , endText )

		# Grp and Rotate >>> BAKE  <<<
		mc.group(em = True, n = 'Mocap_grp')
		mc.parent('Hips','Mocap_grp')
		mc.setAttr('Mocap_grp.ry',-90)
		
		mc.bakeResults(rotCtrl, simulation = True, t= time, at=["rx","ry","rz"])
		mc.bakeResults(parentCtrl, simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])
		
		mc.delete('*_mdv')  
		mc.delete('Mocap_grp')
	
class Main:
	'''
	Creates the script UI
	'''
	def __init__(self):
		self.pre = function()

	def bakeGUI(self,*args):
		# Make a new window
		if mc.window('bakeWin', exists = True):
			mc.deleteUI('bakeWin')
		
			
		dWin = mc.window('bakeWin', title="Bake Mocap Animation [-90]", iconName='bkeMo', widthHeight=(300, 200), s = False, mm = False, mxb = False, mw = False )
		
		mc.frameLayout( label='Bake Options',collapsable=False, mw=5, mh=5 )
		#mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150), (2, 150)] )
		mc.button( label='Get Time' , command = self.pre.keyTime,h=50)
		mc.button( label='Timeline' , command = self.pre.timeLine,h=50)
		
		
		#mc.rowColumnLayout( numberOfColumns=4, columnAttach=(1, 'right', 0), columnWidth=[(1, 50), (1, 50)] )
		mc.text( label='Start :' )
		mc.textField('startTexFld', tx = '' , h = 30)
		mc.text( label='End :' )
		mc.textField('endTexFld', tx = '', h = 30 )
				   
		mc.button( label='Bake', command = self.pre.bakeMocap ,w=300, h=50 )
		# time = self.pre.timeLine
		# startTime = time[0]
		# endTime = time[1]
		
		mc.showWindow( dWin )
	
# bakeGUI()

# time = timeLine ( )
# startTime = time[0]
# endTime = time[1]

# bake = Main()
# bake.bakeGUI()
