# >>>>>   Untimate Export Button   <<<<<
# import Object from reference ****
# Delete Name space of Male.Rig ****
# Select *_bind_jnt and _prop_jnt and root bake translate and rotate
# Delete Rig
# Select Root and Export
import pymel.core as pm
import maya.cmds as mc
versions = '3.15'
modify_date = '2020.8.24'

class function:

	def timeLine(self,*args):
		# TimeLine Query 
		startTime = mc.playbackOptions(min = True,q = True)
		endTime = mc.playbackOptions(max = True,q = True)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)
		return [ startTime , endTime ]
	
	def keyTime(self,*args):
		# Hips.tx Query
		minKey = mc.keyframe('cog_ctrl.tx',index=(0,0),query=True)
		maxKey =  mc.keyframe('cog_ctrl.tx',iv = True,query=True)
		startTime = minKey[0]
		endTime = startTime + (len(maxKey)-1)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)    
		return [ startTime , endTime ]
	
	def importChar(self,*args):
	# IMPORT REFERENCE
		allrefs = pm.getReferences(recursive = True )
		for ref in allrefs:
			try:
				allrefs[ref].importContents( removeNamespace = True )
			except RuntimeError:
				pass
		print '\nImport and clear namespace ...'

	'''
		# IMPORT REFERENCE
		allCH = ['bob01','fifinn01','goldwomansax01','hamlu01','lucky01','pingapin01','pipitt01','rawl01','tigus01']
		allPet = ['pet_bear01','pet_beaver01', 'bison' ,'pet_cutecat01','pet_dangerousdragon01', 'fennec_fox', 'pet_loyalredfox01' , 'gerenuk', 'pet_husky01' ,'pet_jackalope01', 'tamarinmonkey' ,'octopus','pomeranian01', 'panda', 'aggressivecrestedpenguin', 'pet_pomeranian01' ,'pet_quokka01','pet_salukidog01','sloth','tamarinmonkey','pet_vogelkop01','pet_wobblycat01']
		RN = []
		refs = mc.ls(type='reference')
		if 'inGameCameraRN' in refs:
			RN.append('inGameCameraRN')
		if 'male_rig_heroRN' in refs:
			RN.append('male_rig_heroRN')
		if 'male_rig_heroRN1' in refs:
			RN.append('male_rig_heroRN1')

		for c in range(len(allCH)): # Check Charecter Name
			name = 'npc_' + allCH[c] + '_rig_heroRN'
			if name in refs:
				RN.append(name)

		for p in range(len(allPet)): # Check Pet Name
			name = allPet[p] + '_rig_heroRN'
			if name in refs:
				RN.append(name)

		for i in range(len(RN)):
			rFile = mc.referenceQuery(RN[i], f=True)
			mc.file(rFile, importReference=True)

		# FIND EXISTING OBJECT and Delete Name space
		mc.namespace(setNamespace=':')
		ns = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
		if 'UI' in ns:
			ns.remove('UI')
		if 'shared' in ns:
			ns.remove('shared')
		for i in range(len(ns)):
			print i
			mc.namespace(rm = ns[i],mergeNamespaceWithParent = True, force = True)

		# scene cleaner
		if mc.checkBox("sceneCleaner", value = True , q = True):
			if mc.objExists ( 'Root1' ):
				mc.delete ( 'Root1' )
			if mc.objExists ( 'Root2' ):
				mc.delete ( 'Root2' )
			if mc.objExists ( 'Root3' ):
				mc.delete ( 'Root3' )
			if mc.objExists ( 'rig_grp1' ):
				mc.delete ( 'rig_grp1' )
			if mc.objExists ( 'group1' ):
				mc.delete ( 'group1' )
			if mc.objExists ( 'group' ):
				mc.delete ( 'group' )
	'''
	def bakeAnim(self,*args):
		
		# Get Time From outside
		startText =  mc.textField('startTexFld', tx = True, q = True)
		endText = mc.textField('endTexFld', tx = True, q = True)
		mc.playbackOptions(min = startText)
		mc.playbackOptions(max = endText)
		time = ( startText , endText )
		
		# scene cleaner
		if mc.checkBox("sceneCleaner", value = True , q = True):
			if mc.objExists ( 'Root1' ):
				mc.delete ( 'Root1' )
			if mc.objExists ( 'Root2' ):
				mc.delete ( 'Root2' )
			if mc.objExists ( 'Root3' ):
				mc.delete ( 'Root3' )
			if mc.objExists ( 'rig_grp1' ):
				mc.delete ( 'rig_grp1' )
			if mc.objExists ( 'group1' ):
				mc.delete ( 'group1' )
			if mc.objExists ( 'group' ):
				mc.delete ( 'group' )

		if mc.objExists( 'Root' ):
			mc.setAttr( 'Root.v', 1)
			bakeJnt = mc.ls('*_bind_jnt','Root','*_prop_jnt') # call _jnt
		elif mc.objExists( 'root' ):
			mc.setAttr( 'root.v', 1)
			bakeJnt = mc.ls('*_bJnt','root','*_bJnt') # call _jnt
		else:
			mc.warning('There are Root or root in the scene.')

		
		# Baking Function
		if mc.checkBox("bakeScaleCheck", value = True , q = True):
			bakeAttrs = ["tx","ty","tz","rx","ry","rz","sx","sy","sz"]

		elif mc.checkBox("bakeScaleCheck", value = True , q = False):
			bakeAttrs = ["tx","ty","tz","rx","ry","rz"]

		print bakeAttrs 

		mc.bakeResults(bakeJnt, simulation = True, t= time, at=bakeAttrs)
		pomBsh = 'pomLFT_bsh','pomRGT_bsh','pomLFT','pomRGT'
		sel =  mc.ls(dag=1)
		if 'pomLFT' in sel:
			print True
			mc.bakeResults(pomBsh, simulation = True, t= time, sac= True)
		# Delete Rig GRP
		mc.delete('rig_grp')

class Ui:
	def __init__(self):
		self.function = function()

	def createGUI(self,*args):
		# Make a new window
		if mc.window('bakeEH', exists = True):
			mc.deleteUI('bakeEH')
		
			
		dWin = mc.window('bakeEH', title="Bake Joint Animation EH", iconName='bakeEH', widthHeight=(300, 200), s = 1, mm = 1, mxb = 0, mw = 0 )
		
		mc.frameLayout( label='EH bake animation v. {0}' .format(versions) , collapsable=False, mw=5, mh=5 )
		#mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150), (2, 150)] )
		mc.button( label='GetTime' , command = self.function.timeLine,h=50)
		mc.button( label='Timeline' , command = self.function.timeLine,h=50)
		
		
		#mc.rowColumnLayout( numberOfColumns=4, columnAttach=(1, 'right', 0), columnWidth=[(1, 50), (1, 50)] )
		mc.text( label='Start :' )
		mc.textField('startTexFld', tx = '' , h = 30)
		mc.text( label='End :' )
		mc.textField('endTexFld', tx = '', h = 30 )

		mc.checkBox( "sceneCleaner", label=' Use Scene Cleaner', align='Center', value = 1,  h = 20, ann = 'I can only clean something' )
		mc.checkBox( "bakeScaleCheck", label='Bake Scale', align='Center', value = 1 ,  h = 20 )

		
		#mc.text( label= "  " )
		



		mc.button( label='Import Reference', command = self.function.importChar ,w=50, h=50 )

		mc.button( label='Bake', command = self.function.bakeAnim ,w=50, h=50 )  
		
		mc.showWindow( dWin )

		
#local run	
call = Ui()
call.createGUI()