# This can use for any reference weapon
import pymel.core as pm
import maya.cmds as mc
#import maya.OpenMaya as om
class function:

	def weaponList(self,*args):
		# FIND EXISTING OBJECT
		all = mc.ls(dag=True)
		weaponJnt = mc.ls('*:weapon_jnt')
		if weaponJnt == None:
			print 'there are no weapon'
		# Function
		if weaponJnt[0] in all: # FIND is that Great , Sword or SAS
			giveWeapon_ns = weaponJnt[0].split(':')
			ns = giveWeapon_ns[0] + ':'
			mc.select(ns + 'weapon_jnt')        
			mc.pickWalk(d='down')
			model = mc.ls(sl=True)
			splitName = model[0].split('_')
			namePos = len(splitName)-1
			modelName = splitName[namePos] # USE THIS TO COMPARE
			print modelName
			# TO COMPARE
			if modelName == 'great':
				kind = '_great'
				print 'A'
			elif modelName == 'sword':
				kind = '_sword'
				print 'B'
			elif modelName == 'R':
				kind = '_swordShield_R'
				print 'C'
			else:
				print 'No Weapon'
				
			mc.select(cl=True)  
			
			if (ns + 'shield_jnt') in all:
				kindShd = '_swordShield_L'
			else:
				kindShd = kind

	def hideWeapon(self,*args):
		giveWeapon = ['m_0010_valiant_var0_1','m_0010_valiant_var1_1','m_0020_dawnguard_var0_1','m_0020_dawnguard_var1_1','m_0030_horkbane_var0_1','m_0030_horkbane_var1_1','m_0030_horkbane_var2_1','m_0040_talon_var0_1','m_0040_talon_var1_1','m_0050_ethereal_var0_1','m_0050_ethereal_var1_1','m_0060_cyber_var0_1']
		for i in range(len(giveWeapon)):
			weapon = ns + giveWeapon[i] + kind
			try:
				mc.setAttr(weapon + '.v',0)
			
				if (ns + 'shield_jnt') in all:
					kindShd = '_swordShield_L'
					shield = ns + giveWeapon[i] + kindShd
					mc.setAttr(shield + '.v',0)
				else:
					kindShd = weapon
			except:
				print 'There are no weapon'
class Main:
	def __init__( self ):
		self.func = function()

	def weaponGUI(self,*args):
		# Make a new window
		if mc.window('WeaponWin', exists = True):
			mc.deleteUI('WeaponWin')
		weWin = mc.window('WeaponWin', title="Show Me Weapon", iconName='bkeMo', widthHeight=(50, 100), s = True, mm = False, mxb = False, mw = False )
		
		mc.frameLayout( label='Show me Weapon',collapsable=False, mw=5, mh=5 )
		#mc.columnLayout( adjustableColumn=True )
		#mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150), (2, 150)] )
		mc.button( label='valiant_var0',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0010_valiant_var0_1' + kind + '.v',1),mc.setAttr(ns + 'm_0010_valiant_var0_1' + kindShd + '.v',1), mc.select(ns + 'm_0010_valiant_var0_1' + kind), mc.select(ns + 'm_0010_valiant_var0_1' + kindShd), mc.select(cl=True) ")
		mc.button( label='valiant_var1',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0010_valiant_var1_1' + kind + '.v',1),mc.setAttr(ns + 'm_0010_valiant_var1_1' + kindShd + '.v',1), mc.select(ns + 'm_0010_valiant_var1_1' + kind), mc.select(ns + 'm_0010_valiant_var1_1' + kindShd), mc.select(cl=True) ")
		
		mc.button( label='dawnguard_var0',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0020_dawnguard_var0_1' + kind + '.v',1),mc.setAttr(ns + 'm_0020_dawnguard_var0_1' + kindShd + '.v',1), mc.select(ns + 'm_0020_dawnguard_var0_1' + kind), mc.select(ns + 'm_0020_dawnguard_var0_1' + kindShd), mc.select(cl=True) ")
		mc.button( label='dawnguard_var1',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0020_dawnguard_var1_1' + kind + '.v',1),mc.setAttr(ns + 'm_0020_dawnguard_var1_1' + kindShd + '.v',1), mc.select(ns + 'm_0020_dawnguard_var1_1' + kind), mc.select(ns + 'm_0020_dawnguard_var1_1' + kindShd), mc.select(cl=True) ")
		
		mc.button( label='horkbane_var0',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0030_horkbane_var0_1' + kind + '.v',1),mc.setAttr(ns + 'm_0030_horkbane_var0_1' + kindShd + '.v',1), mc.select(ns + 'm_0030_horkbane_var0_1' + kind), mc.select(ns + 'm_0030_horkbane_var0_1' + kindShd), mc.select(cl=True) ")
		mc.button( label='horkbane_var1',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0030_horkbane_var1_1' + kind + '.v',1),mc.setAttr(ns + 'm_0030_horkbane_var1_1' + kindShd + '.v',1), mc.select(ns + 'm_0030_horkbane_var1_1' + kind), mc.select(ns + 'm_0030_horkbane_var1_1' + kindShd), mc.select(cl=True) ")
		mc.button( label='horkbane_var2',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0030_horkbane_var2_1' + kind + '.v',1),mc.setAttr(ns + 'm_0030_horkbane_var2_1' + kindShd + '.v',1), mc.select(ns + 'm_0030_horkbane_var2_1' + kind), mc.select(ns + 'm_0030_horkbane_var2_1' + kindShd), mc.select(cl=True) ")
		
		mc.button( label='talon_var0',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0040_talon_var0_1' + kind + '.v',1),mc.setAttr(ns + 'm_0040_talon_var0_1' + kindShd + '.v',1), mc.select(ns + 'm_0040_talon_var0_1' + kind), mc.select(ns + 'm_0040_talon_var0_1' + kindShd), mc.select(cl=True) ")
		mc.button( label='talon_var1',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0040_talon_var1_1' + kind + '.v',1),mc.setAttr(ns + 'm_0040_talon_var1_1' + kindShd + '.v',1), mc.select(ns + 'm_0040_talon_var1_1' + kind), mc.select(ns + 'm_0040_talon_var1_1' + kindShd), mc.select(cl=True) ") 
		
		mc.button( label='ethereal_var0',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0050_ethereal_var0_1' + kind + '.v',1),mc.setAttr(ns + 'm_0050_ethereal_var0_1' + kindShd + '.v',1), mc.select(ns + 'm_0050_ethereal_var0_1' + kind), mc.select(ns + 'm_0050_ethereal_var0_1' + kindShd), mc.select(cl=True) ")
		mc.button( label='ethereal_var1',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0050_ethereal_var1_1' + kind + '.v',1),mc.setAttr(ns + 'm_0050_ethereal_var1_1' + kindShd + '.v',1), mc.select(ns + 'm_0050_ethereal_var1_1' + kind), mc.select(ns + 'm_0050_ethereal_var1_1' + kindShd), mc.select(cl=True) ")       
		
		mc.button( label='cyber_var0',command = "self.func.hideWeapon(),mc.setAttr(ns + 'm_0060_cyber_var0_1' + kind + '.v',1),mc.setAttr(ns + 'm_0060_cyber_var0_1' + kindShd + '.v',1), mc.select(ns + 'm_0060_cyber_var0_1' + kind), mc.select(ns + 'm_0060_cyber_var0_1' + kindShd), mc.select(cl=True) ")
		mc.showWindow( weWin )
		
	#weaponGUI()

# show = Main()
# show.weaponGUI()

# from axionTools.animation import showMeWeapon as smw
# reload(smw)


# show = smw.Main()
# show.weaponGUI()
