# >>>>>   Untimate Export Button   <<<<<
# import Object from reference ****
# Delete Name space of Male.Rig ****
# Select *_bind_jnt and _prop_jnt and root bake translate and rotate
# Delete Rig
# Select Root and Export

import maya.cmds as mc

class function:

	def timeLine(self,*args):
		# TimeLine Query
 		startTime = mc.playbackOptions(min = True,q = True)
		endTime = mc.playbackOptions(max = True,q = True)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)
		return [ startTime , endTime ]
	
	def keyTime(self,*args):
		# Hips.tx Query
		minKey = mc.keyframe('cog_ctrl.tx',index=(0,0),query=True)
		maxKey =  mc.keyframe('cog_ctrl.tx',iv = True,query=True)
		startTime = minKey[0]
		endTime = startTime + (len(maxKey)-1)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)    
		return [ startTime , endTime ]
	
	def importChar(self,*args):
		# IMPORT REFERENCE
		allCH = ['bob01','fifinn01','goldwomansax01','hamlu01','lucky01','pingapin01','pipitt01','rawl01','tigus01']
		allPet = ['aggressivecrestedpenguin01','bear01','beaver01','cutecat01','dangerousdragon01','gerenuk01','jackalope01','loyalredfox01','octopus01','pomeranian01','quokka01','salukidog01','sloth01','tamarinmonkey01','vogelkop01','wobblycat01']
		RN = []
		refs = mc.ls(type='reference')
		if 'inGameCameraRN' in refs:
			RN.append('inGameCameraRN')
		if 'male_rig_heroRN' in refs:
			RN.append('male_rig_heroRN')
		if 'Awin_mother_rigRN' in refs:
			RN.append('Awin_mother_rigRN')
		for c in range(len(allCH)): # Check Charecter Name
			name = 'npc_' + allCH[c] + '_rig_heroRN'
			if name in refs:
				RN.append(name)
		for p in range(len(allPet)): # Check Pet Name
			name = 'pet_' + allPet[p] + '_rig_heroRN'
			if name in refs:
				RN.append(name)
		for i in range(len(RN)):
			rFile = mc.referenceQuery(RN[i], f=True)
			mc.file(rFile, importReference=True)

		# FIND EXISTING OBJECT and Delete Name space
		mc.namespace(setNamespace=':')
		ns = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
		if 'UI' in ns:
			ns.remove('UI')
		if 'shared' in ns:
			ns.remove('shared')
		for i in range(len(ns)):
			print i
			mc.namespace(rm = ns[i],mergeNamespaceWithParent = True, force = True)
	
	def bakeAnim(self,*args):
		
		# Get Time From outside
		startText =  mc.textField('startTexFld', tx = True, q = True)
		endText = mc.textField('endTexFld', tx = True, q = True)
		mc.playbackOptions(min = startText)
		mc.playbackOptions(max = endText)
		time = ( startText , endText )
		
		# Unhide Root 
		mc.setAttr( 'root.v', 1)
		
		# Baking Function
		bakeJnt = mc.ls('*_bJnt','root') # call _jnt
		mc.bakeResults(bakeJnt, simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])

		handJnt = ( 'handLFT_bJnt','handRGT_bJnt')
		mc.bakeResults(handJnt, simulation = True, t= time, at=["sx","sy","sz"])
		pomBsh = 'pomLFT_bsh','pomRGT_bsh','pomLFT','pomRGT'
		sel =  mc.ls(dag=1)
		if 'pomLFT' in sel:
			print True
			mc.bakeResults(pomBsh, simulation = True, t= time, sac= True)
		# Delete Rig GRP
		mc.delete('rig_grp')

class Ui:
	def __init__(self):
		self.function = function()

	def createGUI(self,*args):
		# Make a new window
		if mc.window('bakeEH', exists = True):
			mc.deleteUI('bakeEH')
		
			
		dWin = mc.window('bakeEH', title="Bake Joint Animation SS", iconName='bakeSS', widthHeight=(300, 200), s = 1, mm = 1, mxb = 0, mw = 0 )
		
		mc.frameLayout( label='Bake Options',collapsable=False, mw=5, mh=5 )
		#mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150), (2, 150)] )
		mc.button( label='GetTime' , command = self.function.timeLine,h=50)
		mc.button( label='Timeline' , command = self.function.timeLine,h=50)
		
		
		#mc.rowColumnLayout( numberOfColumns=4, columnAttach=(1, 'right', 0), columnWidth=[(1, 50), (1, 50)] )
		mc.text( label='Start :' )
		mc.textField('startTexFld', tx = '' , h = 30)
		mc.text( label='End :' )
		mc.textField('endTexFld', tx = '', h = 30 )
		
		mc.button( label='Import Reference', command = self.function.importChar ,w=50, h=50 )
		mc.button( label='Bake', command = self.function.bakeAnim ,w=50, h=50 )  
		
		mc.showWindow( dWin )
		
#local run	
#call = Ui()
#call.createGUI()