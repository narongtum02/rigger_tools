#     __________________
# - -/__ Update __/- - - - - - - - - - - - - - - - - - - - - - - - - - - 
# 
# Regulus Animation Exporter
#
#
# Add bake specific ply for switch visibility

# import sys
# sys.path.append(r'D:\True_Axion\Tools\mayaTools\python')

import pymel.core as pm
import maya.mel as mel
import maya.cmds as mc
import os

from axionTools.framework.reloadWrapper import reloadWrapper as reload

from axionTools.pipeline import fileTools as fileTools 
reload(fileTools)

from axionTools.pipeline import data_dict as data
reload(data)

# import socket
# localMachine = socket.gethostname()

from axionTools.pipeline import logger 
reload(logger)

PROJECT_NAME = 'Regulus'
version = 1.3

# 1. group 'geo_grp' collect all of the skin
# 2. except than that if have '*_ply' bake the key

facial_bsh = 'facial_bsh'
export_grp = 'Export_grp'
facialOffset_jnt = 'facialOffset_jnt'
facialOffset_attr = 'sx'
polyMeshes_to_bake_suffix = '_ply'

class ExportLogger(logger.MayaLogger):
	LOGGER_NAME = PROJECT_NAME

class function:

	def setFirstJnt(self,*args):
		'''
		Set the selected object as the "export joint"
		for the animation.

		Called by function: Ui.CreateGui
		Call function: -
		Return: -
		'''
		rawSelection = mc.ls(sl=True)
		if len(rawSelection) == 0:
			mc.textFieldButtonGrp("btnFirstJnt",e=True, tx="")
			mc.error("Please select the first joint of the chain.")
		else:
			firstJnt = mc.ls(sl=True, type = 'joint')
			if len(firstJnt) == 0:
				mc.error("You have to select the root joint fo the chain.")
			else:
				mc.textFieldButtonGrp("btnFirstJnt", e=True, tx=firstJnt[0])


	def getTimeLine(self,*args):
		# TimeLine Query 
		startTime = mc.playbackOptions( min = True,q = True )
		endTime = mc.playbackOptions( max = True,q = True )



		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)
		return (startTime, endTime) 


	def setTimeLine(self,*args):
		# Set The Query Time
		getStartTime = mc.textField('startTexFld', tx = True, q = True)
		getEndTime = mc.textField('endTexFld', tx = True, q = True)

		try:
			getStartTime = float(getStartTime)
			getEndTime = float(getEndTime)
		except:
			mc.error('Invalid literal for float.')
				
		# print(	"{0}{1}".format('Data type is: ',type(startTime))	)
		ExportLogger.debug("{0}{1}".format('Data type is: ',type(getStartTime)))



		mc.playbackOptions(min = getStartTime,max = getEndTime)
		


	def openContainFile( self , *args ):
		FOLDER_PATH = mc.textField( 'pathField', tx = True, q = True)
		# MovingJellyLogger.info(FOLDER_PATH)

		# folder = fileTools.currentBackFolder()
		# folder += r'preview\\'
		if os.path.exists( FOLDER_PATH ):
			fileTools.openContainerFile( path = FOLDER_PATH )
		else:
			CoreLogger.info('file not exists.')
			pass
			# MovingJellyLogger.info('The file does not exist.')



	def importRef(self,*args):
		allrefs = pm.getReferences(recursive = True )
		for ref in allrefs:
			try:
				allrefs[ref].importContents( removeNamespace = True )
			except RuntimeError:
				print ("\nCan't Import ...")
				pass
		print ('\nImport and clear namespace ...')



	def bakeAnim(self,*args):


		# Get Time From outside
		startText =  mc.textField('startTexFld', tx = True, q = True)
		endText = mc.textField('endTexFld', tx = True, q = True)

		time = ( startText , endText )

		# BAKE VIS ON Meshes ------------------------------------ [1]
		bake_mesh = mc.ls( '*{0}'.format( polyMeshes_to_bake_suffix ) )

		if bake_mesh:
			bakeAttrs = ['v']
			mc.bakeResults(bake_mesh, simulation = True, t= time, disableImplicitControl = True, preserveOutsideKeys = True, at=bakeAttrs)
	
		else:
			ExportLogger.debug('There are no _ply for visibility.')


		# BAKE Joint ------------------------------------ [2]

		if mc.objExists('root'):
			root = 'root'
		elif mc.objExists( 'Root' ):
			root = 'Root'
		else:
			mc.warning("There are 'Root' or 'root' in the scene, Please consult Rigger.")

		# Quarry bake joint
		bakeJnt = mc.ls('*_bJnt','*_prop_jnt', '*_jnt', '*_pJnt') # call _jnt
		bakeJnt.append(root)
		mc.setAttr( '{0}.v'.format( root ), 1)



		# append root_weapon 
		if mc.objExists( 'root_weapon' ):
			bakeJnt.append( 'root_weapon' )
		elif mc.objExists( 'root_sword' ):
			bakeJnt.append( 'root_sword' )

		
		bakeAttrs = ["tx","ty","tz","rx","ry","rz","sx","sy","sz"]
		print (bakeAttrs) 

		#... Try to use preserveOutsideKeys
		mc.bakeResults(bakeJnt, preserveOutsideKeys = True, simulation = True, t= time, at=bakeAttrs)


		# BAKE facial blendshape ------------------------------------ [3]
		if mc.objExists( facial_bsh ):
			# bake all chanel in facial_bsh
			mc.bakeResults(facial_bsh, preserveOutsideKeys = True, simulation = True, t= time )

		# END ANIM BAKE ------






	def noWeapon(self,*args):

		if mc.objExists( 'root_weapon' ):
			mc.parent( 'root_weapon', w = True )
		elif mc.objExists( 'root_sword' ):
			mc.parent( 'root_sword', w = True )



	def getPath(self,*args):
		# Get path
		path = fileTools.findCurrentPath()
		path = path.replace('\\','/')
		return path


	def exportFBX(self,*args):

		
		# get path from field
		path = mc.textField( 'pathField', tx = True, q = True)

		# get current
		# path = self.getPath()

		# get file name from field
		fileName = mc.textField( 'nameField', tx = True, q = True)

		# fileName = self.getSceneName()
		path = path + '\\'
		# MovingJellyLogger.info(path)
		# MovingJellyLogger.info(fileName)

		# # condition of jelly visibility
		# try:
		# 	mc.select( '*_ply', r = True )
		# except:
		# 	ExportLogger.info('There are no suffix {0}'.format("*_ply"))
				
		mc.select( export_grp, add = True)


		# Set time length
		ExportLogger.debug('Set the time length: {0}'.format(path))
		setTime = function()
		setTime.setTimeLine()


		#... reset Take001 to file name
		clipName = mc.textFieldButtonGrp( 'animClip', tx = True, q = True )
		startTime = mc.textField( 'startTexFld', tx = True, q = True )
		endTime = mc.textField( 'endTexFld', tx = True, q = True )
		
		cleanUpExporterCommand = 'FBXExportSplitAnimationIntoTakes -c'
		clearExporterCommand = 'FBXExportDeleteOriginalTakeOnSplitAnimation -v true'
		createExportClipCommand = ' FBXExportSplitAnimationIntoTakes -v "{0}" {1} {2}'.format( clipName, startTime, endTime ) 
		
		mel.eval( cleanUpExporterCommand )
		mel.eval( clearExporterCommand )
		mel.eval( createExportClipCommand )
		
		# mel.eval('FBXExportUseSceneName -v true')


		# Export obj
		exportCommand = 'file -force -options "v=0;" -typ "FBX export" -pr -es '
		path = path.replace('\\','/')
		# MovingJellyLogger.info('This path result%s ' %path)

		exportFBXPath = r'"'+ path + fileName + '.fbx' '"'
		exportCommand += exportFBXPath

		# MovingJellyLogger.info('This Export result%s' %exportCommand)

		# Exec
		mel.eval( exportCommand )
		print ('>>> %s has been export.'%fileName)
		ExportLogger.debug('This is export command: {0}'.format(exportCommand))
		fileTools.openContainerFile( path = path )
		# Deselect
		mc.select(deselect = True)

		print ('# # # %s Export Complete # # #' %PROJECT_NAME)



	def exportSelectionFBX(self,*args):


			
		# get path from field
		path = mc.textField( 'pathField', tx = True, q = True)

		# get current
		# path = self.getPath()

		# get file name from field
		fileName = mc.textField( 'nameField', tx = True, q = True)

		# fileName = self.getSceneName()
		path = path + '\\'
		# MovingJellyLogger.info(path)
		# MovingJellyLogger.info(fileName)

		# # condition of jelly visibility
		# try:
		# 	mc.select( '*_ply', r = True )
		# except:
		# 	ExportLogger.info('There are no suffix {0}'.format("*_ply"))
				
		# mc.select( export_grp, add = True)


		# Set time length
		ExportLogger.debug('Set the time length: {0}'.format(path))
		setTime = function()
		setTime.setTimeLine()


		#... reset Take001 to file name
		clipName = mc.textFieldButtonGrp( 'animClip', tx = True, q = True )
		startTime = mc.textField( 'startTexFld', tx = True, q = True )
		endTime = mc.textField( 'endTexFld', tx = True, q = True )
		
		cleanUpExporterCommand = 'FBXExportSplitAnimationIntoTakes -c'
		clearExporterCommand = 'FBXExportDeleteOriginalTakeOnSplitAnimation -v true'
		createExportClipCommand = ' FBXExportSplitAnimationIntoTakes -v "{0}" {1} {2}'.format( clipName, startTime, endTime ) 
		
		mel.eval( cleanUpExporterCommand )
		mel.eval( clearExporterCommand )
		mel.eval( createExportClipCommand )
		
		# mel.eval('FBXExportUseSceneName -v true')


		# Export obj
		exportCommand = 'file -force -options "v=0;" -typ "FBX export" -pr -es '
		path = path.replace('\\','/')
		# MovingJellyLogger.info('This path result%s ' %path)

		exportFBXPath = r'"'+ path + fileName + '.fbx' '"'
		exportCommand += exportFBXPath

		# MovingJellyLogger.info('This Export result%s' %exportCommand)

		# Exec
		mel.eval( exportCommand )
		print ('>>> %s has been export.'%fileName)
		ExportLogger.debug('This is export command: {0}'.format(exportCommand))
		fileTools.openContainerFile( path = path )
		# Deselect
		mc.select(deselect = True)

		print ('# # # %s Export Complete # # #' %PROJECT_NAME)






class Ui:
	def __init__(self):
		self.function = function()
	
	def createGUI(self,*args):


		
		# Find folder and important dir
		# partFileName = mc.file( q = True , sn = True )
		scene_anim = fileTools.Scene()
		scene_anim.get_scene_name()
		partFileName = scene_anim.sceneNameFullPath

		splitfileName = partFileName.split('/')
		preName = splitfileName[ len(splitfileName)-1 ]

		# find maya extention
		MayaExt = preName.split('.')[-1]
		
		# name = preName.split( MayaExt )[0]
		name = scene_anim.sceneNameShort_noExt



		path = fileTools.findCurrentPath()
		path = path.replace('\\','/')

			
		'''
		path += r'\\preview\\'
		'''

		# normalize path
		path = os.path.normpath( path )

		# MovingJellyLogger.warning('This is funtion %s.' %path)
		
		
		# Make a new window
		if mc.window('pbWin', exists = True):
			mc.deleteUI('pbWin')
	
		dWin = mc.window('pbWin', title="{0} Anim Export {1}".format(PROJECT_NAME,version) , iconName ='PB', widthHeight=(300, 200), s = 1, mm = 0, mxb = 0, mw = False )
		
		mc.frameLayout( label='Export Options',collapsable=False, mw=5, mh=5 )
		mc.columnLayout( adjustableColumn=True )
		
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150),(2, 150)])
		
		mc.text( label='Start :', h = 20)
		mc.text( label='End :', h = 20 )
		mc.textField('startTexFld', tx = '' , h = 30)
		mc.textField('endTexFld', tx = '', h = 30 )
		
		mc.text( label='', h = 8 )
		mc.text( label='', h = 8 )
		
		mc.button( label='GetTime' , command = self.function.getTimeLine,h=30)
		mc.button( label='SetTime' , command = self.function.setTimeLine,h=30)
		
		mc.text( label='', h = 8 )
		mc.text( label='', h = 8 )

		
		
		#Create Space
		mc.text( label='', h = 8 )
		mc.text( label='', h = 8 )
		
		mc.setParent("..")
		
		# BOTTON FOR CLICK
		mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 50),(2, 250)])
		
		mc.text( label='Path :', h = 25 )
		mc.textField( 'pathField' , fi = path )   
		mc.text( label='Name :', h = 25 )
		mc.textField( 'nameField', fi = name )

		#... Add selected root for export individual
		# mc.text( label='Root :', h = 25 )
		# mc.textField( 'rootField', fi = 'root' )
		
		mc.setParent("..")
		mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=1, columnWidth=[(1, 300)])

		
		mc.textFieldButtonGrp ("animClip", cw = [(1, 50), (2, 200), (3, 80)], l = "Clip : ", fi = name , bl = "No WP", bc = self.function.noWeapon)
		
		#mc.textFieldButtonGrp ("btnFirstJnt", cw = [(1, 50), (2, 200), (3, 50)], l = "Root : ", bl = "Set", bc = self.function.setFirstJnt)
		
		# mc.text( label='', h = 25 )
		# mc.text (h = 35, w = 50, al = "left", ww = 1, l = "something")
		# mc.textFieldButtonGrp ("btnFirstJnt", cw = [(1, 50), (2, 200), (3, 50)], l = "First joint: ", bl = "Set", bc = self.function.setFirstJnt)

		mc.separator( ann = 'Constraint', w=140, h = 20, style='in' )
		mc.button( label='Manual Export Selection', command = self.function.exportSelectionFBX ,w=50, h=30 )
		mc.separator( ann = 'Constraint', w=140, h = 20, style='in' )


		mc.text( label='', h = 8 )
		mc.setParent("..")
		mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=1, columnWidth=[(1, 300),(2, 150)])






		# CONNECT FRAME
		# mc.button( label = 'Connect Frame', command = self.function.setCamera ,w = 250, h = 30)
		# mc.text( label ='', h = 8 )

		# FOR PLAYBLAST
		# mc.button( label = 'Playblast', command = self.function.playBlast ,w=300, h=50 )
		# mc.text( label ='', h = 8 )

		# FOR BROWSE
		# mc.button( label = 'Open folder', command = self.function.openContainFile ,w = 250, h = 30)

		mc.button( label='Import Reference', command = self.function.importRef ,w=50, h=50 )
		mc.button( label='Bake Anim', command = self.function.bakeAnim ,w=50, h=50 )


		mc.button( label='Export Anim', command = self.function.exportFBX ,w=50, h=50 )  


		mc.showWindow( dWin )
	

# Manual run
# run = Ui()
# run.createGUI()


# from axionTools.asset import RegulusAnimExporter
# reload(RegulusAnimExporter)

# run = RegulusAnimExporter.Ui()
# run.createGUI()