def reset():
    import maya.cmds as mc
    sel = mc.ls('*:*_ctrl')
    for i in range(len(sel)):
        name = sel[i]
        attr = 'tx','ty','tz','rx','ry','rz','sx','sy','sz'
        for a in range(len(attr)):
            attrName = name + '.' + attr[a]
            lock = mc.getAttr(attrName,l=1)
            if lock == True:
                print 'LOCK'
            if lock == False:
                if attr[a] == 'sx':
                    mc.setAttr(attrName,1)
                elif attr[a] == 'sy':
                    mc.setAttr(attrName,1)
                elif attr[a] == 'sz':
                    mc.setAttr(attrName,1)
                else:
                    mc.setAttr(attrName,0)
    mc.select(cl=True)
