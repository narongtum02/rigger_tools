import maya.cmds as mc

from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload(rigTools)

def ikfkSnap():
	if len(mc.ls(sl=1)) > 0: # SELECT AND FINDING FUNCTION
		sel = mc.ls(sl=1) # list all select object
		for i in range(len(sel)):

			if 'Stick' in sel[i]: # if Stick in the select
				selName =  sel[i]
				#rawNam = giveName.split('_')
				if ':' in selName: # find out is need to extrac namespace or not
					# WE NEED TO SEP ':'
					sepNS = selName.split(':')
					namespace = sepNS[0] + ':'
					giveName = sepNS[1]
					sepName = giveName.split('_')
					if len(sepName) == 2:
						selCtrl = giveName
						print "A:"
					
					elif len(sepName) > 2:
						selCtrl = sepName[-2]+'_'+sepName[-1]
						noName = giveName.split(selCtrl)[0]  # IF have more than ONE _
						namespace = sepNS[0] + ':' + noName
						print "B:"

					# func
					rigTools.ikfkSwitch( selCtrl, namespace = namespace, longAlias = selCtrl, item=None)
					print namespace
					print selCtrl

				elif ':' not in selName: # there is no name space but have to find out is it have the preName or not
					print 'No nameSpace N/a'
					sepName = selName.split('_')
					if len(sepName) == 2:
						selCtrl = selName
						namespace = ''
						print "A"
					
					elif len(sepName) > 2:
						selCtrl = sepName[-2]+'_'+sepName[-1] # selCtrl = nn.split('_')[-1]
						namespace = giveName.split(selCtrl)[0]
						print "B"

					# func
					print namespace
					print selCtrl
					rigTools.ikfkSwitch( selCtrl, namespace = namespace, longAlias = selCtrl, item=None)
			else:
				print "Please Select any Stick" 

	else:
		print "pls select somthing"
