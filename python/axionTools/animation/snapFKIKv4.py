import maya.cmds as mc

from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload(rigTools)

def ikfkSnap():
	sel = mc.ls(sl=True)
	for each in sel:
		rigTools.ikfkSwitchV4( selCtrl =  each)

