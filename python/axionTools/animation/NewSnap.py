def Snap(): # DODE SNAP TOOL #
	# Find Namespace of Selection
	import maya.cmds as mc
	import maya.OpenMaya as om
	sel = mc.ls(sl=True)
	if len(sel) == 0:
		print '>>> Pls Select IK or FK Control <<<'
	if len(sel) > 0:
		name = sel[0]
		if ':' in name:
			splitName =  name.split(':')
			nameSpace = splitName[0]
			ns = nameSpace + ':'
			ctrlName = splitName[1]
		else:
			ns = ''
			ctrlName = name
		if '_pov_' in ctrlName:
			print '>>> Cannot use with POV <<<'
		if 'IK' in ctrlName: # IK Function 'pls Select IK before snap'
			# IK Snap Tool 
			# select the joints
			sel = mc.ls(sl=True)
			# assign selection
			FKctrl = mc.listConnections(sel[0] + '.FKctrl') # link with 'handLFT_bind_jnt'
			FKpov = mc.listConnections(sel[0] + '.FKpov') # link with 'elbowLFT_bind_jnt'
			IKctrl = mc.listConnections(sel[0] + '.IKctrl')
			IKpov = mc.listConnections(sel[0] + '.IKpov')
			IKrot = mc.listConnections(sel[0] + '.IKrot')
			# get position from FKctrl
			FKcRaw = mc.xform(FKctrl, ws=True, q=True, t=True)
			FKcPos = om.MVector(FKcRaw[0], FKcRaw[1], FKcRaw[2])
			# get position from FKpov
			FKpRaw = mc.xform(FKpov, ws=True, q=True, t=True)
			FKpPos = om.MVector(FKpRaw[0], FKpRaw[1], FKpRaw[2])
			# move IK to position
			mc.move(FKcPos.x, FKcPos.y, FKcPos.z, IKctrl)
			mc.move(FKpPos.x, FKpPos.y, FKpPos.z, IKpov)
			#Get rotation and Set Rotation
			FKrot = mc.getAttr(IKrot[0] + '.r')
			newRot = FKrot[0]
			rot = ('.rx','.ry','.rz')
			for r in range (3):
				print sel[0]+rot[r]
				mc.setAttr(sel[0] + rot[r], newRot[r])
	
		if 'FK' in ctrlName:# SIDE ZONE
		
			if 'LFT' in ctrlName:
				side = 'LFT'
			if 'RGT' in ctrlName:
				side = 'RGT'
			# ARM ZONE
			if 'upperArm' in ctrlName:
				giveName = 'upperArm','lowerArm' ,'hand'
			if 'lowerArm' in ctrlName:
				giveName = 'upperArm','lowerArm' ,'hand'
			if 'hand' in ctrlName:
				giveName = 'upperArm','lowerArm' ,'hand'
			# LEG ZONE    
			if 'upperLeg' in ctrlName:
				giveName = 'upperLeg','lowerLeg' ,'foot'
			if 'lowerLeg' in ctrlName:
				giveName = 'upperLeg','lowerLeg' ,'foot'
			if 'foot' in ctrlName:
				giveName = 'upperLeg','lowerLeg' ,'foot'
			if 'toes' in ctrlName:
				giveName = 'upperLeg','lowerLeg' ,'foot'        
			# FK Fuction
			#---------- <  IK SNAP FK  > ------------#
			# Naming Convention
			for i in range (len(giveName)):
				name = ns + giveName[i] + side
				ctrl = name + '_FK_ctrl'
				Loc = name + '_Loc_null'
				# SNAP FUNCTION
				# Need to Get rotation from Loc_null
				rotRaw = mc.xform(Loc, q=True, ro=True)
				rotPos = om.MVector(rotRaw[0], rotRaw[1], rotRaw[2])
				mc.rotate(rotPos.x, rotPos.y, rotPos.z, ctrl)
#Snap()
