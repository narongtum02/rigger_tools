# >>>>>   Untimate Export Button   <<<<<
# import Object from reference ****
# Delete Name space of Male.Rig ****
# Delete Weapon ( if have Shield delete it too) ****
# Bake Rot + Tran ( Bake to base Animation ) ****
# Bring Hip.tz to Root.tz ****
# Delete key from Hip.tz ****

#import pymel.core as pm

#import maya.OpenMaya as om



import maya.cmds as mc
class function:

	def timeLine(self,*args):
		# TimeLine Query 
		startTime = mc.playbackOptions(min = True,q = True)
		endTime = mc.playbackOptions(max = True,q = True)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)
		return [ startTime , endTime ]

	def keyTime(self,*args):
		# Hips.tx Query
		minKey = mc.keyframe('cog_ctrl.tx',index=(0,0),query=True)
		maxKey =  mc.keyframe('cog_ctrl.tx',iv = True,query=True)
		startTime = minKey[0]
		endTime = startTime + (len(maxKey)-1)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)    
		return [ startTime , endTime ]

	def importChar(self,*args):
		# IMPORT REFERENCE
		refs = mc.ls(type='reference')
		for i in refs:
			rFile = mc.referenceQuery(i, f=True)
			mc.file(rFile, importReference=True)
		
		# FIND EXISTING OBJECT
		all = mc.ls(dag=True)
		weapon = mc.ls('*:weapon_jnt')
		root = mc.ls('*:Root')
		sheild = mc.ls('*:shield_jnt')
		# Function
		if root[0] in all:
			root_ns = root[0].split(':')
			rig_ns = ':' + root_ns[0]
			mc.namespace(rm = rig_ns,mergeNamespaceWithParent = True, force = True)
		
		if weapon[0] in all:
			giveWeapon_ns = weapon[0].split(':')
			weapon_ns = ':' + giveWeapon_ns[0]
			mc.namespace(rm = weapon_ns,mergeNamespaceWithParent = True, force = True)
		
		elif sheild[0] in all:
			mc.delete('shield_jnt')
			
		else:
			print 'No Weapon'

	def bakeMocap(self,*args):
		
		# Get Time From outside
		startText =  mc.textField('startTexFld', tx = True, q = True)
		endText = mc.textField('endTexFld', tx = True, q = True)
		mc.playbackOptions(min = startText)
		mc.playbackOptions(max = endText)
		time = ( startText , endText )
		
		# Unhide Root 
		mc.setAttr( 'Root.v', 1)
		
		# Baking Function
		bakeJnt = mc.ls('*_jnt') # call _jnt
		bakeJnt.remove('hips_bind_jnt')
		
		mc.bakeResults(bakeJnt, simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])
		mc.bakeResults('hips_bind_jnt', simulation = True, t= time, at=["tx","ty","rx","ry","rz"])
		
		# Baking Locator
		mc.spaceLocator( n = 'RootMother', p=(0, 0, 0) )
		mc.pointConstraint( 'hips_bind_jnt' ,'RootMother', sk = ['x','y'],mo=1,w=1)
		mc.bakeResults('RootMother', simulation = True, t= time, at=["tx","ty","tz"])
		mc.delete('RootMother_pointConstraint1')
		mc.delete('Root_parentConstraint1')
		mc.delete('Root_scaleConstraint1')
		# Baking back to Root
		mc.pointConstraint( 'RootMother' ,'Root', sk = ['x','y'],mo=0,w=1)
		mc.bakeResults('Root', simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])
		mc.delete('RootMother')
		mc.delete('hips_bind_jnt_parentConstraint1') 
		mc.delete('rig_grp')
		mc.setKeyframe( 'hips_bind_jnt', at='tz', t= ['0','0'] )
		
		# Delete Weapon
		mc.delete('weapon_jnt')
		if len(mc.ls('shield_jnt*')) > 1:
			mc.delete('shield_jnt')
			

class Ui:
	def __init__(self):
		self.function = function()
	
	def createGUI(self,*args):
		# Make a new window
		if mc.window('bakeWin', exists = True):
			mc.deleteUI('bakeWin')
	
		dWin = mc.window('bakeWin', title="Bake Joint Animation", iconName='bkeMo', widthHeight=(300, 200), s = False, mm = False, mxb = False, mw = False )
		
		mc.frameLayout( label='Bake Options',collapsable=False, mw=5, mh=5 )
		#mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150), (2, 150)] )
		mc.button( label='GetTime' , command = self.function.timeLine,h=50)
		mc.button( label='Timeline' , command = self.function.timeLine,h=50)
	
		
		#mc.rowColumnLayout( numberOfColumns=4, columnAttach=(1, 'right', 0), columnWidth=[(1, 50), (1, 50)] )
		mc.text( label='Start :' )
		mc.textField('startTexFld', tx = '' , h = 30)
		mc.text( label='End :' )
		mc.textField('endTexFld', tx = '', h = 30 )
	
		mc.button( label='Import Reference', command = self.function.importChar ,w=50, h=50 )
		mc.button( label='Bake', command = self.function.bakeMocap ,w=50, h=50 )
	
	mc.checkBox( label='One', p= 'Bake Options')
	#mc.checkBox( label='Two' )
	#mc.checkBox( label='Three' )
	#mc.checkBox( label='Four' )
		mc.showWindow( dWin )
'''		
aa= Ui()
aa.createGUI()

createGUI()
time = timeLine ( )
startTime = time[0]
endTime = time[1]
#local run
'''

