# >>>>>   Untimate Export Button   <<<<<
# import Object from reference ****
# Delete Name space of Male.Rig ****
# Delete Weapon ( if have Shield delete it too) ****
# Bake Rot + Tran ( Bake to base Animation ) ****
# Bring Hip.tz to Root.tz ****
# Delete key from Hip.tz ****

import pymel.core as pm
import maya.cmds as mc
#import maya.OpenMaya as om

class function:
	def timeLine(self,*args):
		# TimeLine Query 
		startTime = mc.playbackOptions(min = True,q = True)
		endTime = mc.playbackOptions(max = True,q = True)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)
		return [ startTime , endTime ]

	def keyTime(self,*args):
		# Hips.tx Query
		minKey = mc.keyframe('cog_ctrl.tx',index=(0,0),query=True)
		maxKey =  mc.keyframe('cog_ctrl.tx',iv = True,query=True)
		startTime = minKey[0]
		endTime = startTime + (len(maxKey)-1)
		mc.textField('startTexFld', edit = True, tx = startTime)
		mc.textField('endTexFld',edit = True, tx = endTime)    
		return [ startTime , endTime ]

	def importChar(self,*args):
	# IMPORT REFERENCE
		allrefs = pm.getReferences(recursive = True )
		for ref in allrefs:
			try:
				allrefs[ref].importContents( removeNamespace = True )
			except RuntimeError:
				pass
		print '\nImport and clear namespace ...'

	def bakeMocap(self,*args):           
		# Get Time From outside
		startText =  mc.textField('startTexFld', tx = True, q = True)
		endText = mc.textField('endTexFld', tx = True, q = True)
		mc.playbackOptions(min = startText)
		mc.playbackOptions(max = endText)
		time = ( startText , endText )
		# Unhide Root 
		mc.setAttr( 'Root.v', 1)
		# Baking Function
		bakeJnt = mc.ls('*_jnt') # call _jnt
		# From Check Box Param
		if mc.checkBox("bakeRootParam" ,q = 1, value = True ):
			#print 'checkbox is on'
			bakeJnt.remove('hips_bind_jnt')
			mc.bakeResults(bakeJnt, simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])
			mc.bakeResults('hips_bind_jnt', simulation = True, t= time, at=["tx","ty","rx","ry","rz"])
			# Baking Locator
			mc.spaceLocator( n = 'RootMother', p=(0, 0, 0) )
			mc.pointConstraint( 'hips_bind_jnt' ,'RootMother', sk = ['x','y'],mo=1,w=1)
			mc.bakeResults('RootMother', simulation = True, t= time, at=["tx","ty","tz"])
			mc.delete('RootMother_pointConstraint1')
			mc.delete('Root_parentConstraint1')
			mc.delete('Root_scaleConstraint1')
			# Baking back to Root
			mc.pointConstraint( 'RootMother' ,'Root', sk = ['x','y'],mo=0,w=1)
			mc.bakeResults('Root', simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])
			mc.delete('RootMother')
			mc.delete('hips_bind_jnt_parentConstraint1') 
			mc.delete('rig_grp')
			mc.setKeyframe( 'hips_bind_jnt', at='tz', t= ['0','0'] )
		else:
			#print 'checkbox is off'
			mc.bakeResults(bakeJnt, simulation = True, t= time, at=["tx","ty","tz","rx","ry","rz"])
		# Delete Weapon
		mc.delete('weapon_jnt')
		if len(mc.ls('shield_jnt*')) > 1:
			mc.delete('shield_jnt')


class Ui:
	def __init__(self):
		self.function = function()
	
	def createGUI(self,*args):
		# Make a new window
		if mc.window('bakeWin', exists = True):
			mc.deleteUI('bakeWin')
	
		dWin = mc.window('bakeWin', title="Bake Joint Animation [PH]", iconName='bkeMo', widthHeight=(300, 200), s = 1, mm = 0, mxb = 0, mw = False )
		
		mc.frameLayout( label='Bake Options [PH]',collapsable=False, mw=5, mh=5 )
		mc.columnLayout( adjustableColumn=True )
		mc.rowColumnLayout( numberOfColumns=2, columnWidth=[(1, 150), (2, 150)])
		#mc.rowColumnLayout( nr=5, rh=[(1, 150), (2, 150), (3, 150)])
		
		mc.button( label='GetTime' , command = self.function.timeLine,h=50)
		mc.button( label='Timeline' , command = self.function.timeLine,h=50)
		
		mc.text( label='Start :' )
		mc.textField('startTexFld', tx = '' , h = 30)
		mc.text( label='End :' )
		mc.textField('endTexFld', tx = '', h = 30 )
		mc.separator(style='none')
		# check box Param
		mc.checkBox("bakeRootParam", l='bake Root' ,value = True )

		#mc.checkBox(l='00')
		mc.button( label='Import Reference', command = self.function.importChar ,w=50, h=50 )
		mc.button( label='Bake', command = self.function.bakeMocap ,w=50, h=50 )
		
		mc.showWindow( dWin )
	
#aa= Ui()
#aa.createGUI()
