import maya.cmds as mc

# set key for every bind joint
# select every geo that want to export

from axionTools.rigging.skin import skinUtil as skinUtil
reload(skinUtil)

from axionTools.pipeline import fileTools as fileTools 
reload(fileTools)


import os

def exportOutfit():
	items = mc.ls(sl=True)

	# set key frame
	mc.currentTime(0)
	skinUtil.selectBindJnt()
	mc.setKeyframe()

	# Select and delete
	#mc.select(  , r = True )
	print 'Deleting rig_grp...'
	mc.delete( 'rig_grp' )






	for s in range(len(items)):
		# Find Name
		giveName = items[s].split('_')
		newName = giveName[1].split('0')
		name = newName[0]
		# ready for export
		mc.parent( items[s] , w = True )
		mc.select( cl = True)
		mc.select('*Root', items[s] )
		# file path
		#filePath = 'D:/True_Axion/Project_EVERGLEAM_HILL/3D_ART/Character/Player/clothes/' + name + '/fbx/' # To SVN
		#filePath = 'D:/True_Axion/Project_EVERGLEAM_HILL/3D_ART/Character/Player/female/fbx/' # To SVN
		filePath = 'D:/True_Axion/Project_EVERGLEAM_HILL/3D_ART/Character/Player/clothes/' + name + '/fbx/' # To DESKTOP

		fileName = items[s]
		# check exists folder 
		fileTools.currentFilePath(filePath)
		
		print 'File has been export at: %s' %filePath
		mc.file( filePath + fileName + '.fbx', f=True, op=('v=0'), typ='FBX export', pr=True, es = True)


	# open folder
	os.startfile( filePath )

		

#exportOutfit()

