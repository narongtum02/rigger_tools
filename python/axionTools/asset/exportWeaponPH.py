 def exportWeapon():
    ExportItem = mc.ls(sl=True)
    for i in range(len(ExportItem)):
        # Find all Name and path
        selName = ExportItem[i]
        prename = ExportItem[i].split('|')
        name = prename[1]
        sp = name.split('_')    # Split by '_'
        sex = sp[0]
        numName = sp[1] + '_' + sp[2]
        varNum = sp[3]
        texNum = sp[4]
        typeName = sp[5]
        
        # Clear & Selection
        mc.select(cl=True)
        # Export to SVN 
        filePath = 'D:/True_Axion/Project_HOPE/Content/3D_ART/Character/Player/m/' + numName + '/' + varNum + '/' + texNum + '/' + typeName + '/Unity_export/' # To SVN
        mc.select(selName)
        sel = mc.ls(sl=True)
        mc.file(filePath + name + '.fbx', f=True, op=('v=0'), typ='FBX export', pr=True, es = True)