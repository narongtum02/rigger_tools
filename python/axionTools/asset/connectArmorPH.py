# Armor Show INVICTUS
version = 0.8
import maya.cmds as mc
class function:
	
	def armorRig_show ( self, *args ):
		giveName = mc.textField( 'nameField', tx = True, q = True )
		print giveName
		mainCtrl = 'nucleus_ctrl'

		sepName = giveName.split('_')

		sex = sepName[0]
		digit = sepName[1]
		armorName = sepName[2] + '_' + sepName[3]

		print 'armorRig_show_function'
		print sex
		print digit
		print armorName
	 
		giveType = 'helmet','chest','arms','hip','legs'
		enAttr = 'none:all:helmet:chest:arms:hip:legs'

		# NAME GIVING

		# Create Type List

		mainAttr = mainCtrl + '.' + armorName
		name = sex + '_' + digit + '_' + armorName + '_1'

		set = name + '_set'
		ubCon = name + '_ub_con'
		lbCon = name + '_lb_con'
		colrFls = '.colorIfFalse.colorIfFalse'
		colrTru = '.colorIfTrue.colorIfTrue'
		colrOut = '.outColor.outColor'
		
		# add Attr
		mc.addAttr( mainCtrl, longName  = armorName, at = 'enum', keyable = True, en = enAttr)
		# Connect set Vis
		mc.connectAttr(mainAttr, set + '.v')

		# Create Node
		mc.createNode('condition', n = ubCon)
		mc.createNode('condition', n = lbCon)
		# Set Attr
		mc.setAttr(ubCon + '.secondTerm' ,1)
		mc.setAttr(lbCon + '.secondTerm' ,1)
		# Connect to Main
		mc.connectAttr(mainAttr, ubCon + '.firstTerm')
		mc.connectAttr(mainAttr, lbCon + '.firstTerm')

		for t in range(len(giveType)):
			type = giveType[t]
			armor = name + '_' + type
			amrCon = armor + '_con'
			count = t+2
			# Create and Set Node
			mc.createNode('condition', n = amrCon)
			mc.setAttr(amrCon + '.secondTerm', count)
			giveChanel = ('R','G','B')
			for a in range(len(giveChanel)):
				chanel = giveChanel[a]
				mc.setAttr(amrCon + colrTru + chanel,1)
				mc.setAttr(amrCon + colrFls + chanel,0)
			# Let Connect
			mc.connectAttr(mainAttr, amrCon + '.firstTerm')
			# IF is TYPE
			if t <= 2 :
				allCon = ubCon
				if t == 0:
					colr = 'R'
				elif t == 1:
					colr = 'G'
				elif t == 2:
					colr = 'B'
			elif t >= 3:
				allCon = lbCon
				if t == 3:
					colr = 'R'
				elif t == 4:
					colr = 'G'
			# Let Connect IN
			mc.setAttr(allCon + colrTru + colr, 1)
			# Let Connect OUT
			mc.connectAttr(amrCon + colrOut + 'R', allCon + colrFls + colr)
			mc.connectAttr(set + '.v', allCon + colrTru + colr)
			mc.connectAttr(allCon + colrOut + colr,armor + '.v')

			# place for creating window

		#armorRig_show ( giveName = 'f_0030_horsbane_var0_1', mainCtrl = 'nucleus_ctrl' )
			
class Ui:
	def __init__(self):
		self.function = function()

	def createGUI( self ):
		sel = mc.ls(sl=1)
		if len(sel) > 0:
			name = sel[0]
			if 'set' in name:
				name = name.split('_set')[0]
			print name
		else:
			name = 'f_0010_valiant_var0_1'

		# Make a new window
		if mc.window('dtArmorRig_show', exists = True):
			mc.deleteUI('dtArmorRig_show')
	
		dWin = mc.window('dtArmorRig_show', title="The armorRig_show v. %s" %version , iconName ='PB', widthHeight=(300, 200), s = 1, mm = 0, mxb = 0, mw = False )
		
		mc.frameLayout( label='armorRig_show',collapsable=False, mw=5, mh=5 )
		mc.columnLayout( adjustableColumn=True )
		
		mc.rowColumnLayout( numberOfColumns=3, columnWidth=[(1, 50),(2, 200),(3,50)])

		mc.text( label='Name :', h = 25 )
		#mc.textField( 'nameField' )
		mc.textField( 'nameField', fi = name )
		mc.button( label = 'Connect', command = self.function.armorRig_show ,w = 50, h = 30)

		mc.showWindow( dWin )
	
# Manual run
#run = Ui()
#run.createGUI()