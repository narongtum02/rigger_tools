# HOPE automate export FBX 
# select geo and run

from axionTools.rigging.skin import skinUtil as skinUtil
reload(skinUtil)

from axionTools.pipeline import fileTools as fileTools 
reload(fileTools)

import maya.cmds as mc
def exportArmor():

	ExportItem = mc.ls(sl=True)


	# set key frame
	mc.currentTime(0)
	skinUtil.selectBindJnt()
	mc.setKeyframe()

	# Select and delete
	#mc.select(  , r = True )
	if mc.objExists('rig_grp'):
		print 'Deleting rig_grp...'
		mc.delete( 'rig_grp' )
	else:
		print 'rig_grp has been already deleted'



	for i in range(len(ExportItem)):
		name = ExportItem[i]
		# Split by '_'
		sp = name.split('_')
		sex = sp[0]
		numName = sp[1]
		armorName = sp[2]
		varNum = sp[3]
		catName = sp[5]
		if 'm' in sex[0]:
			gender = 'Male'
			
		if 'f' in sex[0]:
			gender = 'Female'
			
		mc.parent (ExportItem[i],w=True)
		mc.select(cl=True)
		mc.select('*Root', '*'+ name)
		# for SVN
		# D:\True_Axion\Project_HOPE\Content\3D_ART\Character\Player\f\Armor\0010_valiant\var0
		
		#filePath = 'D:/WORK/Hope/share/char/Player/' + gender + '/Export/' + numName + '/' + varNum + '/' # This Computer
		
		filePath = 'D:/True_Axion/Project_HOPE/Content/3D_ART/Character/Player/' + sex + '/Armor/' + numName + '_cosmetics/' + armorName + '/' + varNum + '/' # To SVN

		fileTools.checkExistFolder( filename = filePath )
		
		mc.select('*Root', name)
		sel = mc.ls(sl=True)
		fileName = sel[1]
		mc.file(filePath + fileName + '.fbx', f=True, op=('v=0'), typ='FBX export', pr=True, es = True)
		print 'File has been export at: %s' %filePath

	# Open in Explore
	fileTools.openContainerFile( path = filePath )