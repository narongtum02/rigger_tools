# INVICTUS BSH connect and create
import maya.cmds as mc 
def connectExpression():
	# connect Blendshape to Ctrl
	attrs = [ 'Blink','Serious','Relax','Hit','Happy','AngryAh','AngryEhh' ]
	ctrl = 'head_ctrl'
	# test rail ::: sel = 'm_0200_WrestlerArmor_var0_1_helmet'
	print 'Please Select helmet'

	sel = mc.ls(sl=True)
	shp = mc.listRelatives( sel, s = True )[0]
	#mc.nodeType( 'expression_WrestlerArmor_bsh', api = True)
	bsh = mc.listConnections( shp,  type = 'blendShape' )[0]
	for attr in attrs:
	    mc.connectAttr( ctrl + '.%s' %attr, bsh + '.%s' %attr )
	    print '%s_______Connected' %attr

#connectExpression()


def connectBshToCtrl():
	# connect Blendshape to Ctrl
	attrs = [ 'Blink','Serious','Relax','Hit','Happy','AngryAh','AngryEhh' ]
	ctrl = 'head_ctrl'
	print 'Please Select Blendshape'
	sel = mc.ls(sl=True)
	bsh = sel[0]
	for attr in attrs:
	    mc.connectAttr( ctrl + '.%s' %attr, bsh + '.%s' %attr )
	    print '%s_______Connected' %attr

#connectBshToCtrl()