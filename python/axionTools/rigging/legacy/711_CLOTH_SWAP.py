# Untimate CLOTH SWAP
# NAME GIVING
# For 711 Cloth naming

import maya.cmds as mc


giveName = mc.ls(sl=True)[0]


mainCtrl = 'sevenEleven_ctrl'
giveType = 'hat','ub','lb','fw'

enAttr = 'none:all:head:ub:lb:fw'
colrFls = '.colorIfFalse.colorIfFalse'
colrTru = '.colorIfTrue.colorIfTrue'
colrOut = '.outColor.outColor'







if giveName:
	subAsset = giveName.split('_')

	if len(subAsset) == 4:
		print 'This might be a 711 asset.'
		project = subAsset[1]
		cloth = subAsset[2]
		gender = subAsset[3]+'01'
		nameAttr = 'seven' + '_' + cloth + '_' + gender

	else:
		mc.error('Please check project name.')

else:
	mc.error('Please select group name.')



# Create Type List


mainAttr = mainCtrl + '.' + nameAttr
name = giveName
set = giveName
ubCon = name + '_ub_con'
lbCon = name + '_lb_con'


# add Attr
mc.addAttr( mainCtrl, longName  = nameAttr, at = 'enum', keyable = True, en = enAttr)
# Connect set Vis
mc.connectAttr(mainAttr, giveName + '.v')
# Create Node
mc.createNode('condition', n = ubCon)
mc.createNode('condition', n = lbCon)
# Set Attr
mc.setAttr(ubCon + '.secondTerm' ,1)
mc.setAttr(lbCon + '.secondTerm' ,1)
# Connect to Main
mc.connectAttr(mainAttr, ubCon + '.firstTerm')
mc.connectAttr(mainAttr, lbCon + '.firstTerm')

for t in range(len(giveType)):

	type = giveType[t]


	# armor =  type + '_' + name
	# armor =  type + ''

	armor = type + '_' + project + '_' + cloth +'_' + gender
	print armor

	amrCon = armor + '_con'
	count = t+2

	
	# Create and Set Node
	mc.createNode('condition', n = amrCon)
	mc.setAttr(amrCon + '.secondTerm', count)
	giveChanel = ('R','G','B')
	for a in range(len(giveChanel)):
		chanel = giveChanel[a]
		mc.setAttr(amrCon + colrTru + chanel,1)
		mc.setAttr(amrCon + colrFls + chanel,0)
	# Let Connect
	mc.connectAttr(mainAttr, amrCon + '.firstTerm')
	# IF is TYPE
	if t <= 1 :
		allCon = ubCon
		if t == 0:
			colr = 'R'
		elif t == 1:
			colr = 'G'
		#elif t == 2:
			#colr = 'B'
	elif t >= 2:
		allCon = lbCon
		if t == 2:
			colr = 'R'
		elif t == 3:
			colr = 'G'
	# Let Connect IN
	mc.setAttr(allCon + colrTru + colr, 1)
	# Let Connect OUT
	mc.connectAttr(amrCon + colrOut + 'R', allCon + colrFls + colr)
	mc.connectAttr(set + '.v', allCon + colrTru + colr)
	mc.connectAttr(allCon + colrOut + colr,armor + '.v')

	print 'Connect Complete'
	
	
		

if subAsset == 'boy01':
	print 'male'
	mc.select('maleFaceShow_con' , r = True)
	mc.select('maleFace01_pma' , add = True)
	mc.select('maleHair01_pma' , add = True)

elif subAsset == 'girl01':
	mc.select('femaleFaceShow_con' , r = True)
	mc.select( name + '_ub_con' , add = True)

'''

mc.select('femaleFaceShow_con' , r = True)

mc.select( name + '_ub_con' , add = True)
mc.select('maleHair02_pma' , add = True)
'''



