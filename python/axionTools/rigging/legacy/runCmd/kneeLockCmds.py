from axionTools.rigging.version import kneeLock_v002 as kl
reload(kl)

from axionTools.rigging.util import misc as misc
reload(misc)

from axionTools.pipeline import fileTools as fileTools 
reload(fileTools)







nameUPR = kl.createDistance( 	'upperLegLFT_IK_ctrl' , 
								'legLFT_pov_ctrl'
								 )

nameLWR = kl.createDistance( 'legLFT_pov_ctrl' , 
								'footLFT_IK_ctrl' )


blendColor = kl.createBlendColor(name = nameUPR[0] , uprDistance = nameUPR[2], lwrDistance = nameLWR[2] )


kl.distJnt( 
			stretchNode		= 	'legStretchLFT_pma' 	, 
            upperJnt		= 	'lowerLegLFT_IK_jnt'	, 
            lowerJnt		= 	'footLFT_IK_jnt'	 	, 
            blendName		=	blendColor
            )




