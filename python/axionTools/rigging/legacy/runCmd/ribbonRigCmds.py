from axionTools.rigging.feature import ribbon_jh as rbnRig
reload(rbnRig)



rbnRig.runRbnRig( 	
				width = 20				,
				numJoints = 3 			,
				prefix = 'uprAnim' 		, 
				side = 'LFT'			, 
				aim = 'y+'				,	
				jointTOP = 'joint1'			,
				jointEND = 'joint2'			
			)
