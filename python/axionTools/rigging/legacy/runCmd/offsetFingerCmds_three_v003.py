'''
this is core function for finger curl 
you can see run command code at 
D:\\True_Axion\\Tools\\riggerTools\\python\\axionTools\\rigging\\runCmd\\offsetFingerCmds_five_v003.py
'''


from axionTools.rigging import fingerOffsetRig as finOffRig
reload(finOffRig)



# ----------------------------------  LEFT HAND
fingerName,fingerbehavior,side,ctrlName = finOffRig.defineVariable( fingerbehavior = ('fist','roll','relax'), fingerName = ('thumb','index','middle' ) , side = 'LFT' , ctrlName = 'stickLFT_ctrl')

# 1 create each offset group
# run relax function	
finOffRig.createOffsetGrp( side ,fingerName )
finOffRig.creaePostStore( side ,fingerbehavior )

# 3
# create function for relax only
finOffRig.creRelax( finName = 'thumb', side = 'LFT', amp = 1.1)
finOffRig.creRelax( finName = 'index', side = 'LFT', amp = 2.1)
finOffRig.creRelax( finName = 'middle',side = 'LFT',  amp = 3.1)

# 4.2
finOffRig.connectToOffGrp(fingerName,fingerbehavior,side)


from axionTools.rigging import core
reload(core)

# LFT Stick
stickLFT = core.Base()
stickLFT.createController('stick_ctrlShape')
stickLFT.setName(ctrlName)


from axionTools.rigging.readWriteCtrlSizeData import flipController as fip
reload(fip)

#  Stick
sel = mc.select(ctrlName)
upSize = fip.buildUI()
upSize.flipCtrlShape(sel , axis=[3, 3, 3])

# 6
finOffRig.doCreateAttr( ctrlName,fingerbehavior  )

#7.1 
finOffRig.normalConnect( ctrlName , fingerbehavior ,side)


# 8
# connection for relax
finOffRig.conxAdv( ctrlName, side, finger = 'relax', position = 'middleRelax' )
finOffRig.conxAdv( ctrlName, side, finger = 'relax', position = 'indexRelax' )
finOffRig.conxAdv( ctrlName, side, finger = 'relax', position = 'thumbRelax' )
#conxAdv(ctrlName, finger = 'relax', position = 'ringRelax')
#conxAdv(ctrlName, finger = 'relax', position = 'pinkyRelax')


# 9
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '01')	
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '02')
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '03')


# 10.1
finOffRig.doConnectRelax(	side,figName = 'index' , finPst = 'Relax' ,numOfctrl = ('01','02','03')		)
finOffRig.doConnectRelax(	side,figName = 'thumb' , finPst = 'Relax', numOfctrl = ('01','02','03')		)	
finOffRig.doConnectRelax(	 side,figName = 'middle' , finPst = 'Relax',numOfctrl = ('01','02','03')		)
#doConnectRelax(figName = 'ring' , finPst = 'Relax', numOfctrl = ('01','02','03'))	
#doConnectRelax(figName = 'pinky' , finPst = 'Relax', numOfctrl = ('01','02','03'))



from axionTools.rigging.controllerBox import adjustController as adjust
reload(adjust)

mc.select(ctrlName)
zroName = adjust.createZroGrp()

from axionTools.rigging.util import misc as misc
reload(misc)

misc.snapPointConst('hand%s_bind_jnt' %side,zroName)
misc.snapParentConstrMo('hand%s_bind_jnt' %side,zroName)
mc.parent(zroName,'fly_ctrl')		










# ----------------------------------  RIGHT HAND

# 0 return neccery variable
fingerName,fingerbehavior,side,ctrlName = finOffRig.defineVariable( fingerbehavior = ('fist','roll','relax'), fingerName = ('thumb','index','middle' ) , side = 'RGT' , ctrlName = 'stickRGT_ctrl')

# 1 create each offset group
# run relax function	

finOffRig.createOffsetGrp( side ,fingerName )
finOffRig.creaePostStore( side ,fingerbehavior )

# 3
# create function for relax only
finOffRig.creRelax(side, finName = 'thumb', amp = 1.1)
finOffRig.creRelax(side, finName = 'index', amp = 2.1)
finOffRig.creRelax(side, finName = 'middle',  amp = 3.1)
#creRelax( finName = 'ring',  amp = 4.1)
#creRelax( finName = 'pinky',  amp = 5.1)

# 4.2
finOffRig.connectToOffGrp(fingerName,fingerbehavior,side)




# create controller
stickRGT = core.Base()
stickRGT.createController('stick_ctrlShape')
stickRGT.setName(ctrlName)

sel = mc.select(ctrlName)
upSize = fip.buildUI()
upSize.flipCtrlShape(sel , axis=[3, 3, 3])


# 6
finOffRig.doCreateAttr( ctrlName,fingerbehavior  )
#7.1 
finOffRig.normalConnect( ctrlName , fingerbehavior ,side)



# 8
# connection for relax
finOffRig.conxAdv( ctrlName, side, finger = 'relax', position = 'middleRelax' )
finOffRig.conxAdv( ctrlName, side, finger = 'relax', position = 'indexRelax' )
finOffRig.conxAdv( ctrlName, side, finger = 'relax', position = 'thumbRelax' )
#conxAdv(ctrlName, finger = 'relax', position = 'ringRelax')
#conxAdv(ctrlName, finger = 'relax', position = 'pinkyRelax')


# 9
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '01')	
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '02')
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '03')



# 10.1
finOffRig.doConnectRelax(	side,figName = 'index' , finPst = 'Relax' ,numOfctrl = ('01','02','03')		)
finOffRig.doConnectRelax(	side,figName = 'thumb' , finPst = 'Relax', numOfctrl = ('01','02','03')		)	
finOffRig.doConnectRelax(	 side,figName = 'middle' , finPst = 'Relax',numOfctrl = ('01','02','03')		)
#doConnectRelax(figName = 'ring' , finPst = 'Relax', numOfctrl = ('01','02','03'))	
#doConnectRelax(figName = 'pinky' , finPst = 'Relax', numOfctrl = ('01','02','03'))


mc.select(ctrlName)
zroName = adjust.createZroGrp()

misc.snapPointConst('hand%s_bind_jnt' %side,zroName)
misc.snapParentConstrMo('hand%s_bind_jnt' %side,zroName)
mc.parent(zroName,'fly_ctrl')		