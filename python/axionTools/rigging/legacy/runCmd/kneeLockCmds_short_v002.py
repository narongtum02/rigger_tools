from axionTools.rigging import kneeLockCmds as run
reload(run)

#  LFT hand
run.runLock(			nameUPR 		=		 	'upperArmLFT_FK_ctrl',
						namePov		 	= 			'armLFT_pov_ctrl',
						nameLWR 		= 			'handLFT_IK_ctrl',
						stretchNode 	= 		'armStretchLFT_pma',
						upperJnt 		= 			'lowerArmLFT_IK_jnt',
						lowerJnt 		= 			'handLFT_IK_jnt')



#  RGT hand
run.runLock(			nameUPR				 = 		'upperArmRGT_FK_ctrl',
						namePov 			= 		'armRGT_pov_ctrl',
						nameLWR 			= 		'handRGT_IK_ctrl',
						stretchNode 		= 	'armStretchRGT_pma',
						upperJnt 			= 		'lowerArmRGT_IK_jnt',
						lowerJnt 			= 		'handRGT_IK_jnt')




#  LFT knee
run.runLock(			nameUPR 			= 		'upperLegLFT_FK_ctrl',
						namePov 			= 		'legLFT_pov_ctrl',
						nameLWR 			= 		'footLFT_IK_ctrl',
						stretchNode			 = 	'legStretchLFT_pma',
						upperJnt 			= 		'lowerLegLFT_IK_jnt',
						lowerJnt 			= 		'footLFT_IK_jnt')


#  RGT knee
run.runLock(			nameUPR 			= 		'upperLegRGT_FK_ctrl',
						namePov 			= 		'legRGT_pov_ctrl',
						nameLWR 			= 		'footRGT_IK_ctrl',
						stretchNode 		= 	'legStretchRGT_pma',
						upperJnt 			= 		'lowerLegRGT_IK_jnt',
						lowerJnt 			= 		'footRGT_IK_jnt')