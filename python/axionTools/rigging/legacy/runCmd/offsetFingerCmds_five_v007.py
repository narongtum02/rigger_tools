# Update
# 10.Oct.09.Fri_update rig 
from axionTools.rigging.feature import fingerOffsetRig as finOffRig
reload(finOffRig)


# this is for EH and hope naming convertion only
side = 'LFT'

# ----------------------------------  LEFT HAND
#fingerName,fingerbehavior,side,ctrlName = finOffRig.defineVariable( fingerbehavior = ('fist','roll','relax'), fingerName = ('thumb','index','middle' ) , side = 'SIDE' , ctrlName = 'stickSIDE_ctrl')
fingerName,fingerbehavior,side,ctrlName,numCtrl,zroNam, offsetNam = finOffRig.defineVariable( fingerbehavior = ('fist','roll','relax'), fingerName = ('thumb','index','middle','ring','pinky') , side = side , ctrlName = 'stick%s_ctrl' %side , numCtrl = 3 , zroNam = 'Zro_grp' , offsetNam = 'Offset_grp')

# 1 create each offset group
# run relax function	
finOffRig.createOffsetGrp(  side , fingerName )
finOffRig.creaePostStore( side ,fingerbehavior )

# 3
# create function for relax only
finOffRig.creRelax( side, finName = 'thumb',	amp = 1.1)
finOffRig.creRelax( side, finName = 'index', 	amp = 2.1)
finOffRig.creRelax( side, finName = 'middle',	amp = 3.1)
finOffRig.creRelax( side, finName = 'ring', 	amp = 4.1)
finOffRig.creRelax( side, finName = 'pinky',  	amp = 5.1)

# 4.2
finOffRig.connectToOffGrp(fingerName,fingerbehavior,side)


from axionTools.rigging.autoRig.base import core
reload(core)




#stickSIDE = core.Dag('armStickSIDE_ctrl')


stickSIDE = core.Dag('armStick%s_ctrl' %side)
stickSIDE.nmCreateController('stick_ctrlShape')
stickSIDE.scaleShape(scale = ( 0.08,0.08,0.08 )  )

if side == 'LFT':
	stickSIDE.setColor('red')
elif side == 'RGT':
	stickSIDE.setColor('blue')

zro_grp = core.Null('armStick%sZro_grp' %side)
stickSIDE.parent(zro_grp)

zro_grp.snapPoint('hand%s_bind_jnt' %side)
core.parentConstraint('hand%s_bind_jnt' %side , zro_grp , mo = True)


# 6
finOffRig.doCreateAttr( stickSIDE.name,fingerbehavior )

#7.1 
finOffRig.normalConnect( stickSIDE.name , fingerbehavior ,side)


# 8
# connection for relax
finOffRig.conxAdv( stickSIDE.name, side, finger = 'relax', position = 'middleRelax' )
finOffRig.conxAdv( stickSIDE.name, side, finger = 'relax', position = 'indexRelax' )
finOffRig.conxAdv( stickSIDE.name, side, finger = 'relax', position = 'thumbRelax' )
finOffRig.conxAdv( stickSIDE.name, side, finger = 'relax', position = 'ringRelax' )
finOffRig.conxAdv( stickSIDE.name, side, finger = 'relax', position = 'pinkyRelax' )



# 9
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '01')	
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '02')
finOffRig.connectPma(  fingerbehavior,fingerName,side, nameOfPost = ('fist','roll'),numVal = '03')


# 10.1
finOffRig.doConnectRelax(	side, figName = 'index' 	, finPst = 'Relax' 		,numOfctrl = ('01','02','03')		)
finOffRig.doConnectRelax(	side, figName = 'thumb' 	, finPst = 'Relax'		, numOfctrl = ('01','02','03')		)	
finOffRig.doConnectRelax(	side, figName = 'ring' 		, finPst = 'Relax'		,numOfctrl = ('01','02','03')		)
finOffRig.doConnectRelax(	side, figName = 'pinky' 	, finPst = 'Relax'		,numOfctrl = ('01','02','03')		)
finOffRig.doConnectRelax(	side, figName = 'middle'	 , finPst = 'Relax'		,numOfctrl = ('01','02','03')		)



