from axionTools.rigging import kneeLock as kl
reload(kl)

from axionTools.rigging.util import misc as misc
reload(misc)

from axionTools.pipeline import fileTools as fileTools 
reload(fileTools)



# ------------------------  Hand LFT
# create distance 
nameUPR = kl.createDistance( 'shoulderLFT_null_grp' , 'armLFT_pov_ctrl' )
nameLWR = kl.createDistance( 'armLFT_pov_ctrl' , 'handLFT_IK_ctrl' )


node = kl.createBlendColor( 
									name = nameUPR[0] 			, 
									uprDistance = nameUPR[2]	, 
									lwrDistance = nameLWR[2] 	,
									uprNam = nameUPR[4]			,
									side =  nameUPR[5]              
								)


namLock = kl.doAddAttr( nameUPR[6] , nameLWR[4])

kl.distJnt( 
				stretchNode		= 	'armStretchLFT_pma' 	, 
				upperJnt		= 	'lowerArmLFT_IK_jnt'	, 
				lowerJnt 		= 	'handLFT_IK_jnt'	 	, 
				blendName		=		node[0]      	    ,
				namLock         =     	namLock             ,
				povName         =     	nameUPR[6]
			)



kl.arrangeGrp(
				nameGrp		=	nameUPR[3]	,    
				side		=	nameUPR[5]	,
				distanceName=	nameUPR[2]	,
				uprLoc		=	nameUPR[7]	,
				lwrLoc		=	nameUPR[8]	
				
)


kl.arrangeGrp(
				nameGrp		=	nameLWR[3]	,    
				side		=	nameLWR[5]	,
				distanceName=	nameLWR[2]	,
				uprLoc		=	nameLWR[7]	,
				lwrLoc		=	nameLWR[8]	
				
)










# ------------------------  leg LFT
# create distance 
nameUPR = kl.createDistance( 'upperLegLFT_IK_ctrl' , 'legLFT_pov_ctrl' )
nameLWR = kl.createDistance( 'legLFT_pov_ctrl' , 'footLFT_IK_ctrl' )


node = kl.createBlendColor( 
									name = nameUPR[0] 			, 
									uprDistance = nameUPR[2]	, 
									lwrDistance = nameLWR[2] 	,
									uprNam = nameUPR[4]			,
									side =  nameUPR[5]              
								)


namLock = kl.doAddAttr( nameUPR[6] , nameLWR[4])

kl.distJnt( 
				stretchNode		= 	'legStretchLFT_pma' 	, 
				upperJnt		= 	'lowerLegLFT_IK_jnt'	, 
				lowerJnt 		= 	'footLFT_IK_jnt'	 	, 
				blendName		=		node[0]      	    ,
				namLock         =     	namLock             ,
				povName         =     	nameUPR[6]
			)



kl.arrangeGrp(
				nameGrp		=	nameUPR[3]	,    
				side		=	nameUPR[5]	,
				distanceName=	nameUPR[2]	,
				uprLoc		=	nameUPR[7]	,
				lwrLoc		=	nameUPR[8]	
				
)


kl.arrangeGrp(
				nameGrp		=	nameLWR[3]	,    
				side		=	nameLWR[5]	,
				distanceName=	nameLWR[2]	,
				uprLoc		=	nameLWR[7]	,
				lwrLoc		=	nameLWR[8]	
				
)









# ------------------------  leg RGT
# create distance 
nameUPR = kl.createDistance( 'upperLegRGT_IK_ctrl' , 'legRGT_pov_ctrl' )
nameLWR = kl.createDistance( 'legRGT_pov_ctrl' , 'footRGT_IK_ctrl' )


node = kl.createBlendColor( 
									name = nameUPR[0] 			, 
									uprDistance = nameUPR[2]	, 
									lwrDistance = nameLWR[2] 	,
									uprNam = nameUPR[4]			,
									side =  nameUPR[5]              
								)


namLock = kl.doAddAttr( nameUPR[6] , nameLWR[4])

kl.distJnt( 
				stretchNode		= 	'legStretchRGT_pma' 	, 
				upperJnt		= 	'lowerLegRGT_IK_jnt'	, 
				lowerJnt 		= 	'footRGT_IK_jnt'	 	, 
				blendName		=		node[0]      	    ,
				namLock         =     	namLock             ,
				povName         =     	nameUPR[6]
			)



kl.arrangeGrp(
				nameGrp		=	nameUPR[3]	,    
				side		=	nameUPR[5]	,
				distanceName=	nameUPR[2]	,
				uprLoc		=	nameUPR[7]	,
				lwrLoc		=	nameUPR[8]	
				
)


kl.arrangeGrp(
				nameGrp		=	nameLWR[3]	,    
				side		=	nameLWR[5]	,
				distanceName=	nameLWR[2]	,
				uprLoc		=	nameLWR[7]	,
				lwrLoc		=	nameLWR[8]	
				
)