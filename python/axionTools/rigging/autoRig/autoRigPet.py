# Untimate pet quick rig
# Main Ctrl
import maya.cmds as mc

def MainRig():
    giveFrontRear = 'F','R'
    giveSide = 'LFT','RGT'
    giveMain = 'body','chest','hip','head','nose'
    giveExtra = 'tongue','tail','ear'
    
    for i in range(len(giveMain)):
        name = giveMain[i]
        jnt = name + '_bind_jnt'
        grp = name + '_Zro_grp'
        ctrl = name + '_ctrl'
        
        mc.parentConstraint(jnt,grp,mo=0,w=1)
        mc.delete(grp + '_parentConstraint1')
        if i == 0:
            mc.parent(grp,'Main_RIG')
        if i == 3:
            mc.parent(grp,'chest_ctrl')
        if i == 4:
            mc.parent(grp,'head_ctrl')
        if i > 0:
            mc.parent(grp,'body_ctrl')
        mc.parentConstraint(ctrl,jnt,mo=1,w=1)
        
    giveExtra = 'tongue','tail','ear'
    for i in range(len(giveExtra)):
        if i < 2: # tail 
            for c in range(3):
                name = giveExtra[i] + '0%d'% (c+1)
                jnt = name + '_bind_jnt'
                grp = name + '_Zro_grp'
                ctrl = name + '_ctrl'
                mc.parentConstraint(jnt,grp,mo=0,w=1)
                mc.delete(grp + '_parentConstraint1')
                
                if c == 0:
                    if i == 0:
                        mc.parent(grp,'head_ctrl')
                    if i == 1:
                        mc.parent(grp,'hip_ctrl')
                if c > 0:
                    mc.parent(grp,giveExtra[i] + '0%d'% (c) + '_ctrl')
                mc.parentConstraint(ctrl,jnt)
        if i == 2: # ear chain
            for s in range(len(giveSide)):
                side = giveSide[s]
                for c in range(2):
                    name = giveExtra[i] + '0%d'% (c+1) + side
                    jnt = name + '_bind_jnt'
                    grp = name + '_Zro_grp'
                    ctrl = name + '_ctrl'
                    mc.parentConstraint(jnt,grp,mo=0,w=1)
                    mc.delete(grp + '_parentConstraint1')
                    if c ==0:
                        mc.parent(grp,'head_ctrl')
                    if c > 0:
                        mc.parent(grp,giveExtra[i] + '0%d'% (c) + side + '_ctrl')
                    mc.parentConstraint(ctrl,jnt)
    
    #duplicate IK / FK
    giveSys = 'IK','FK'
    for y in range(len(giveSys)): # IK FK SYS
        sys = '_' + giveSys[y]
        for s in range(len(giveSide)): # SIDE
            side = giveSide[s]
            giveName = 'FLeg','RLeg'
            for f in range(len(giveName)): # NAME
                name = giveName[f] + '01' + side #'0%d'% (i)
                jnt = name + '_bind_jnt'
                ikfk = name + sys + '_jnt'
                mc.duplicate(jnt,n=ikfk)
                mc.parent(ikfk,giveSys[y] + '_jnt_grp')
                mc.select(ikfk,hi=True)
                #mc.select(ikfk,d=True)
                #--------------RENAMER Bind Joint-------------#
                sel = mc.ls(sl=True)
                search = '_bind'
                replace = sys
                for i in range(len(sel)-1,-1,-1):
                    spName = sel[i].split('|')
                    oldName = spName[len(spName)-1]
                    newName = oldName.replace(search, replace)
                    mc.rename(sel[i],newName)
    
    #Snap FK Ctrl
    for s in range(len(giveSide)): # SIDE
        side = giveSide[s]
        giveName = 'FLeg','RLeg'
        for f in range(len(giveName)): # NAME
            for c in range(3):
                name = giveName[f] + '0%d'% (c+1) + side
                jnt = name + '_FK_jnt'
                grp = name + '_FK_Zro_grp'
                ctrl = name + '_FK_ctrl'
                
                mc.parentConstraint(jnt,grp,mo=0,w=1)
                mc.delete(grp + '_parentConstraint1')
                if c == 0:
                    mc.parent(grp,'FK_ctrl_grp')
                    if f == 0:
                        mc.parentConstraint('chest_ctrl',grp,mo=1,w=1)
                    if f == 1:
                        mc.parentConstraint('hip_ctrl',grp,mo=1,w=1)
                if c > 0: # back in to chain
                    mc.parent(grp,giveName[f] + '0%d'% (c) + side + '_FK_ctrl')
                mc.parentConstraint(ctrl,jnt)
                  
    
    #Snap IK Ctrl
    for s in range(len(giveSide)): # SIDE
        side = giveSide[s]
        giveName = 'FLeg','RLeg'
        for i in range(len(giveName)): # NAME
            name = giveName[i] + side
            jnt = giveName[i] + '03' + side + '_IK_jnt'
            topJnt = giveName[i] + '01' + side + '_IK_jnt'
            povJnt = name + '_pov_tmpJnt'
            grp = name + '_IK_Zro_grp'
            povGrp = name + '_pov_Zro_grp'
            ctrl = name + '_IK_ctrl'
            pov = name + '_pov_ctrl'
            null = name + '_IK_null'
            
            mc.group(n=null,em=1)
            
            mc.pointConstraint(jnt,grp,mo=0,w=1)
            mc.pointConstraint(jnt,null,mo=0,w=1)
            mc.pointConstraint(povJnt,povGrp,mo=0,w=1)
            
            mc.delete(grp + '_pointConstraint1')
            mc.delete(null + '_pointConstraint1')
            mc.delete(povGrp + '_pointConstraint1')
    
            mc.parent(grp,'IK_ctrl_grp')
            mc.parent(null,ctrl)
            mc.parent(povGrp,ctrl)
            if i == 0:
                mc.parentConstraint('chest_ctrl',topJnt,mo=1,w=1)
            if i == 1:
                mc.parentConstraint('hip_ctrl',topJnt,mo=1,w=1)
    #IK FK FOR PET Setup
    for s in range(len(giveSide)): # Side Loop
        side = giveSide[s]
        for f in range(len(giveFrontRear)):
            # Naming
            front = giveFrontRear[f]
            giveName = ('Leg01','Leg02','Leg03')
            leg = front + 'Leg' + side
            sJnt = front + giveName[0] + side + '_IK_jnt'
            eJnt = front + giveName[2] + side + '_IK_jnt'
            handle = leg + '_IK_handle'
            IKCtrl = leg + '_IK_ctrl' 
            pov = leg + '_pov_ctrl'
            rev = leg + '_IKFK_rev'
            
            # IKFK Blend from Placement
            mc.createNode('reverse', n = rev)
            giveCor = 'X','Y','Z'
            for n in range(3):
                cor = giveCor[n]
                mc.connectAttr('placement_ctrl.IK_FK_' + leg, rev + '.input.input' + cor)
            
            #mc.connectAttr(rev + '.input.input' + cor)
    
            # Leg IK
            mc.ikHandle( n = handle, sj = sJnt, ee = eJnt, sol = 'ikRPsolver' )
            mc.poleVectorConstraint (pov, handle, w = 1)
            mc.parent(handle,IKCtrl)
            
            # Leg IK Rotate
            ikRotCtrl = leg + '_IK_ctrl'
            ikRotJnt = front + giveName[2] + side + '_IK_jnt'
            mc.orientConstraint(ikRotCtrl, ikRotJnt, w=1, mo=1)
            
            for i in range(len(giveName)) : # Cons > FK
                name = front+giveName[i]+side
                # Naming
                ikJnt = name + '_IK_jnt'
                fkJnt = name + '_FK_jnt'
                fkCtrl = name + '_FK_ctrl'
                # Constraint
                mc.parentConstraint(fkCtrl, fkJnt, w=1, mo=1)
                
            for i in range(len(giveName)) : # Cons > Bind
                name = front+giveName[i]+side
                # Naming
                bindJnt = name + '_bind_jnt'
                ikJnt = name + '_IK_jnt'
                fkJnt = name + '_FK_jnt'
                ikpConAttr = bindJnt + '_parentConstraint1' + '.' + ikJnt + 'W0'
                fkpConAttr = bindJnt + '_parentConstraint1' + '.' + fkJnt + 'W1'
                # Constraint
                mc.parentConstraint(ikJnt, fkJnt, bindJnt, w=0, mo=1)
                
                cor = giveCor[i]
                mc.connectAttr(rev + '.output.output' + cor, ikpConAttr)
                mc.connectAttr(rev + '.input.input' + cor, fkpConAttr)

    # POV parent LOCAL WORLD
    for s in range(len(giveSide)):
        side = giveSide[s]
        for f in range(len(giveFrontRear)):
            name = giveFrontRear[f] + 'Leg' + side + '_pov' # Call Damm Pov
            pov = name + '_ctrl'
            mc.select(pov)
            mc.pickWalk(d='up')
            upSel = mc.pickWalk(d='up')
            upGrp = upSel[0]
            grp = name + '_Zro_grp'
            wor = name + '_Wor_grp'
            loc = name + '_Loc_grp'
            rev = name + '_rev'
            pConAttrLoc = grp + '_parentConstraint1' + '.' + loc + 'W0'
            pConAttrWor = grp + '_parentConstraint1' + '.' + wor + 'W1'
    
                
            mc.group(em=1,n=wor)
            mc.group(em=1,n=loc)
            mc.createNode('reverse',n=rev)
            
            mc.parentConstraint(grp,wor,w=1,mo=0)
            mc.parentConstraint(grp,loc,w=1,mo=0)
            mc.delete(wor + '_parentConstraint1')
            mc.delete(loc + '_parentConstraint1')
            
            mc.parent(wor,loc,upGrp)
            # World to wor
            mc.parentConstraint('fly_ctrl',wor,w=1,mo=1)
            # Parent to GRP
            mc.parentConstraint(loc,wor,grp,w=0,mo=1)
            # Connect povCtrl to rev to pCon
            mc.connectAttr(pov + '.localWorld', rev + '.inputX')
            mc.connectAttr(rev + '.outputX', pConAttrLoc)
            mc.connectAttr(rev + '.inputX', pConAttrWor)





def AutoStrech():
    #AutoStrech IK For Dog
    stchGrp = 'stretch_RIG'
    mc.group( n = stchGrp, em=1)
    mc.parent(stchGrp,'NOTOUCH_GRP')
    givePart = 'F','R'
    for p in range(len(givePart)):
        part = givePart[p] + 'Leg'
        giveSide = 'LFT','RGT'
        for i in range(len(giveSide)):
            side = giveSide[i]
            #NAMING AND STUFF
            giveName = 'Leg01','Leg02','Leg03'
            # >>>>>  CHANGE ONLY THIS PART <<<<<
            # 3 Joint Name
            strJnt = part + '01' + side + '_IK_jnt'
            midJnt = part + '02' + side + '_IK_jnt'
            endJnt = part + '03' + side + '_IK_jnt'
            # Get joint position
            strJntTy = mc.getAttr( midJnt + '.ty')
            endJntTy = mc.getAttr( endJnt + '.ty')
            if side == 'LFT':
                disJnt = strJntTy + endJntTy
                ampVal = 0.1
            if side == 'RGT':
                disJnt = (strJntTy + endJntTy)*(-1)
                ampVal = (-0.1)
            # 3 Ctrl Name
            strPoint = part + '01' + side + '_IK_jnt'
            endPoint = part + side + '_IK_null'
            IKCtrl = part + side + '_IK_ctrl'
            # Start End Loc Name
            strLoc = part + 'StartDist' + side + '_loc'
            endLoc = part + 'EndDist' + side + '_loc'
            # Node System
            disNode = part + 'AutoStretch' + side + '_dtw'
            mdvAutoNode = part + 'AutoStretch' + side + '_mdv'
            mdvNode = part + 'Stretch' + side + '_mdv'
            mdvAmpNode = part + 'StretchAmp' + side + '_mdv'
            cndNode = part + 'AutoStretch' + side +  '_cnd'
            bcNode = part + 'AutoStretch' + side + '_bc'
            pmaNode = part + 'Stretch' + side + '_pma'
            minusNode = part + 'MinuseStretch' + side + '_mdv'
            
            #Create Locator
            mc.spaceLocator(n = strLoc)
            mc.spaceLocator(n = endLoc)
            mc.setAttr( strLoc + '.v', 0)
            mc.setAttr( endLoc + '.v', 0)
            mc.parent(strLoc,stchGrp)
            mc.parent(endLoc,stchGrp)
            #SnapLocator
            mc.pointConstraint( strPoint, strLoc, mo=0, w=1)
            mc.pointConstraint( endPoint, endLoc, mo=0, w=1)
            #measurement
            mc.createNode('distanceBetween', n = disNode)
            mc.connectAttr( strLoc + 'Shape.worldPosition', disNode + '.point1')
            mc.connectAttr( endLoc + 'Shape.worldPosition', disNode + '.point2')
            #Create AutoStretch_mdv and Set
            mc.createNode('multiplyDivide', n = mdvAutoNode)
            mc.setAttr( mdvAutoNode + '.operation', 2)
            mc.setAttr( mdvAutoNode + '.input2.input2X', disJnt)
            #connect
            mc.connectAttr( disNode + '.distance', mdvAutoNode + '.input1.input1X ')
            
            #Create legAutoStretch_cnd
            mc.createNode('condition', n = cndNode)
            mc.setAttr( cndNode + '.operation', 3)
            mc.setAttr( cndNode + '.secondTerm', disJnt)
            #connect
            mc.connectAttr( mdvAutoNode + '.output.outputX', cndNode + '.colorIfTrue.colorIfTrueR')
            mc.connectAttr( disNode + '.distance', cndNode + '.firstTerm')
            
            #Create legStretchLFT_mdv and Set
            mc.createNode('multiplyDivide', n = mdvNode)
            mc.setAttr ( mdvNode + '.operation', 1)
            mc.setAttr( mdvNode + '.input2.input2X', strJntTy)
            mc.setAttr( mdvNode + '.input2.input2Y', endJntTy)
            #connect
            mc.connectAttr( cndNode + '.outColor.outColorR', mdvNode + '.input1.input1X')
            mc.connectAttr( cndNode + '.outColor.outColorR', mdvNode + '.input1.input1Y')
            
            #Create legStretchLFT_mdv and Set
            mc.createNode('multiplyDivide', n = mdvAmpNode)
            mc.setAttr( mdvAmpNode + '.input2X', ampVal)
            mc.setAttr( mdvAmpNode + '.input2Y', ampVal)
            #connect
            mc.connectAttr( IKCtrl + '.lowStretch', mdvAmpNode + '.input1.input1Y')
            mc.connectAttr( IKCtrl + '.upStretch', mdvAmpNode + '.input1.input1X')
            
            #Create blendColors
            mc.createNode('blendColors', n = bcNode)
            mc.setAttr( bcNode + '.color2R', strJntTy)
            mc.setAttr( bcNode + '.color2G', endJntTy)
            #connect
            mc.connectAttr( mdvNode + '.output', bcNode + '.color1')
            mc.connectAttr( IKCtrl + '.autoStretch', bcNode + '.blender')
            
            #Create legStretchLFT_pma
            mc.createNode('plusMinusAverage', n = pmaNode)
            #connect KEY bc
            mc.connectAttr( bcNode + '.outputR', pmaNode + '.input2D[1].input2Dx')
            mc.connectAttr( bcNode + '.outputG', pmaNode + '.input2D[1].input2Dy')
            #connect KEY amp
            mc.connectAttr( mdvAmpNode + '.outputX', pmaNode + '.input2D[2].input2Dx')
            mc.connectAttr( mdvAmpNode + '.outputY', pmaNode + '.input2D[2].input2Dy')
            
    
            mc.connectAttr ( pmaNode + '.output2D.output2Dx', midJnt + '.ty')
            mc.connectAttr ( pmaNode + '.output2D.output2Dy', endJnt + '.ty')
            



def DeleteTmpJnt():
    mc.delete ('TMP_GRP')
    tmpAmount = len(mc.ls('*tmpJnt'))
    if tmpAmount > 0:
        tmp = mc.select('*tmpJnt','ExtraJoint_grp')
        mc.delete()
        print 'Temp Joint Clear'
    else :
        print '>>> Temp Joint Is No More <<<'

MainRig()
AutoStrech()
DeleteTmpJnt()
mc.select(cl=1)