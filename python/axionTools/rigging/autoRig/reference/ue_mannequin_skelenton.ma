//Maya ASCII 2018 scene
//Name: ue_mannequin_skelenton.ma
//Last modified: Mon, Apr 04, 2022 11:42:36 AM
//Codeset: 1252
requires maya "2018";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "27B089B6-4D1F-3EFC-C9E4-5CBA4574706B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -90.880534939696929 139.26615990202347 146.50538869486473 ;
	setAttr ".r" -type "double3" -12.93835272960319 -35.799999999999407 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8117EDC6-43AB-0835-B987-69914DA9EB99";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 209.789478075072;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "D27232CE-4E07-2C6F-B526-7A972AB09584";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BF12A0AD-4A93-9036-CA57-EEA153B1B97B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "8ACAEF77-4954-F943-DE14-0D92A0524CCC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "CDB52BF1-4903-67D7-D25B-FF944C655D27";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "D4B2FFA9-4777-F473-7AF6-B19F2E5E0454";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E4691C5F-4860-2013-BB9E-EF861B12C496";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "root";
	rename -uid "3AECE8B5-43C9-844D-3EF7-EABFABBDD253";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".r" -type "double3" -1.2722218725854067e-14 0 0 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0 -1 0 0 1 0 0 0 0 0 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 2;
createNode joint -n "pelvis" -p "root";
	rename -uid "5E08815E-4E04-BD1D-596A-4D9E15F68236";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 1.3536841578012896e-28 1.0561532974243379 96.750602722167969 ;
	setAttr ".r" -type "double3" 89.9981155395526 -89.790634155273452 -89.9981155395526 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.2018400000000001e-07 0.99999300000000002 0.0036541199999999999 0
		 2.1958400000000001e-10 0.0036541199999999999 -0.99999300000000002 0 -1 1.2018400000000001e-07 2.1958400000000001e-10 0
		 0 96.750602999999998 -1.0561529999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "spine_01" -p "pelvis";
	rename -uid "27FC86FC-4C8E-BBA2-38B3-0F8AA23386EA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.808877944946303 -0.8514151573181179 -6.1043446976361514e-13 ;
	setAttr ".r" -type "double3" 3.6147667604251203e-15 7.1771005146841367e-15 -7.1538553237915208 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.1922100000000001e-07 0.99175400000000002 0.128159 0
		 1.5184899999999998e-08 0.128159 -0.99175400000000002 0 -1 1.2018400000000001e-07 2.1958400000000001e-10 0
		 1.29887e-06 107.556297 -0.165247 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "spine_02" -p "spine_01";
	rename -uid "48643E3B-4361-1ED9-11CB-C7A94A962F92";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 18.87534904479979 3.8011586666107124 5.9660931846326821e-08 ;
	setAttr ".r" -type "double3" 5.2825190368735666e-15 6.5537772274502669e-15 14.063553810119634 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.1933800000000001e-07 0.99317 -0.116677 0 -1.42407e-08 -0.116677 -0.99317 0
		 -1 1.2018400000000001e-07 2.1958400000000001e-10 0 3.5472700000000003e-06 126.76314600000001 -1.516014 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "spine_03" -p "spine_02";
	rename -uid "91DF978B-405A-F5FF-0E4B-54AF2B97E289";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 13.40732860565187 0.42047739028930664 -5.5901874146705669e-13 ;
	setAttr ".r" -type "double3" 5.560265338287921e-15 5.9801232088215152e-15 2.7794167995452859 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.18507e-07 0.986344 -0.16469900000000001 0 -2.0010799999999999e-08 -0.16469900000000001 -0.986344 0
		 -1 1.2018400000000001e-07 2.1958400000000001e-10 0 5.1412799999999997e-06 140.029842 -3.4979399999999998 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "clavicle_l" -p "spine_03";
	rename -uid "D14235D3-4710-BDD4-D85D-0A867A0859B8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 11.883687973022461 -2.7320878505706752 -3.7819831371307377 ;
	setAttr ".r" -type "double3" 108.71916198730469 61.853576660156236 101.54093170166016 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.881745 -0.169211 -0.440334 0 -0.446774 1.66167e-05 -0.89464699999999997 0
		 0.151391 0.98558000000000001 -0.075584200000000004 0 3.78199 152.20121700000001 -2.7603930000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "upperarm_l" -p "clavicle_l";
	rename -uid "A607C6AC-4792-C75E-E12B-9D976B57F697";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 15.784872055053707 1.4317955709941543e-09 6.3591301113774534e-09 ;
	setAttr ".r" -type "double3" 7.6739015579223482 40.300533294677741 -17.021003723144553 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.64483999999999997 -0.76087199999999999 -0.072499800000000003 0
		 -0.068049100000000001 0.037325999999999998 -0.99698299999999995 0 0.76128300000000004 0.64782799999999996 -0.0277074 0
		 17.700220999999999 149.530248 -9.7110020000000006 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "lowerarm_l" -p "upperarm_l";
	rename -uid "17468EF3-4F34-76F8-15A6-BBB02E1E7BFD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 30.339929580688477 8.407477380956152e-09 3.197527576048742e-09 ;
	setAttr ".r" -type "double3" -3.6132805347442596 -10.397336959838896 -30.360864639282223 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.71849099999999999 -0.54738500000000001 0.42911500000000002 0
		 0.226213 -0.399509 -0.88838099999999998 0 0.65772200000000003 0.73536500000000005 -0.163218 0
		 37.264611000000002 126.445457 -11.910640000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "hand_l" -p "lowerarm_l";
	rename -uid "AEF9FB6E-4002-8E4D-4819-36A2111453AF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 26.975143432617212 1.5729746394299582e-09 -9.6207202204823261e-09 ;
	setAttr ".r" -type "double3" -76.356178283691392 2.4998080730438152 -0.41257095336912925 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000007 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.68747400000000003 -0.57604999999999995 0.44220500000000001 0
		 -0.61435799999999996 -0.78602099999999997 -0.068818900000000002 0 0.38722600000000001 -0.224361 -0.89427000000000001 0
		 56.646011000000001 111.67965599999999 -0.3352 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "index_01_l" -p "hand_l";
	rename -uid "458485F2-4E10-DCD8-B459-6D9643F63EC5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 12.068114280700668 1.7634615898132466 -2.1093976497650124 ;
	setAttr ".r" -type "double3" 14.866970062255842 -3.7637891769409246 25.53691482543945 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.38012000000000001 -0.87149799999999999 0.30983899999999998 0
		 -0.72907500000000003 -0.48845300000000003 -0.47943999999999998 0 0.56917300000000004 -0.043650700000000001 -0.82105799999999995 0
		 63.042318000000002 103.81496799999999 6.7663960000000003 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "index_02_l" -p "index_01_l";
	rename -uid "46CDA4C5-47BD-E4E4-EFB2-DCA430692482";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.2874979972839142 -2.9850326654923265e-08 5.0484167957165482e-09 ;
	setAttr ".r" -type "double3" 1.3378193378448411 -0.47529235482215576 11.986148834228528 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.225136 -0.95426599999999995 0.19669800000000001 0
		 -0.77866000000000002 -0.297568 -0.552396 0 0.58566399999999996 -0.0287964 -0.81004200000000004 0
		 64.672083000000001 100.078422 8.0948290000000007 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "index_03_l" -p "index_02_l";
	rename -uid "1838CD86-4842-F3D3-ECAF-1AA86E91F366";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.3937902450561523 1.1695433954628243e-08 -2.3492283673931524e-09 ;
	setAttr ".r" -type "double3" 1.1373670101165791 0.99726903438568082 -9.496332168579114 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.34027099999999999 -0.891459 0.29919400000000002 0
		 -0.71895699999999996 -0.45172000000000001 -0.52825100000000003 0 0.60606599999999999 -0.035359300000000003 -0.794628 0
		 65.436147000000005 96.839843000000002 8.7623789999999993 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "middle_01_l" -p "hand_l";
	rename -uid "51C8839E-498A-86E5-4148-A09EFF839DAF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 12.244280815124501 1.293643593788147 0.57116198539734375 ;
	setAttr ".r" -type "double3" 1.917851328849782 -7.0405654907226634 22.82585525512696 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43979000000000001 -0.85705799999999999 0.26839400000000002 0
		 -0.82123299999999999 -0.504741 -0.26610800000000001 0 0.36353999999999997 -0.103382 -0.92582500000000001 0
		 64.490046000000007 103.48136 4.4794869999999998 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "middle_02_l" -p "middle_01_l";
	rename -uid "6A9DB681-4825-9929-1BA9-91ADCFA1CFDD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.6403741836547923 -3.6481964116319432e-09 1.8308607963035683e-09 ;
	setAttr ".r" -type "double3" -2.0249526500701882 1.136837124824529 12.280713081359867 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999978 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.24778600000000001 -0.94256899999999999 0.22397900000000001 0
		 -0.908447 -0.30638399999999999 -0.28434599999999999 0 0.33663900000000002 -0.133016 -0.93219200000000002 0
		 66.530837000000005 99.504288000000003 5.7249359999999996 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "middle_03_l" -p "middle_02_l";
	rename -uid "F0290873-4B30-F35B-7EEF-37A3A183FA2E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.6488435268401958 -1.998942877889931e-08 1.6076384667940147e-09 ;
	setAttr ".r" -type "double3" 0.78144752979278087 -4.3899536132812536 -15.39975643157959 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000007 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.50448899999999997 -0.83511999999999997 0.21923699999999999 0
		 -0.80587900000000001 -0.54657900000000004 -0.22761899999999999 0 0.30991999999999997 -0.061847100000000002 -0.94874899999999995 0
		 67.434971000000004 96.065003000000004 6.5422000000000002 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "pinky_01_l" -p "hand_l";
	rename -uid "46A6F5CE-4FA2-2132-1D12-859C432194FD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.140665054321261 2.2631511688232564 4.6431479454040492 ;
	setAttr ".r" -type "double3" -18.724597930908214 -18.933965682983409 20.185865402221687 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53545799999999999 -0.84076899999999999 0.079954300000000006 0
		 -0.84322399999999997 -0.52688299999999999 0.106617 0 -0.047513600000000003 -0.12450799999999999 -0.99107999999999996 0
		 64.025017000000005 103.0175 -0.158918 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "pinky_02_l" -p "pinky_01_l";
	rename -uid "9C5EE40A-46D2-06B4-632C-2B9C4ECBBA4C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.5709807872772359 1.8669865653464512e-08 3.9181458078019205e-10 ;
	setAttr ".r" -type "double3" 1.0638334751129355 -1.3156856298446564 11.208059310913093 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000007 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36015999999999998 -0.929759 0.076370400000000005 0
		 -0.93209600000000004 -0.355267 0.070589100000000002 0 -0.038498999999999999 -0.096607899999999997 -0.99457799999999996 0
		 65.937128000000001 100.015131 0.12659799999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "pinky_03_l" -p "pinky_02_l";
	rename -uid "8B070777-4CE3-9572-6B9E-6CADB7E96FAB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 2.9856307506561137 -3.0058771471885848e-08 -4.0375436327622083e-09 ;
	setAttr ".r" -type "double3" 0.44569900631904397 3.8696639537811333 1.0389990806579421 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.34501500000000002 -0.92739499999999997 0.14458199999999999 0
		 -0.93856399999999995 -0.33957999999999999 0.061512400000000002 0 -0.0079490499999999992 -0.15692200000000001 -0.98757899999999998 0
		 67.012433000000001 97.239213000000007 0.35461199999999998 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ring_01_l" -p "hand_l";
	rename -uid "8CE07E41-4BD0-0593-22F8-2B939A501EA5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 11.497884750366191 1.7535265684127808 2.846912384033204 ;
	setAttr ".r" -type "double3" -13.510274887084972 -10.989254951477058 23.292085647583026 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.455208 -0.86727699999999996 0.20153499999999999 0
		 -0.884494 -0.46645500000000001 -0.0095143399999999996 0 0.102259 -0.173925 -0.97943499999999994 0
		 64.575614000000002 103.039254 2.082643 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ring_02_l" -p "ring_01_l";
	rename -uid "0631256D-46B0-4E24-A907-CF9812E3D42B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.4301772117614888 4.6665036279591732e-09 -9.4033225650491659e-10 ;
	setAttr ".r" -type "double3" 0.30135601758957292 -1.6697421073913523 13.315453529357912 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.24213899999999999 -0.95605600000000002 0.16530400000000001 0
		 -0.96504100000000004 -0.254936 -0.060852700000000003 0 0.10032099999999999 -0.144791 -0.98436299999999999 0
		 66.592264999999998 99.197063 2.9754779999999998 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ring_03_l" -p "ring_02_l";
	rename -uid "B0D7939E-4D7A-B0D5-A60C-CBAE496DEAFB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.4766523838043213 -1.6786373180366354e-08 2.7686493098144638e-09 ;
	setAttr ".r" -type "double3" -0.36076423525810208 2.987668037414545 -12.899674415588393 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44562499999999999 -0.86627900000000002 0.22578599999999999 0
		 -0.88739100000000004 -0.46072999999999997 -0.016281 0 0.11813 -0.193105 -0.97404100000000005 0
		 67.434096999999994 95.873187999999999 3.5501830000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thumb_01_l" -p "hand_l";
	rename -uid "76D54814-4DB1-0C7C-85D6-E9B70AD43EA3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.7620363235473491 2.3749806880950928 -2.5378196239471431 ;
	setAttr ".r" -type "double3" 95.069137573242173 36.918972015380845 27.056173324584964 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.033459099999999999 -0.56121399999999999 0.82699400000000001 0
		 0.58346200000000004 -0.66084900000000002 -0.47207100000000002 0 0.81145100000000003 0.49831500000000001 0.305336 0
		 57.477991000000003 107.639089 3.8766500000000002 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thumb_02_l" -p "thumb_01_l";
	rename -uid "E1768314-4A8F-727C-BC66-75ADCD4C1453";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 3.8696718215942454 5.0118629246753699e-09 9.9849586376876687e-09 ;
	setAttr ".r" -type "double3" 1.6131404638290638 9.8332386016845525 15.1513223648071 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.043499799999999998 -0.78903800000000002 0.61280199999999996 0
		 0.57761200000000001 -0.48061100000000001 -0.65983199999999997 0 0.81515199999999999 0.382664 0.43485099999999999 0
		 57.607466000000002 105.467375 7.0768459999999997 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thumb_03_l" -p "thumb_02_l";
	rename -uid "0D575AA8-46DA-14FF-6280-3BA37D71D788";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 4.0621714591979838 1.0722018828346336e-09 -5.1272763812448829e-10 ;
	setAttr ".r" -type "double3" 2.4147641658782866 0.47919920086861961 -12.385654449462885 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.088219599999999995 -0.67076499999999994 0.73640499999999998 0
		 0.60730399999999995 -0.62221199999999999 -0.49399700000000002 0 0.78955600000000004 0.403642 0.46224999999999999 0
		 57.784170000000003 102.26216599999999 9.5661529999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "lowerarm_twist_01_l" -p "lowerarm_l";
	rename -uid "5D87B76B-48D5-7C65-16DA-02B0FCC60330";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 14.000000000000021 1.0658141036401503e-14 4.2632564145606011e-14 ;
	setAttr ".r" -type "double3" 4.7708320221952728e-15 -6.3611093629270312e-15 9.5416640443905456e-15 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.71849099999999999 -0.54738500000000001 0.42911500000000002 0
		 0.226213 -0.399509 -0.88838099999999998 0 0.65772200000000003 0.73536500000000005 -0.163218 0
		 47.323486000000003 118.782061 -5.9030290000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "upperarm_twist_01_l" -p "upperarm_l";
	rename -uid "6F979A9A-49D5-B34F-010E-688385200C09";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 0.49999999999998579 -1.7763568394002505e-15 0 ;
	setAttr ".r" -type "double3" -8.3489560388417319e-15 -2.6140183788278279e-14 1.2722218725854065e-14 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.64483999999999997 -0.76087199999999999 -0.072499800000000003 0
		 -0.068049100000000001 0.037325999999999998 -0.99698299999999995 0 0.76128300000000004 0.64782799999999996 -0.0277074 0
		 18.022639999999999 149.149812 -9.7472519999999996 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "clavicle_r" -p "spine_03";
	rename -uid "53EE366A-46AC-0B7C-B285-7FBABE8AF6E9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 11.883789062499972 -2.7321023941040039 3.7820026874542219 ;
	setAttr ".r" -type "double3" 108.71916198730466 61.85357666015625 -78.459037780761761 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.881745 0.169211 0.44033299999999997 0 -0.446774 -1.60328e-05 0.89464699999999997 0
		 0.151391 -0.98558000000000001 0.075584700000000005 0 -3.7819959999999999 152.20132000000001 -2.7603949999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "upperarm_r" -p "clavicle_r";
	rename -uid "BB3EE2CF-4923-98F5-753C-6EB047714FB0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -15.784797668457033 -7.0139599359642091e-06 -1.1171471697934976e-05 ;
	setAttr ".r" -type "double3" 7.6739015579223446 40.300533294677741 -17.021003723144549 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999989 0.99999999999999967 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.64483999999999997 0.76087099999999996 0.072499400000000006 0
		 -0.068049100000000001 -0.037325499999999998 0.99698399999999998 0 0.76128200000000001 -0.64782799999999996 0.027707699999999998 0
		 -17.70016 149.53037399999999 -9.7109769999999997 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "lowerarm_r" -p "upperarm_r";
	rename -uid "133EB19D-427E-8B1D-7084-678FA3728812";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -30.340049743652358 -4.0850213789411782e-06 1.7513537216018449e-06 ;
	setAttr ".r" -type "double3" -3.6132805347442654 -10.397336959838865 -30.360864639282223 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.71849099999999999 0.54738500000000001 -0.42911500000000002 0
		 0.226213 0.39950999999999998 0.88837999999999995 0 0.65772200000000003 -0.73536500000000005 0.163219 0
		 -37.264631999999999 126.445494 -11.910615999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "hand_r" -p "lowerarm_r";
	rename -uid "512E1313-4A8D-240B-D90E-7FB3B4F2EC26";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -26.975244522094705 2.5634119097617258e-05 -1.1905384695864996e-06 ;
	setAttr ".r" -type "double3" -76.356178283691392 2.4998080730438224 -0.41257095336915173 ;
	setAttr ".s" -type "double3" 1 0.99999999999999944 1.0000000000000004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.68747400000000003 0.57604999999999995 -0.44220599999999999 0
		 -0.61435799999999996 0.78602099999999997 0.068818400000000002 0 0.38722600000000001 0.22436200000000001 0.89427000000000001 0
		 -56.646102999999997 111.67966 -0.33510200000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "index_01_r" -p "hand_r";
	rename -uid "034A97CB-4699-DDC4-B309-8BA770479CB6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -12.067941665649421 -1.7637253999710083 2.1094281673431419 ;
	setAttr ".r" -type "double3" 14.866970062255861 -3.7637891769409069 25.536914825439467 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999967 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.38012000000000001 0.87149799999999999 -0.30983899999999998 0
		 -0.72907500000000003 0.48845300000000003 0.47943999999999998 0 0.56917300000000004 0.043651000000000002 0.82105799999999995 0
		 -63.042119999999997 103.814876 6.7664309999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "index_02_r" -p "index_01_r";
	rename -uid "7BC68A91-4A2E-B95D-B7FA-A68472EA51C8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -4.2876882553100515 9.2459835286717862e-05 -7.4262097765398494e-05 ;
	setAttr ".r" -type "double3" 1.3378193378448413 -0.47529235482215715 11.986148834228526 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999967 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.225136 0.95426599999999995 -0.19669800000000001 0
		 -0.77866000000000002 0.29756899999999997 0.552396 0 0.58566399999999996 0.028796700000000001 0.81004200000000004 0
		 -64.672067999999996 100.07820700000001 8.0949080000000002 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "index_03_r" -p "index_02_r";
	rename -uid "AC8F36F5-4722-3BE1-2E72-1AB874BE78EC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -3.3937981128692485 0.00012069699005223811 -1.2407956095472628e-05 ;
	setAttr ".r" -type "double3" 1.137367010116572 0.99726903438568415 -9.4963321685791175 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.34027099999999999 0.89145799999999997 -0.29919499999999999 0
		 -0.71895699999999996 0.45172099999999998 0.52825100000000003 0 0.60606599999999999 0.035359500000000002 0.794628 0
		 -65.436234999999996 96.839657000000003 8.762518 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "middle_01_r" -p "hand_r";
	rename -uid "9CED0459-4C7A-844C-7885-988B2A7A7804";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -12.244112968444831 -1.2937241792678691 -0.5711302161216687 ;
	setAttr ".r" -type "double3" 1.9178513288498 -7.0405654907226447 22.82585525512696 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.43979099999999999 0.85705799999999999 -0.26839400000000002 0
		 -0.82123299999999999 0.504741 0.26610800000000001 0 0.36353999999999997 0.103383 0.92582399999999998 0
		 -64.489963000000003 103.481409 4.4795379999999998 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "middle_02_r" -p "middle_01_r";
	rename -uid "A519F98F-4D48-3067-F262-118FDB32F741";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -4.6405687332153249 -0.00014491056208498776 7.6369551553767678e-06 ;
	setAttr ".r" -type "double3" -2.0249526500701802 1.1368371248245235 12.280713081359879 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000002 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.24778700000000001 0.94256799999999996 -0.22397900000000001 0
		 -0.908447 0.30638500000000002 0.28434500000000001 0 0.33663900000000002 0.133017 0.93219099999999999 0
		 -66.530719000000005 99.504098999999997 5.7250100000000002 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "middle_03_r" -p "middle_02_r";
	rename -uid "FAF85DD7-415F-4A69-0694-3A96B1D96957";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -3.6489090919494913 3.2996809736118848e-05 -2.2666431496176642e-06 ;
	setAttr ".r" -type "double3" 0.78144752979277188 -4.3899536132812607 -15.399756431579586 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000002 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.50448899999999997 0.83511999999999997 -0.21923699999999999 0
		 -0.80587900000000001 0.54657999999999995 0.22761899999999999 0 0.30991999999999997 0.0618475 0.94874899999999995 0
		 -67.434900999999996 96.064762999999999 6.5422969999999996 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "pinky_01_r" -p "hand_r";
	rename -uid "55D9362B-45B8-FF4B-342C-AA8F54A7FD7C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -10.14059257507324 -2.2633547782897665 -4.643094539642326 ;
	setAttr ".r" -type "double3" -18.724597930908203 -18.933965682983381 20.185865402221708 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53545799999999999 0.84076799999999996 -0.079954800000000006 0
		 -0.84322399999999997 0.52688299999999999 -0.106617 0 -0.047513600000000003 0.12450899999999999 0.99107999999999996 0
		 -64.024916000000005 103.017399 -0.15881300000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "pinky_02_r" -p "pinky_01_r";
	rename -uid "4D0761C1-4C7E-6938-BF50-22BC7EF0F7C5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -3.5710570812225484 -7.8019678781515722e-05 -8.107984291072512e-06 ;
	setAttr ".r" -type "double3" 1.0638334751129153 -1.3156856298446618 11.208059310913086 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.36015999999999998 0.929759 -0.076370900000000005 0
		 -0.93209600000000004 0.355267 -0.070589299999999994 0 -0.038498999999999999 0.0966085 0.99457799999999996 0
		 -65.937002000000007 100.01492500000001 0.12670999999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "pinky_03_r" -p "pinky_02_r";
	rename -uid "50D2A2BD-4D83-DC18-EBFB-5FB634F66B51";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -2.9854192733764791 0.00031727668827841171 -3.5056928025412049e-05 ;
	setAttr ".r" -type "double3" 0.4457004666328454 3.8696639537811257 1.0389991998672383 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999989 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.34501500000000002 0.92739400000000005 -0.14458199999999999 0
		 -0.93856300000000004 0.33957999999999999 -0.061512499999999998 0 -0.0079489899999999995 0.15692300000000001 0.98757899999999998 0
		 -67.012525999999994 97.239312999999996 0.35465200000000002 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ring_01_r" -p "hand_r";
	rename -uid "BEE66DFC-4746-C0D3-03AE-EABD4ACF01DF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -11.49797344207764 -1.7537660598754741 -2.8469147682189875 ;
	setAttr ".r" -type "double3" -13.510274887084956 -10.98925495147704 23.292085647583026 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.455208 0.86727699999999996 -0.20153499999999999 0
		 -0.884494 0.46645500000000001 0.0095140999999999993 0 0.102259 0.173926 0.97943499999999994 0
		 -64.575622999999993 103.03902100000001 2.082767 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ring_02_r" -p "ring_01_r";
	rename -uid "B03CE22A-4E42-9DB9-A853-90A58FCA3EB8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -4.4298644065856863 8.4479885117616504e-05 -1.8378697857812654e-05 ;
	setAttr ".r" -type "double3" 0.30135601758956898 -1.6697421073913632 13.315453529357908 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.24213899999999999 0.95605600000000002 -0.16530500000000001 0
		 -0.96504100000000004 0.254936 0.0608526 0 0.10032099999999999 0.144791 0.98436299999999999 0
		 -66.592209999999994 99.197139000000007 2.9755229999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ring_03_r" -p "ring_02_r";
	rename -uid "3C6C49DD-459E-BF67-FD11-B0B1E0B50D5C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -3.4766588211059428 7.194164207646736e-05 -2.8431277332430227e-06 ;
	setAttr ".r" -type "double3" -0.36076423525810414 2.9876680374145548 -12.899674415588393 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.44562600000000002 0.86627799999999999 -0.22578599999999999 0
		 -0.88739100000000004 0.46072999999999997 0.016280699999999999 0 0.11813 0.193106 0.97404100000000005 0
		 -67.434113999999994 95.873276000000004 3.550233 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thumb_01_r" -p "hand_r";
	rename -uid "5C2B1B35-4101-B749-39BB-79BF3D2959D0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -4.7621245384216415 -2.3751199245452881 2.5378017425537096 ;
	setAttr ".r" -type "double3" 95.069137573242188 36.918972015380845 27.056173324584957 ;
	setAttr ".s" -type "double3" 1 0.99999999999999911 1.0000000000000004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.033459200000000001 0.56121399999999999 -0.82699400000000001 0
		 0.58346299999999995 0.66084900000000002 0.47206999999999999 0 0.81145 -0.49831500000000001 -0.305336 0
		 -57.478065999999998 107.638931 3.8767640000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thumb_02_r" -p "thumb_01_r";
	rename -uid "D4A8CD7F-4AE8-E624-20B6-2E9B5257ED2B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -3.8695690631866455 0.00011357050243532285 5.5954889845111211e-05 ;
	setAttr ".r" -type "double3" 1.6131424903869553 9.8332386016845845 15.151324272155753 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999956 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0435001 0.78903800000000002 -0.61280299999999999 0
		 0.57761200000000001 0.48061100000000001 0.65983199999999997 0 0.81515199999999999 -0.38266499999999998 -0.43485099999999999 0
		 -57.607427000000001 105.46732299999999 7.0769120000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thumb_03_r" -p "thumb_02_r";
	rename -uid "973A209B-470B-B9AD-C258-BC8C1610A7AE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -4.0621762275695659 2.0121733648181817e-06 3.2049592846306041e-06 ;
	setAttr ".r" -type "double3" 2.4147622585296515 0.47919237613677684 -12.385654449462892 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000004 1.0000000000000004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.088219400000000003 0.67076400000000003 -0.73640499999999998 0
		 0.60730499999999998 0.62221199999999999 0.49399700000000002 0 0.78955600000000004 -0.403642 -0.46224900000000002 0
		 -57.784128000000003 102.262112 9.5662240000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "lowerarm_twist_01_r" -p "lowerarm_r";
	rename -uid "C2AE5A43-467C-EAE0-1F95-D6A754E738B0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -13.999999999999979 2.433194459428023e-05 -6.5783269889152507e-06 ;
	setAttr ".r" -type "double3" -13.510370254516589 1.033680271475643e-14 -3.1805546814635168e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.71849099999999999 0.54738500000000001 -0.42911500000000002 0
		 0.066295000000000007 0.56025100000000005 0.82566499999999998 0 0.69236900000000001 -0.62168199999999996 0.36624699999999999 0
		 -47.323507999999997 118.782118 -5.9029800000000003 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "upperarm_twist_01_r" -p "upperarm_r";
	rename -uid "9922A014-414B-0FD9-0275-F6B18008A663";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -0.5 -3.700793968164362e-06 -1.1559803851923789e-06 ;
	setAttr ".r" -type "double3" -19.951902389526371 9.1440947092076135e-15 3.1805546814635176e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.64483999999999997 0.76087099999999996 0.072499400000000006 0
		 -0.32373800000000003 0.185974 0.92768899999999999 0 0.69236900000000001 -0.62168199999999996 0.36624699999999999 0
		 -18.022580999999999 149.14993899999999 -9.7472300000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "neck_01" -p "spine_03";
	rename -uid "8D33C014-440F-674A-B11F-EFBF7B9877EC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 16.558782577514648 -0.35531756281852012 -5.9659742021895282e-08 ;
	setAttr ".r" -type "double3" 2.5479867198969077e-15 8.981716354340843e-15 -23.508049011230465 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999967 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.1665300000000001e-07 0.97017600000000004 0.24240100000000001 0
		 2.8919700000000001e-08 0.24240100000000001 -0.97017600000000004 0 -1 1.2018400000000001e-07 2.1958400000000001e-10 0
		 7.1703700000000004e-06 156.42101700000001 -5.874689 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "head" -p "neck_01";
	rename -uid "86BD4CE5-44CF-94D1-5F32-94A968A8D90E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 9.2836132049560547 0.36415687203407998 1.4882622993142233e-15 ;
	setAttr ".r" -type "double3" 4.7479790199914579e-15 7.6103939128666309e-15 15.348649978637695 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.20147e-07 0.99973400000000001 -0.023042400000000001 0
		 -2.98886e-09 -0.023042400000000001 -0.99973400000000001 0 -1 1.2018400000000001e-07 2.1958400000000001e-10 0
		 8.2638599999999995e-06 165.51602800000001 -3.977627 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thigh_l" -p "pelvis";
	rename -uid "DBF77DA3-49C2-5FC5-17A8-5999410B7E24";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -1.4488286972045756 -0.53142350912094016 -9.0058097839355451 ;
	setAttr ".r" -type "double3" 8.5634660720825195 -7.0322942733764551 -1.5154702663421764 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.122429 0.99202800000000002 0.0298731 0 -0.147785 0.011542 -0.98895200000000005 0
		 -0.98141299999999998 -0.12549099999999999 0.14519299999999999 0 9.0058100000000003 95.299841000000001 -0.53002800000000005 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "calf_l" -p "thigh_l";
	rename -uid "F5EDC0DF-44B8-B890-803F-2FB24F66CE02";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -42.572036743164091 1.707427532693373e-10 -4.6678749754391902e-10 ;
	setAttr ".r" -type "double3" -5.7359738349914542 1.7872760295867836 -7.6135840415954616 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.071110499999999993 0.98518899999999998 0.156031 0
		 -0.063531599999999994 0.15163599999999999 -0.98639299999999996 0 -0.99544299999999997 -0.080055799999999996 0.051807800000000001 0
		 14.217846 53.067203999999997 -1.8017860000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "calf_twist_01_l" -p "calf_l";
	rename -uid "6D331D91-49B0-690F-7DBF-E29A05B0F2E3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -20.476776123046875 0 0 ;
	setAttr ".r" -type "double3" 0.32356074452400296 -0.21909196674823669 -0.87298214435577393 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0739403 0.98245099999999996 0.171238 0 -0.070226499999999997 0.16615199999999999 -0.98359600000000003 0
		 -0.99478699999999998 -0.084752800000000003 0.056708799999999997 0 15.673961 32.893706000000002 -4.9967940000000004 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "foot_l" -p "calf_l";
	rename -uid "B757D034-4455-48EB-5131-B6B5B259CA68";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -40.196689605712891 -3.9338505786190581e-09 1.8994228412338998e-10 ;
	setAttr ".r" -type "double3" -0.41538643836974937 3.7049334049224916 8.0595779418945348 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.014825899999999999 0.99980800000000003 0.0128132 0
		 -0.045693999999999999 0.012123699999999999 -0.99888200000000005 0 -0.99884499999999998 -0.0153949 0.045505499999999997 0
		 17.076255 13.465861 -8.0737089999999991 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ball_l" -p "foot_l";
	rename -uid "C25EA07B-4CAD-A148-22A2-E5862287D2FF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -10.453837394714348 -16.577854156494144 0.080155946314334869 ;
	setAttr ".r" -type "double3" 0.0039439606481165386 0.0089543778448371702 -91.883583068847685 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.046312699999999998 -0.044977299999999998 0.99791399999999997 0
		 -0.013384800000000001 0.99886799999999998 0.045641500000000002 0 -0.99883699999999997 -0.015470599999999999 0.045658299999999999 0
		 17.908688000000001 2.8118120000000002 8.3553099999999993 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thigh_twist_01_l" -p "thigh_l";
	rename -uid "49ED47F0-4501-E074-DA7F-F593BF5155DE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -22.094238281250028 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -5.4386773109435982 -0.00021173585264992517 -0.056330300867558906 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12228700000000001 0.99201499999999998 0.030845899999999999 0
		 -0.054220499999999998 0.024355399999999999 -0.99823200000000001 0 -0.99101300000000003 -0.12374300000000001 0.050809300000000002 0
		 11.710777 73.381746000000007 -1.190051 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thigh_r" -p "pelvis";
	rename -uid "7AB48132-4034-BB51-3F1C-EFAF3B92B3CA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" -1.4486445188522197 -0.53142756223678633 9.0058031082153303 ;
	setAttr ".r" -type "double3" 8.563466072082516 -7.0322942733764719 178.4845275878906 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.122429 -0.99202800000000002 -0.0298731 0 -0.147785 -0.011542 0.98895200000000005 0
		 -0.98141299999999998 0.12549099999999999 -0.14519299999999999 0 -9.0058030000000002 95.300027 -0.53002300000000002 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "calf_r" -p "thigh_r";
	rename -uid "933BB2EE-4E08-4EEE-97B4-F0AAE325F211";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 42.572250366210973 -1.6233628815642831e-06 -5.8367555766380974e-07 ;
	setAttr ".r" -type "double3" -5.7359738349914622 1.7872760295867949 -7.613584041595443 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999978 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.071110800000000002 -0.98518899999999998 -0.156031 0
		 -0.063531699999999997 -0.15163599999999999 0.98639299999999996 0 -0.99544299999999997 0.080056100000000005 -0.051807800000000001 0
		 -14.217876 53.06718 -1.8017909999999999 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "calf_twist_01_r" -p "calf_r";
	rename -uid "A0ED354E-4226-9949-0A22-F3B155F66212";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 20.476907730102521 0 -3.5527136788005009e-15 ;
	setAttr ".r" -type "double3" 0.32337033748626898 -0.21913294494151955 -0.87296384572982189 ;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999933 0.99999999999999978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.073941199999999999 -0.98245099999999996 -0.171238 0
		 -0.0702232 -0.16615199999999999 0.98359700000000005 0 -0.99478699999999998 0.084753200000000001 -0.056705400000000003 0
		 -15.674004999999999 32.893552999999997 -4.9968199999999996 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "foot_r" -p "calf_r";
	rename -uid "FAB7A421-48B1-ABA6-EF24-4BABFFED96DC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 40.196819305419908 1.6769354296286565e-06 -1.0918018237049409e-05 ;
	setAttr ".r" -type "double3" -0.41538643836974959 3.7049334049224849 8.0595779418945401 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999933 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.014826199999999999 -0.99980800000000003 -0.0128132 0
		 -0.045693999999999999 -0.012123699999999999 0.99888200000000005 0 -0.99884499999999998 0.0153951 -0.045505499999999997 0
		 -17.076291999999999 13.465709 -8.0737330000000007 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ball_r" -p "foot_r";
	rename -uid "52FB2050-4BDD-9060-A27A-E3BF8BF020BA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 10.453816413879407 16.577796936035163 -0.080158449709415436 ;
	setAttr ".r" -type "double3" 0.0039439606482484816 0.0089543778447274368 -91.883583068847685 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999978 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.046312699999999998 0.044977299999999998 -0.99791399999999997 0
		 -0.013384999999999999 -0.99886799999999998 -0.045641500000000002 0 -0.99883699999999997 0.015470899999999999 -0.045658299999999999 0
		 -17.908722000000001 2.8116810000000001 8.3552289999999996 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "thigh_twist_01_r" -p "thigh_r";
	rename -uid "046763B8-433D-44EA-BC59-F7920116C38F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	setAttr ".t" -type "double3" 22.094240188598661 5.3290705182007514e-15 -3.5527136788005009e-15 ;
	setAttr ".r" -type "double3" -5.4388685226440501 -0.00016392453107533537 -0.056340634822833927 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999989 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.12228600000000001 -0.99201499999999998 -0.030845999999999998 0
		 -0.054217300000000003 -0.0243559 0.99823200000000001 0 -0.99101300000000003 0.12374300000000001 -0.050805999999999997 0
		 -11.710775999999999 73.381930999999994 -1.1900470000000001 1;
	setAttr ".radi" 3;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "ik_foot_root" -p "root";
	rename -uid "2835A69D-49C2-271D-A534-D2A906255221";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".r" -type "double3" -1.2722218725854067e-14 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0 -1 0 0 1 0 0 0 0 0 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode joint -n "ik_foot_l" -p "ik_foot_root";
	rename -uid "AA4D5E24-4C1D-8BF6-884F-E29BBE65FD86";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 17.076271057128906 8.0721273422241246 13.465730667114256 ;
	setAttr ".r" -type "double3" 141.8209991455083 -88.877769470214858 -139.20716857910213 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0148276 0.99980800000000003 0.012795600000000001 0
		 -0.045693900000000003 0.0121061 -0.99888200000000005 0 -0.99884499999999998 -0.0153957 0.0455056 0
		 17.076270999999998 13.465731 -8.0721270000000001 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode joint -n "ik_foot_r" -p "ik_foot_root";
	rename -uid "B53C0FC1-4EF7-1064-D3CE-2999E6A7E723";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -17.076288223266602 8.0721483230590856 13.465573310852049 ;
	setAttr ".r" -type "double3" -38.178882598876925 88.877769470214858 139.20716857910207 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.0148276 -0.99980800000000003 -0.012795600000000001 0
		 -0.045695899999999998 -0.0121061 0.99888200000000005 0 -0.99884499999999998 0.015395799999999999 -0.045507699999999998 0
		 -17.076288000000002 13.465572999999999 -8.0721480000000003 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode joint -n "ik_hand_root" -p "root";
	rename -uid "9E71D96E-4B16-43B8-B369-9F8A91AB8F14";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".r" -type "double3" -1.2722218725854067e-14 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0 -1 0 0 1 0 0 0 0 0 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode joint -n "ik_hand_gun" -p "ik_hand_root";
	rename -uid "9001C97D-47D9-E733-0F91-16A8A267CA6E";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -56.646099090576172 0.33541175723078348 111.67965698242188 ;
	setAttr ".r" -type "double3" 74.068016052246065 -35.172615051269524 32.751056671142592 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.68747400000000003 0.57604200000000005 -0.442216 0
		 -0.61435799999999996 0.786022 0.068803299999999998 0 0.38722499999999999 0.224379 0.89426600000000001 0
		 -56.646099 111.67965700000001 -0.33541199999999999 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode joint -n "ik_hand_l" -p "ik_hand_gun";
	rename -uid "D9D5FAB2-4E3A-2B3E-A1AD-D7AA73C35058";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 77.885429382324219 -69.601913452148452 43.869503021240234 ;
	setAttr ".r" -type "double3" -145.80032348632813 -32.168746948242173 -93.709014892578139 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.68747400000000003 -0.57604200000000005 0.442216 0
		 -0.61435799999999996 -0.786022 -0.068804100000000007 0 0.38722600000000001 -0.22437799999999999 -0.89426499999999998 0
		 56.646000000000001 111.67967299999999 -0.33546300000000001 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode joint -n "ik_hand_r" -p "ik_hand_gun";
	rename -uid "5ACF6593-4586-517E-62B0-499546A76431";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.1316282072803006e-14 0 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -1.0336802714756426e-14 -6.3611093629270335e-15 1.5107634736951704e-14 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000007 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.68747400000000003 0.57604200000000005 -0.442216 0
		 -0.61435799999999996 0.786022 0.068803299999999998 0 0.38722499999999999 0.224379 0.89426600000000001 0
		 -56.646099 111.67965700000001 -0.33541199999999999 1;
	setAttr ".radi" 3;
	setAttr ".fbxID" 5;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "90E9BE68-4F6F-9B7A-EA6B-4694B71CB651";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "2C8263B9-42D0-1D47-98EE-31A1FCEA395B";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "DABD749A-409A-0251-6B72-39BC03E1BB72";
createNode displayLayerManager -n "layerManager";
	rename -uid "924A92B5-4FAC-F532-DF8F-83BBE384E8A7";
createNode displayLayer -n "defaultLayer";
	rename -uid "8E164FDC-49C1-7E7A-8266-A3819526F86E";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "A61191C3-4DC5-E7F9-F79B-13BDFA02DCBA";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "D0FCCB83-4757-FEEE-AE86-2485DDDB144F";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "3FB6BD0B-4FEE-DDE9-BDB9-189B4DF025C6";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".hwi";
	setAttr -av ".ta";
	setAttr -av ".tq";
	setAttr -av ".etmr";
	setAttr -av ".tmr";
	setAttr -av ".aoon";
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av -k on ".hfs";
	setAttr -av -k on ".hfe";
	setAttr -av ".hfc";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".blen";
	setAttr -k on ".blat";
	setAttr -av ".msaa";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".macc";
	setAttr -cb on ".macd";
	setAttr -cb on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr ".ren" -type "string" "arnold";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -cb on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".mbso";
	setAttr -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -cb on ".ope";
	setAttr -cb on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "root.s" "pelvis.is";
connectAttr "pelvis.s" "spine_01.is";
connectAttr "spine_01.s" "spine_02.is";
connectAttr "spine_02.s" "spine_03.is";
connectAttr "spine_03.s" "clavicle_l.is";
connectAttr "clavicle_l.s" "upperarm_l.is";
connectAttr "upperarm_l.s" "lowerarm_l.is";
connectAttr "lowerarm_l.s" "hand_l.is";
connectAttr "hand_l.s" "index_01_l.is";
connectAttr "index_01_l.s" "index_02_l.is";
connectAttr "index_02_l.s" "index_03_l.is";
connectAttr "hand_l.s" "middle_01_l.is";
connectAttr "middle_01_l.s" "middle_02_l.is";
connectAttr "middle_02_l.s" "middle_03_l.is";
connectAttr "hand_l.s" "pinky_01_l.is";
connectAttr "pinky_01_l.s" "pinky_02_l.is";
connectAttr "pinky_02_l.s" "pinky_03_l.is";
connectAttr "hand_l.s" "ring_01_l.is";
connectAttr "ring_01_l.s" "ring_02_l.is";
connectAttr "ring_02_l.s" "ring_03_l.is";
connectAttr "hand_l.s" "thumb_01_l.is";
connectAttr "thumb_01_l.s" "thumb_02_l.is";
connectAttr "thumb_02_l.s" "thumb_03_l.is";
connectAttr "lowerarm_l.s" "lowerarm_twist_01_l.is";
connectAttr "upperarm_l.s" "upperarm_twist_01_l.is";
connectAttr "spine_03.s" "clavicle_r.is";
connectAttr "clavicle_r.s" "upperarm_r.is";
connectAttr "upperarm_r.s" "lowerarm_r.is";
connectAttr "lowerarm_r.s" "hand_r.is";
connectAttr "hand_r.s" "index_01_r.is";
connectAttr "index_01_r.s" "index_02_r.is";
connectAttr "index_02_r.s" "index_03_r.is";
connectAttr "hand_r.s" "middle_01_r.is";
connectAttr "middle_01_r.s" "middle_02_r.is";
connectAttr "middle_02_r.s" "middle_03_r.is";
connectAttr "hand_r.s" "pinky_01_r.is";
connectAttr "pinky_01_r.s" "pinky_02_r.is";
connectAttr "pinky_02_r.s" "pinky_03_r.is";
connectAttr "hand_r.s" "ring_01_r.is";
connectAttr "ring_01_r.s" "ring_02_r.is";
connectAttr "ring_02_r.s" "ring_03_r.is";
connectAttr "hand_r.s" "thumb_01_r.is";
connectAttr "thumb_01_r.s" "thumb_02_r.is";
connectAttr "thumb_02_r.s" "thumb_03_r.is";
connectAttr "lowerarm_r.s" "lowerarm_twist_01_r.is";
connectAttr "upperarm_r.s" "upperarm_twist_01_r.is";
connectAttr "spine_03.s" "neck_01.is";
connectAttr "neck_01.s" "head.is";
connectAttr "pelvis.s" "thigh_l.is";
connectAttr "thigh_l.s" "calf_l.is";
connectAttr "calf_l.s" "calf_twist_01_l.is";
connectAttr "calf_l.s" "foot_l.is";
connectAttr "foot_l.s" "ball_l.is";
connectAttr "thigh_l.s" "thigh_twist_01_l.is";
connectAttr "pelvis.s" "thigh_r.is";
connectAttr "thigh_r.s" "calf_r.is";
connectAttr "calf_r.s" "calf_twist_01_r.is";
connectAttr "calf_r.s" "foot_r.is";
connectAttr "foot_r.s" "ball_r.is";
connectAttr "thigh_r.s" "thigh_twist_01_r.is";
connectAttr "root.s" "ik_foot_root.is";
connectAttr "ik_foot_root.s" "ik_foot_l.is";
connectAttr "ik_foot_root.s" "ik_foot_r.is";
connectAttr "root.s" "ik_hand_root.is";
connectAttr "ik_hand_root.s" "ik_hand_gun.is";
connectAttr "ik_hand_gun.s" "ik_hand_l.is";
connectAttr "ik_hand_gun.s" "ik_hand_r.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of ue_mannequin_skelenton.ma
