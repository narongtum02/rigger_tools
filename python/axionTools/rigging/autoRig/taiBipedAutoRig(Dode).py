import maya.cmds as mc

# sys.path.append(r'D:\True_Axion\Tools\riggerTools\python\axionTools\rigging\autoRig\bodyRig')

from axionTools.rigging.autoRig.bodyRig import rootRig
reload( rootRig )

from axionTools.rigging.autoRig.bodyRig import hipRig
reload( hipRig )

from axionTools.rigging.autoRig.bodyRig import spineFkRig
reload( spineFkRig )

from axionTools.rigging.autoRig.bodyRig import neckRig
reload( neckRig )

from axionTools.rigging.autoRig.bodyRig import headRig
reload( headRig )

from axionTools.rigging.autoRig.bodyRig import clavicleRig
reload( clavicleRig )

from axionTools.rigging.autoRig.bodyRig import armRig
reload( armRig )

from axionTools.rigging.autoRig.bodyRig import fingerRig
reload( fingerRig )

from axionTools.rigging.autoRig.bodyRig import fingerCurl
reload( fingerCurl )

from axionTools.rigging.autoRig.bodyRig import localFinger
reload( localFinger )

from axionTools.rigging.autoRig.bodyRig import bipedLegRig
reload( bipedLegRig )

from axionTools.rigging.autoRig.bodyRig import spineRig
reload( spineRig )


# 01
# Create main Controller
charScale = rootRig.createMasterGrp()
charScale = 3

# 02
# Create hipRig
hipRig.hipRig( ctrl_grp = 'ctrl_grp'  , tmpJnt = ( 'cog_tmpJnt','hip_tmpJnt' ) )

# 03
# Create spineRig
topSpine = spineRig.spineIK( spineNum = 4 , hipJnt = 'hip_bJnt', cog_ctrl = 'cog_gmbCtrl', ctrl_grp = 'ctrl_grp' , charScale = charScale) 

'''
topSpine = spineFkRig.spineRig( 		parentTo = 'ctrl_grp' ,
										tmpJnt = (	'spine01_tmpJnt' ,'spine02_tmpJnt' ,
								        			'spine03_tmpJnt' ,'spine04_tmpJnt'	),
										priorCtrl = 'cog_gmbCtrl'								)

'''


# 04
# Neck Rig
neckJnt = neckRig.neckRig(	parentTo = 'ctrl_grp'  , 
							tmpJnt = (		'neck_tmpJnt' ,'head01_tmpJnt' 	) ,  
							priorJnt = topSpine	)

# 05
headRig.headRig(	parentTo = 'ctrl_grp'  , 
				tmpJnt = ( 		'head01_tmpJnt' , 'eyeLFT_tmpJnt' , 'eyeRGT_tmpJnt' ,		# head
				'jaw01Lwr_tmpJnt' , 'jaw02Lwr_tmpJnt' , 'jaw03Lwr_tmpJnt' 			,		# jaw
				'jaw01Upr_tmpJnt' , 'jaw02Upr_tmpJnt','eye_tmpJnt' , 
				'eyeTargetLFT_tmpJnt' , 'eyeTargetRGT_tmpJnt'		 		# eye			
																			),
				faceCtrl = True	,
				priorJnt = neckJnt				
			)

# 06
clavLFT = clavicleRig.clavicleRig(		charScale = charScale ,
						parentTo = 'ctrl_grp' ,
						side = 'LFT',
						tmpJnt = ( 	'clavLFT_tmpJnt'  ),
						priorJnt = topSpine

					)

clavRGT = clavicleRig.clavicleRig(		charScale = charScale ,
						parentTo = 'ctrl_grp' ,
						side = 'RGT',
						tmpJnt = ( 	'clavRGT_tmpJnt'  ),
						priorJnt = topSpine

					)
					
# 07
stickNamLFT = armRig.armRig(
								charScale = charScale,
								parentTo = 'ctrl_grp' ,
								side = 'LFT',
								tmpJnt = (	'upperArmLFT_tmpJnt'  , 'lowerArmLFT_tmpJnt' ,  
											'handLFT_tmpJnt' , 'armLFT_pov_tmpJnt' ),
								priorJnt = clavLFT ,
								ikhGrp = 'ikh_grp' ,
								noTouchGrp = 'noTouch_grp' ,
								ribbon = False )


stickNamRGT = armRig.armRig(
								charScale = charScale,
								parentTo = 'ctrl_grp' ,
								side = 'RGT',
								tmpJnt = (	'upperArmRGT_tmpJnt'  , 'lowerArmRGT_tmpJnt' ,  
											'handRGT_tmpJnt' , 'armRGT_pov_tmpJnt' ),
								priorJnt = clavRGT,
								ikhGrp = 'ikh_grp' ,
								noTouchGrp = 'noTouch_grp' ,
								ribbon = False )


# 08			
# Create finger LFT
handLFT = fingerRig.fingerRig( 	side = 'LFT', fingerNum = 3 , 
								fingerName = ['thumb', 'index', 'middle', 'ring', 'pinky'], 
								charScale = charScale ,stickNam = stickNamLFT )
# Create finger RGT
handRGT = fingerRig.fingerRig( 	side = 'RGT', fingerNum = 3 , 
								fingerName = ['thumb', 'index', 'middle', 'ring', 'pinky'], 
								charScale = charScale ,stickNam = stickNamRGT )


# 09
# Main finger curl
fingerCurl.mainFingerCurlRig( 	fingerbehavior = ('fist','roll','relax','spread') , 
								fingerName = ('thumb','index','middle','ring','pinky') , 
								side = 'LFT'  , numCtrl = 3 , zroNam = 'Zro_grp' , 
								offsetNam = 'Offset_grp' )

fingerCurl.mainFingerCurlRig( 	fingerbehavior = ('fist','roll','relax','spread') , 
								fingerName = ('thumb','index','middle','ring','pinky') , 
								side = 'RGT'  , numCtrl = 3 , zroNam = 'Zro_grp' , 
								offsetNam = 'Offset_grp' )


# 10
# local finger curl
# Create Grp
stickGrpNam = localFinger.creEachFingerGrp( parentTo = 'ctrl_grp' , side = 'LFT'  )

# Thumb LFT
localFinger.creHandStick( fingerName = 'thumb' , side = 'LFT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'thumb' , side = 'LFT'  , ctrlName = 'thumblocalLFT_ctrl'  )
# Index LFT
localFinger.creHandStick( fingerName = 'index' , side = 'LFT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'index' , side = 'LFT'  , ctrlName = 'indexlocalLFT_ctrl'  )
# Middle LFT
stickNam = localFinger.creHandStick( fingerName = 'middle' , side = 'LFT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'middle' , side = 'LFT'  , ctrlName = stickNam  )
# Ring LFT
stickNam = localFinger.creHandStick( fingerName = 'ring' , side = 'LFT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'ring' , side = 'LFT'  , ctrlName = stickNam  )
# Pinky LFT
stickNam = localFinger.creHandStick( fingerName = 'pinky' , side = 'LFT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'pinky' , side = 'LFT'  , ctrlName = stickNam  )


# RGT
stickGrpNam = localFinger.creEachFingerGrp(   parentTo = 'ctrl_grp' , side = 'RGT'  )

# Thumb RGT 
localFinger.creHandStick( fingerName = 'thumb' , side = 'RGT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'thumb' , side = 'RGT'  , ctrlName = 'thumblocalRGT_ctrl'  )
# Index RGT
localFinger.creHandStick( fingerName = 'index' , side = 'RGT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'index' , side = 'RGT'  , ctrlName = 'indexlocalRGT_ctrl'  )
# Middle RGT
stickNam = localFinger.creHandStick( fingerName = 'middle' , side = 'RGT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'middle' , side = 'RGT'  , ctrlName = stickNam  )
# Ring RGT
stickNam = localFinger.creHandStick( fingerName = 'ring' , side = 'RGT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'ring' , side = 'RGT'  , ctrlName = stickNam  )
# Pinky RGT
stickNam = localFinger.creHandStick( fingerName = 'pinky' , side = 'RGT' , charScale = charScale , parentTo = stickGrpNam )
localFinger.creLocalFingerAttr(  fingerName = 'pinky' , side = 'RGT'  , ctrlName = stickNam  )


# 11
# Leg
bipedLegRig.bipedLegRig(
								parentTo = 'ctrl_grp' 	,
								side = 'LFT'			,
								tmpJnt = (	'upperLegLFT_tmpJnt'  , 'lowerLegLFT_tmpJnt' ,  'ankleLFT_tmpJnt' , 
											'legLFT_pov_tmpJnt' ,'ballLFT_tmpJnt' ,'toesTipLFT_tmpJnt' ),
								priorJnt = 'hip_bJnt'	,
								ikhGrp = 'ikh_grp' 		,
								noTouchGrp = 'noTouch_grp' 

								)



bipedLegRig.bipedLegRig(
								parentTo = 'ctrl_grp' 	,
								side = 'RGT'			,
								tmpJnt = (	'upperLegRGT_tmpJnt'  , 'lowerLegRGT_tmpJnt' ,  'ankleRGT_tmpJnt' , 
											'legRGT_pov_tmpJnt' ,'ballRGT_tmpJnt' ,'toesTipRGT_tmpJnt' ),
								priorJnt = 'hip_bJnt'	,
								ikhGrp = 'ikh_grp' 		,
								noTouchGrp = 'noTouch_grp' 

								)
	
mc.delete('template_ctrl')
