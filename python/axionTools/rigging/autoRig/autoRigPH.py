#----------------------CONTROL MAPPING----------------------------#
import maya.cmds as mc
def MainCtrl(): # Re Order Center Control
   
    #-------------------ALIGNT Main FK---------------------------#
    cog = 'cog_Zro_grp'
    mc.parentConstraint ( 'hips_bind_jnt', cog,w = 1)
    mc.delete (cog + '_parentConstraint1')
    mc.parent (cog,'fly_ctrl')
    # hip back to COG
    mc.parent ('hips_Zro_grp','cog_ctrl')
    
    jntCenterName = ('hips','spine','chest')
    for c in range(len(jntCenterName)):
    
        # Create Name
        name = jntCenterName[c]
        jnt = name + '_bind_jnt'
        grp = name + '_Zro_grp'
        ctrl = name + '_ctrl'
        Gim = name + '_Gimbal_ctrl'
        
        # Alignt
        mc.parentConstraint ( jnt, grp, w = 1, mo = 0)
        mc.delete (grp + '_parentConstraint1')
        # Constraint to Joint
        mc.parentConstraint ( Gim, jnt, w = 1, mo = 1)
        
                
    for c in range((len(jntCenterName)-1),0,-1): #Parent Ctrl to Chain
    
        # Create Name
        name = jntCenterName[c]
        grp = name + '_Zro_grp'
        upGimbal = jntCenterName[c-1] + '_Gimbal_ctrl'
    
        if jntCenterName[c] == 'spine': #FOR SPINE
            mc.parent (grp,'cog_Gimbal_ctrl')
        else:
            mc.parent (grp,upGimbal)
            
    #-------------------ALIGNT Main(+) Local World ---------------------------#
    
    # Local _ World Space
    rigName = ('upperChest','neck','head')
    for i in range(len(rigName)):
        # Naming
        name = rigName[i]
        jnt = name + '_bind_jnt'
        grp = name + '_Zro_grp'
        ctrl = name +'_ctrl'
        Rig = name + 'Rig' + '_grp'
        Wor = name + '_Wor_grp'
        Loc = name + '_Loc_grp'
        Gim = name + '_Gimbal_ctrl'
        pCons = '_parentConstraint1'
        oCons = '_orientConstraint1'
        
        # Create
        #mc.group( em = True, n = Rig)
        mc.group( em = True, n = Wor)
        mc.group( em = True, n = Loc)
        
        # Position
        mc.parentConstraint( jnt, grp, w = 1, mo = 0)
        mc.parentConstraint( jnt, Wor, w = 1, mo = 0)
        mc.parentConstraint( jnt, Loc, w = 1, mo = 0)
        mc.delete( grp + pCons )
        mc.delete( Wor + pCons )
        mc.delete( Loc + pCons )
        
        #---------------------------------------------------------#
        if i == 0: #Only For Chest
            ctrlUp = 'chest' + '_Gimbal_ctrl'
            # Parent
            mc.parent( grp, Wor, Loc, ctrlUp)
            #mc.reorder( Rig + '_parentConstraint1' ,relative = 1)
        else: #For Other
            ctrlUp = rigName[i-1] + '_Gimbal_ctrl'
            mc.parent( grp, Wor, Loc, ctrlUp)
            #mc.reorder( Rig + '_parentConstraint1' ,relative = 1)
        #---------------------------------------------------------#
        '''
        # Lock & Hide
        mc.setAttr( Rig + '.tx', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.ty', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.tz', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.rx', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.ry', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.rz', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.sx', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.sy', lock = True, keyable = False, channelBox = False)
        mc.setAttr( Rig + '.sz', lock = True, keyable = False, channelBox = False)
        '''
        # LocWorld
        rev = Wor + '_rev'
        LocWorAttr = ctrl + '.localWorld'
        mc.orientConstraint( 'fly_ctrl', Wor, w = 1, mo = 1) # from Fly
        mc.orientConstraint( Wor, Loc, grp, w = 0, mo = 1) # to parent
        mc.createNode( 'reverse', n = rev) # createNode
        
        mc.connectAttr( LocWorAttr,rev + '.input.inputX' )
        
        mc.connectAttr( LocWorAttr, grp + oCons + '.' + Wor + 'W0')
        mc.connectAttr( rev + '.output.outputX', grp + oCons + '.' + Loc + 'W1')
        
        # Constraint to Bind Joint
        mc.parentConstraint( Gim, jnt, w = 1, mo = 1)
        
    
 
    #-----------------------ALIGNT FK ARM LEG---------------------------#
    
def SideCtrl(): # Re Order Side Control

    # Shoulder LOC WOR RIG   # Local_World : Shoulder
    for s in range(2):
        sideName = ('LFT','RGT')
        side = sideName[s]
        
        
        # Naming
        preName = 'shoulder','upperArm'
        for i in range(2):
            name = preName[i] + side + '_FK'
            jnt = preName[i] + side + '_bind_jnt'
            grp = name + '_Zro_grp'
            ctrl = name +'_ctrl'
            Wor = name + '_Wor_grp'
            Loc = name + '_Loc_grp'
            Gim = name + '_Gimbal_ctrl'
            pCons = '_parentConstraint1'
            oCons = '_orientConstraint1'
            
            # Create
            #mc.group( em = True, n = Rig)
            mc.group( em = True, n = Wor)
            mc.group( em = True, n = Loc)
            
            # Position
            mc.parentConstraint( jnt, grp, w = 1, mo = 0)
            mc.parentConstraint( jnt, Wor, w = 1, mo = 0)
            mc.parentConstraint( jnt, Loc, w = 1, mo = 0)
            mc.delete( grp + pCons )
            mc.delete( Wor + pCons )
            mc.delete( Loc + pCons )
            
            #---------------------------------------------------------#
            ctrlUp = 'upperChest_Gimbal_ctrl','shoulder'+ side +'_FK_Gimbal_ctrl'
            # Parent
            mc.parent( grp, Wor, Loc, ctrlUp[i])
            #---------------------------------------------------------#
        
            # LocWorld
            rev = Wor + '_rev'
            LocWorAttr = ctrl + '.localWorld'
            
            
            mc.orientConstraint( 'fly_ctrl', Wor, w = 1, mo = 1) # from Fly
            mc.orientConstraint( Wor, Loc, grp, w = 0, mo = 1) # to parent
            mc.createNode( 'reverse', n = rev) # createNode
            
            mc.connectAttr( LocWorAttr,rev + '.input.inputX' )
            
            mc.connectAttr( LocWorAttr, grp + oCons + '.' + Wor + 'W0')
            mc.connectAttr( rev + '.output.outputX', grp + oCons + '.' + Loc + 'W1')

                # Constraint to Bind Joint
            if i == 0:
                mc.parentConstraint( Gim, jnt, w = 1, mo = 1)


    for s in range(2): 
        sideGive = 'LFT','RGT'
        side = sideGive[s]
        
        # LEG ZONE --------------------------------------------------------
        legJnt = ('upperLeg','lowerLeg','foot','toes')
        for n in range(len(legJnt)): # Re Order Control # LEG
        
            # Create Name
            name = legJnt[n] + side + '_FK'
            jnt = legJnt[n] + side + '_bind_jnt'
            grp = name + '_Zro_grp'
            
            # Alignt
            mc.parentConstraint ( jnt, grp, w = 1, mo = 0)
            mc.delete (grp + '_parentConstraint1')
    
        for n in range((len(legJnt)-1),0,-1): # Parent Ctrl to Chain
        
            # Create Name
            name = legJnt[n] + side + '_FK'
            grp = name + '_Zro_grp'
            upGimbal = legJnt[n-1] + side + '_FK_Gimbal_ctrl'
            
            # Parent 
            mc.parent (grp,upGimbal)
            
        #Parent GRP HEAD
        hipGimbal = 'hips_Gimbal_ctrl'
        zroGrp = legJnt[0] + side + '_FK_Zro_grp'
        mc.parent (zroGrp,hipGimbal)
        
        # ARM ZONE -----------------------------------------------------
        armJnt = ('lowerArm','hand')
        for n in range(len(armJnt)): #Re Order Control # ARM
    
            #Create Name
            name = armJnt[n] + side + '_FK'
            jnt = armJnt[n] + side + '_bind_jnt'
            grp = name + '_Zro_grp'
            
            #Alignt
            mc.parentConstraint ( jnt, grp, w = 1, mo = 0)
            mc.delete (grp + '_parentConstraint1')
                
        for n in range((len(armJnt)-1),0,-1): #Parent Ctrl back in Grp
    
            #Create Name
            grp = armJnt[n]+ side + '_FK_Zro_grp'
            upGimbal = armJnt[n-1] + side + '_FK_Gimbal_ctrl'
            
            #Parent 
            mc.parent (grp,upGimbal)
        
        #Parent GRP HEAD
        upperArmCtrl = 'upperArm' + side + '_FK_Gimbal_ctrl'
        zroGrp = armJnt[0] + side + '_FK_Zro_grp'
        clvJnt = 'shoulder' + side + '_bind_jnt'
        clvCtrl = 'shoulder' + side + '_FK_ctrl'
        clvGimbal = 'shoulder' + side + '_FK_Gimbal_ctrl'
        mc.parent (zroGrp,upperArmCtrl)
        #mc.parentConstraint ( clvGimbal, clvJnt , w = 1, mo = 0)
        
        
        #----------------------- PROP ---------------------------#
        
        #Parent Prop Grp
        for i in range(2):
            
            #Create Name
            jnt = armJnt[i] + side + '_prop_jnt'
            zroGrp = armJnt[i] + side + '_prop_Zro_grp'
            gimbal = armJnt[i] + side + '_prop_Gimbal_ctrl'
            bindJnt = armJnt[i] + side + '_bind_jnt'
            propGrp = 'prop_ctrl_grp'
            
            #Alignt
            mc.parentConstraint (jnt, zroGrp, w = 1, mo = 0)
            mc.delete (zroGrp + '_parentConstraint1')
            
            #Parent
            mc.parent (zroGrp,propGrp)
            
    for s in range(2): # FOR LEG IK Handle
        #Side Zone
        sideGive = 'LFT','RGT'
        side = sideGive[s]
        #Name
        jntName = 'hand','lowerArm'
        for i in range(len(jntName)):
            name = jntName[i] + side
            jnt = name + '_bind_jnt'
            Zro = name + '_prop_Zro_grp'
            propJnt = name + '_prop_jnt'
            gimbal = name + '_prop_Gimbal_ctrl'
            
            mc.parentConstraint (jnt, Zro, w = 1, mo = 1)
            mc.parentConstraint (gimbal, propJnt, w = 1, mo = 1)

            

        #-----------------------ALIGNT FK FINGER---------------------------#

def FingerCtrl(): #reOrder Zro_grp FOR FINGER
    for s in range(2):
        # Name ZONE
        sideGive = 'LFT','RGT'
        side = sideGive[s]
        fingerName = ('thumb','index','middle','ring','pinky')
        finger = 'finger' + side + '_Zro_grp'
        hand = 'hand' + side + '_bind_jnt'
        mc.parentConstraint ( hand, finger, w = 1, mo = 0)
        mc.delete (finger + '_parentConstraint1')
        mc.parent (finger,'fly_ctrl')
        mc.parentConstraint ('hand' + side  + '_bind_jnt', finger, w = 1, mo = 1)
        
        for n in range(len(fingerName)):
            # Create Name
            boneName = fingerName[n]
            
    
            for i in range(3):
                
                # Create SUB Name
                name = boneName + '0%d'%(i+1) + side
                jnt = name + '_bind_jnt'
                grp = name + '_Zro_grp'
                ctrl = name + '_ctrl'
        
                # Alignt
                mc.parentConstraint ( jnt, grp, w = 1, mo = 0)
                mc.delete (grp + '_parentConstraint1')
                mc.parentConstraint (ctrl, jnt, w = 1, mo = 1)
                
            # Re Order Finger Grp
            for i in range(3,1,-1):
                
                # Create SUB Name
                name = boneName+'0%d'% (i) + side 
                grp = name + '_Zro_grp'
                upGrp = boneName +'0%d'% (i-1) + side + '_ctrl'
                
                # Parent
                mc.parent (grp,upGrp)
                
            finger = 'finger' + side + '_Zro_grp'
            zroGrp = boneName + '01' + side + '_Zro_grp'
            mc.parent (zroGrp,finger)
            
            for i in range(3,0,-1):
                name = boneName+'0%d'% (i) + side 
                ctrl = name + '_ctrl'
                bone = name + '_bind_jnt'
                mc.parentConstraint (ctrl,bone,mo=True,w=1)
                
                
#-----------------------IK JOINT ZONE---------------------------#

def IKJoint() :
    
    # Null Alignt
    nullName = 'hips','shoulderLFT','shoulderRGT'
    for i in range (len(nullName)):
        
        #Create Name
        name = nullName[i]
        jnt = name + '_bind_jnt'
        null = name + '_null_grp'
    
        #Alignt
        mc.parentConstraint (jnt, null, w = 1, mo = 0)
        mc.delete (null +'_parentConstraint1')
    
    
    for s in range(2): # CREATE IK JOINT
        #Side Zone
        sideGive = 'LFT','RGT'
        side = sideGive[s]
        #Give Name
        jntName = 'upperArm','lowerArm','hand','upperLeg','lowerLeg','foot','toes','toesTip'
        
        for n in range (len(jntName)): # CREATE IK JOINT
            
            #Name Zone
            name = jntName[n] + side
            jnt = name + '_bind_jnt'
            IKJnt = name + '_IK_jnt'
            
            #Get Position & Create Joint
            
            nPos = mc.xform (jnt, q = True, ws = True, rp = True)
            nOri = mc.xform (jnt, q = True, ws = True, ro = True)
            #nOrd = mc.xform (jnt, q = True ,roo = True)
            
            #Give Them Position
            nJnt = mc.joint (n = IKJnt ,p = nPos, o = nOri)
            mc.select (cl = True)
            
    for s in range(2): # FOR LEG IK PARENT
        #Side Zone
        sideGive = 'LFT','RGT'
        side = sideGive[s]
        
        #Give Name
        jntName = 'upperArm','lowerArm','hand','upperLeg','lowerLeg','foot','toes','toesTip'
        for n in range ((len(jntName)-1),3,-1):  # FOR LEG IK PARENT
        
        
            name = jntName[n] + side
            jnt = name + '_bind_jnt'
            IKJnt = name + '_IK_jnt'
            upIKJnt = jntName[n-1] + side + '_IK_jnt'
            
            #Parent
            mc.parent (IKJnt, upIKJnt)
    
        # Head to Null GRP
        mc.parent (jntName[0]+ side + '_IK_jnt', 'shoulder' + side + '_null_grp')
        mc.parent (jntName[3]+ side + '_IK_jnt', 'hips_null_grp')
            
        for n in range ((len(jntName)-6),0,-1):  # FOR ARM IK PARENT
            
            # Create Name
            name = jntName[n] + side
            jnt = name + '_bind_jnt'
            IKJnt = name + '_IK_jnt'
            upIKJnt = jntName[n-1] + side + '_IK_jnt'
    
            #Parent
            mc.parent (IKJnt, upIKJnt)
        
#-----------------------IK JOINT ZONE---------------------------#    



#----------------------IK SYSTEM----------------------------#
def IKSystem():
    for p in range(2): #IK SYSTEM Snap
    
        sideGive = 'LFT','RGT'
        side = sideGive[p]

        #tempName = ('arm','leg')
        ''' # OLD POV #-----------------------------------#
        for n in range(len(tempName)): #POV Snap
        
            #Create Name
            name = tempName[n] + side
            jnt = name + '_pov_tmpJnt'
            grp = name + '_pov_Zro_grp'
            
            #Alignt
            mc.pointConstraint ( jnt, grp, w = 1, mo = 0)
            mc.delete (grp + '_pointConstraint1')
            
            #Find Master Grp
            mc.parent (grp, 'IK_ctrl_grp')
        '''   
        #-----------------------ALIGNT IK---------------------------#
        
        # IK Zone
        jntName = ('hand','foot')
        
        for n in range(len(jntName)): #IK Control Snap 
            
            if jntName[n] == 'hand':
                
                #Create Name
                name = jntName[n] + side
                jnt = name + '_bind_jnt'
                grp = name + '_IK_Zro_grp'
                
                #Alignt
                mc.pointConstraint ( jnt, grp, w = 1, mo = 0)
                mc.delete (grp + '_pointConstraint1')
                mc.parent (grp, 'IK_ctrl_grp')
    
    
            if jntName[n] == 'foot':
                    
                #Create Name
                name = jntName[n] + side
                toeName = 'toes' + side
                jnt = toeName + '_bind_jnt'
                grp = name + '_IK_Zro_grp'
                
                #Alignt
                mc.pointConstraint ( jnt, grp, w = 1, mo = 0)
                mc.delete (grp + '_pointConstraint1')
                mc.parent (grp, 'IK_ctrl_grp')
            
        #-----------------------ALIGNT FOOT ROLL---------------------------#
        
        # IK FOOT SYSTEM
        tmpFoot = ('footOut','footIn','heelRoll','toesRoll','ballRoll','ankle')
        for i in range(len(tmpFoot)):    # Snap and parent
            
            if tmpFoot[i] in ('footOut','footIn','heelRoll'):
                
                #Create Name
                name = tmpFoot[i] + side
                grp = name + '_IK_Zro_grp'
                jnt = name + '_tmpJnt'
                
                #Alignt
                mc.parentConstraint ( jnt, grp, w=1, mo=0)
                mc.delete (grp + '_parentConstraint1')
                
            if tmpFoot[i] == 'toesRoll':
                
                #Create Name Toes Roll
                name = tmpFoot[i] + side
                grp = name + '_IK_Zro_grp'
                jnt = 'toesTip' + side + '_bind_jnt'
                
                #Alignt
                mc.parentConstraint ( jnt, grp, w=1, mo=0)
                mc.delete (grp + '_parentConstraint1')
                
            if tmpFoot[i] == 'ballRoll':
                
                #Create Name Ball Roll
                name = tmpFoot[i] + side
                grp = name + '_IK_Zro_grp'
                jnt = 'toes' + side + '_bind_jnt'
                
                #Alignt
                mc.parentConstraint ( jnt, grp, w=1, mo=0)
                mc.delete (grp + '_parentConstraint1')
                
            if tmpFoot[i] == 'ankle':
                
                #Create Name Ball Roll
                name = tmpFoot[i] + side
                grp = name + '_IK_Zro_grp'
                jnt = 'foot' + side + '_bind_jnt'
                
                #Alignt
                mc.parentConstraint ( jnt, grp, w=1, mo=0)
                mc.delete (grp + '_parentConstraint1')
        
        
        for i in range(len(tmpFoot)-1,0,-1): # Parent BACK
        
            #Create Name
            name = tmpFoot[i] + side
            grp = name + '_IK_Zro_grp'
            upCtrl = tmpFoot[i-1] + side + '_IK_ctrl'
        
            #Parent
            mc.parent (grp,upCtrl)
            
        mc.parent ('footOut' + side + '_IK_Zro_grp', 'foot' + side + '_IK_ctrl')
#-----------------------BLEND IK---------------------------#            
def BlendIK():

    for s in range(2): #CONSTRAINT
    
        #Side Zone
        sideGive = 'LFT','RGT'
        side = sideGive[s]
        #Give Name
        jntName = 'upperArm','lowerArm','hand','upperLeg','lowerLeg','foot','toes'
        
        for n in range (len(jntName)): # Constraint
        
            #Name
            name = jntName[n] + side
            jnt = name + '_bind_jnt'
            IKJnt = name + '_IK_jnt'
            ctrl = name + '_FK_ctrl'
            gimbal = name + '_FK_Gimbal_ctrl'
            
            #Constraint
            mc.parentConstraint (IKJnt, gimbal, jnt, w = 0, mo = 1)
            
    #-------BLEND ARM ZONE-------# 
    mainCtrl = "placement_ctrl"
    mainName = 'arm'
    for s in range (2): # Side Loop
        sideName = ('LFT','RGT')
        side = sideName[s]  
        attrName = (".IK_FK_Arm_L",".IK_FK_Arm_R")
        attr = attrName[s]
        
        pmaName = mainName + side + '_blend_pma'
        # CreateNode & Connect
        mc.createNode("plusMinusAverage",n = pmaName)
        mc.connectAttr( mainCtrl + attr , pmaName + ".input1D[1]")
        # SetAttr
        mc.setAttr ( pmaName + ".operation", 2)
        mc.setAttr ( pmaName + ".input1D[0]", 1)
        
        for i in range (3): # Detail Loop
            boneName = ('upperArm','lowerArm','hand')
            name = boneName[i]+side
            
            # #Naming
            bindJnt = name + "_bind_jnt"
            IK = "." + name + "_IK_jntW0"
            FK = "." + name + "_FK_Gimbal_ctrlW1"
            con = bindJnt + "_parentConstraint1"
            
            # Connect to Constraint Node
            mc.connectAttr ( pmaName + ".output1D" , con + IK)
            mc.connectAttr ( mainCtrl + attr, con + FK)
            
    #-------BLEND LEG ZONE-------#
    mainCtrl = "placement_ctrl"
    mainName = 'leg'
    for s in range (2): # Side Loop
        sideName = ('LFT','RGT')
        side = sideName[s]  
        attrName = (".IK_FK_Leg_L",".IK_FK_Leg_R")
        attr = attrName[s]
        
        pmaName = mainName + side + '_blend_pma'
        # CreateNode & Connect
        mc.createNode("plusMinusAverage",n = pmaName)
        mc.connectAttr( mainCtrl + attr , pmaName + ".input1D[1]")
        # SetAttr
        mc.setAttr ( pmaName + ".operation", 2)
        mc.setAttr ( pmaName + ".input1D[0]", 1)
        
        boneName = ('upperLeg','lowerLeg','foot','toes')
        for i in range (len(boneName)): # Detail Loop
            
            name = boneName[i]+side
            
            # Naming
            bindJnt = name + "_bind_jnt"
            IK = "." + name + "_IK_jntW0"
            FK = "." + name + "_FK_Gimbal_ctrlW1"
            con = bindJnt + "_parentConstraint1"
            
            # Connect to Constraint Node
            mc.connectAttr ( pmaName + ".output1D" , con + IK)
            mc.connectAttr ( mainCtrl + attr, con + FK)


#-----------------------IK HANDLE---------------------------#

def IKhandle():
    sideGive = 'LFT','RGT' #Side Zone
    for s in range(2): # FOR LEG IK Handle
    
        side = sideGive[s] # SIDE SIDE SIDE
        # POV LOC WOR RIG ------------------------------------------#
        giveName = ('arm','leg')
        for n in range(len(giveName)):
            # Local_World : POV name
            
            # Naming
            preName = giveName[n]
            name = preName + side + '_pov'
            jnt = name + '_tmpJnt'
            grp = name + '_Zro_grp'
            pov = name +'_ctrl'
            Wor = name + '_Wor_grp'
            Loc = name + '_Loc_grp'
            foot = 'foot' + side + '_IK_ctrl'
            hand = 'hand' + side + '_IK_ctrl'
            
            Gim = name + '_Gimbal_ctrl'
            pCons = '_parentConstraint1'
            oCons = '_orientConstraint1'
            
            #--------------------------------------# 
           
            # Between ARM LEG
            if n == 0:
                handle = 'hand' + side + '_IK_handle'
                ikCtrl = hand
            if n == 1:
                handle = 'foot' + side + '_IK_handle'
                ikCtrl = foot
            
            # Create
            mc.group( em = True, n = Wor)
            mc.group( em = True, n = Loc)
            
            # Position
            mc.parentConstraint( jnt, grp, w = 1, mo = 0)
            mc.parentConstraint( jnt, Wor, w = 1, mo = 0)
            mc.parentConstraint( jnt, Loc, w = 1, mo = 0)
            mc.delete( grp + pCons )
            mc.delete( Wor + pCons )
            mc.delete( Loc + pCons )
        
            # Parent
            mc.parent( grp, Wor, Loc, ikCtrl)
        
            # LocWorld
            rev = Wor + '_rev'
            LocWorAttr = pov + '.localWorld'
            mc.parentConstraint( 'fly_ctrl', Wor, w = 1, mo = 1) # from Fly
            mc.parentConstraint( Wor, Loc, grp, w = 0, mo = 1) # to parent
            mc.createNode( 'reverse', n = rev) # createNode
            
            mc.connectAttr( LocWorAttr,rev + '.input.inputX' )
            
            mc.connectAttr( LocWorAttr, grp + pCons + '.' + Wor + 'W0')
            mc.connectAttr( rev + '.output.outputX', grp + pCons + '.' + Loc + 'W1')
            
        #------------------------------------------------------------------------#
        #Ik part name
        jntName = 'upperArm','upperLeg','foot','toes'
        
        for k in range(len(jntName)):
              
            if jntName[k] == 'upperArm':
                end = 'hand' + side
                #Give Name
                name = jntName[k] + side
                sJnt = name + '_IK_jnt'
                eJnt = end + '_IK_jnt'
                handle = end + '_IK_handle'
                IKCtrl = end + '_IK_ctrl' 
                pov = 'arm' + side + '_pov_ctrl'
                
                mc.ikHandle( n = handle, sj = sJnt, ee = eJnt, sol = 'ikRPsolver' )
                
                mc.parent (handle, IKCtrl)
                mc.poleVectorConstraint (pov, handle, w = 1)
                        
            if jntName[k] == 'upperLeg':
                end = 'foot' + side
                #Give Name
                name = jntName[k] + side
                sJnt = name + '_IK_jnt'
                eJnt = end + '_IK_jnt'
                handle = end + '_IK_handle'
                IKCtrl = 'ankle'+ side + '_IK_Zro_grp' 
                pov = 'leg' + side + '_pov_ctrl'
                
                mc.ikHandle( n = handle, sj = sJnt, ee = eJnt, sol = 'ikRPsolver' )
                
                mc.parent (handle, IKCtrl)
                mc.poleVectorConstraint (pov, handle, w = 1)
            
            
            if jntName[k] == 'foot':
                end = 'toes' + side
                #Give Name
                name = jntName[k] + side
                sJnt = name + '_IK_jnt'
                eJnt = end + '_IK_jnt'
                handle = end + '_IK_handle'
                IKCtrl = 'ballRoll'+ side + '_IK_ctrl' 
                
                mc.ikHandle( n = handle, sj = sJnt, ee = eJnt, sol = 'ikSCsolver' )
                
                mc.parent (handle, IKCtrl)
                
            if jntName[k] == 'toes':
                end = 'toesTip' + side
                #Give Name
                name = jntName[k] + side
                sJnt = name + '_IK_jnt'
                eJnt = end + '_IK_jnt'
                handle = end + '_IK_handle'
                IKCtrl = 'toesRoll' + side + '_IK_ctrl'
                
                mc.ikHandle( n = handle, sj = sJnt, ee = eJnt, sol = 'ikSCsolver' )
                
                mc.parent (handle, IKCtrl)
            
    for s in range(2): # FOR LEG IK Handle
        side = sideGive[s] # SIDE SIDE SIDE
        giveName = ('arm','leg')
        offsetName = ('hand','foot')
        for n in range(len(giveName)):
            pov = giveName[n] + side + '_pov_ctrl'
            handle = offsetName[n] + side + '_IK_handle'
            mc.poleVectorConstraint (pov, handle, w = 1)
            
#----------------------- DELETE TEMP JOINT ---------------------------#
def DeleteTmpJnt():
    mc.delete ('TMP_GRP')
    tmpAmount = len(mc.ls('*tmpJnt'))
    if tmpAmount > 0:
        tmp = mc.select('*tmpJnt')
        mc.delete()
        print 'Temp Joint Clear'
    else :
        print '>>> Temp Joint Is No More <<<'
#----------------------- IK Tune ---------------------------#
def HideIK_handle(): # and orientConstraint
    IK_name = mc.ls('*IK_handle')
    for i in range(len(mc.ls('*IK_handle'))):
        mc.setAttr(IK_name[i]+'.v', 0)
        
    # Parrent Constrain To Null Grp
    nameList = 'hips','shoulderLFT','shoulderRGT'
    for i in range (len (nameList)):
        # Naming
        name = nameList[i]
        jnt = name + '_bind_jnt'
        null = name + '_null_grp'
        # Command
        mc.parentConstraint( jnt, null,w = 1,mo = 1 )
        
    mc.orientConstraint ('handLFT_IK_ctrl', 'handLFT_IK_jnt', w = 1, mo = 1)
    mc.orientConstraint ('handRGT_IK_ctrl', 'handRGT_IK_jnt', w = 1, mo = 1)

#----------------------- RIG Snap ---------------------------#    
def Snap(): # Rig Snap Grp
    master = 'handRig_Snap_grp'
    mc.group( em=True, n=master)
    mc.parent(master, 'fly_ctrl')
    # LOCK and HIDE
    attrName = '.tx','.ty','.tz','.rx','.ry','.rz','.sx','.sy','.sz'
    for a in range(len(attrName)):
        attr = attrName[a]
        name = master
        mc.setAttr( name + attr, lock = True, keyable = False, channelBox = False)

    sideGive = 'LFT','RGT'
    for s in range(2):
        side = sideGive[s]
        nameGive = 'upperArm','lowerArm' ,'hand'
        for i in range(len(nameGive)): #LOCAL grp
            name = nameGive[i] + side
            jnt = name + '_bind_jnt'
            jntIK = name + '_IK_jnt'
            FK = name + '_FK_ctrl'
            IK = name + '_IK_ctrl'
            Loc = name + '_Loc_grp'
            pCons = '_parentConstraint1'
            
            # Setup
            mc.group( em = True, n=Loc)
            mc.parentConstraint( jnt, Loc, w=1, mo=0)
            mc.delete( Loc + pCons)
            mc.parent( Loc, master)
            # pCons From IK
            mc.parentConstraint( jntIK, Loc, w=1, mo=1)
            
        # WORLD grp
        name = 'hand' + side
        jnt = name + '_bind_jnt'
        IK = name + '_IK_ctrl'
        Wor = name + '_Wor_grp'
        Gimbal = name + '_FK_ctrl'
        pCons = '_parentConstraint1'
        mc.group( em = True, n=Wor)
        mc.parentConstraint( IK, Wor, w=1, mo=0)
        mc.delete( Wor + pCons)
        mc.parent( Wor, master)
        # pCons From IK
        mc.parentConstraint( Gimbal, Wor, w=1, mo=1)    
        
def ExtraCtrl():
    #Extra Rig
    #--------------------CAPE--------------------#
    giveSide = 'LFT','RGT'
    for s in range(2):
        side = giveSide[s]
        for i in range(4):
            name = 'cape%02d'%(i+1) + side
            jnt = name + '_bind_jnt'
            grp = name + '_Zro_grp'
            ctrl = name + '_ctrl'
            pCon = '_parentConstraint1'
            mc.parentConstraint(jnt,grp,mo=0,w=1)
            mc.delete(grp+pCon)
            #Action
            mc.parentConstraint(ctrl,jnt,mo=1,w=1)
        for i in range(4,1,-1):
            grp = 'cape%02d'%(i) + side + '_Zro_grp'
            upCtrl = 'cape%02d'%(i-1) + side + '_ctrl'
            mc.parent(grp,upCtrl)
        mc.parent('cape01' + side + '_Zro_grp','upperChest_Gimbal_ctrl')
    #-------------------Skirt03Back--------------------#
    for i in range(3):
        name = 'skirtBack%02d'%(i+1)
        jnt = name + '_bind_jnt'
        grp = name + '_Zro_grp'
        ctrl = name + '_ctrl'
        pCon = '_parentConstraint1'
        mc.parentConstraint(jnt,grp,mo=0,w=1)
        mc.delete(grp+pCon)
        #Action
        mc.parentConstraint(ctrl,jnt,mo=1,w=1)
    for i in range(3,1,-1): 
        grp = 'skirtBack%02d'%(i) + '_Zro_grp'
        upCtrl = 'skirtBack%02d'%(i-1) + '_ctrl'
        mc.parent(grp,upCtrl)
    mc.parent('skirtBack01_Zro_grp','spine_Gimbal_ctrl')    
    #-------------------scarf06 Joint--------------------#
    # Large Scarf Ctrl
    mc.parentConstraint('neck_bind_jnt','scarf_Zro_grp',mo=0,w=1)
    mc.delete('scarf_Zro_grp' + '_parentConstraint1')
    # Small Scarf Ctrl
    for i in range(6):
        name = 'scarf%02d'%(i+1)
        jnt = name + '_bind_jnt'
        grp = name + '_Zro_grp'
        ctrl = name + '_ctrl'
        pCon = '_parentConstraint1'
        mc.parentConstraint(jnt,grp,mo=0,w=1)
        mc.delete(grp+pCon)
        #Action
        mc.parentConstraint(ctrl,jnt,mo=1,w=1)
        mc.parent(grp,'scarf_ctrl')
    mc.parent('scarf_Zro_grp','upperChest_Gimbal_ctrl')

    
def ArmorCtrl(): #Armour Zone
    giveSide = 'LFT','RGT'
    mc.parent('armor_grp','fly_ctrl')
    for s in range(2):
        side = giveSide[s]
        giveArmor = 'upperArm','upperLeg','lowerLeg'
        for i in range(len(giveArmor)):
            name = giveArmor[i] + 'Armor' + side
            upJnt = 'shoulder' + side
            jnt = name + '_bind_jnt'
            bone = giveArmor[i] + side + '_bind_jnt'
            grp = name + '_Zro_grp'
            ctrl = name + '_ctrl'
            pCon = '_parentConstraint1'
            mc.parentConstraint(jnt,grp,mo=0,w=1)
            mc.delete(grp+pCon)
            #Action
            mc.parentConstraint(ctrl,jnt,mo=1,w=1)
            mc.scaleConstraint(ctrl,jnt,mo=1,w=1)
            
            mc.parent(grp,'armor_grp')
    
            if giveArmor[i] == 'upperArm':
                mc.parentConstraint(upJnt + '_bind_jnt',grp,mo=1,w=1)
            else:
                mc.parentConstraint(bone,grp,mo=1,w=1)


MainCtrl()
SideCtrl()
FingerCtrl()
ExtraCtrl()
ArmorCtrl()
IKJoint()
IKSystem()
IKhandle()
DeleteTmpJnt()
BlendIK()
HideIK_handle()
Snap()
mc.select(cl=True)