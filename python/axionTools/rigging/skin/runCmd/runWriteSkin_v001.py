from axionTools.rigging.skin.version  import readWriteSkin_v001 as utils
reload(utils)

from axionTools.pipeline import fileTools as fileTools 
reload(fileTools)


def _processPath():
	filePath  = fileTools.desinatePath('\\RIG\\data\\skinData')
	fileTools.currentFilePath(filePath)
	return filePath



def exportWeightData():
	geoData = utils.geoInfo(vtx=True, geo=True, skinC=True)

	vertexList 			= 		geoData[0]
	selGEO 				= 		geoData[1]
	skinClusterNam 		= 		geoData[2]

	vertexDict = utils.getVertexWeights( vertexList , skinClusterNam )
	dirPath = _processPath()

	filePath  = dirPath + '\\%sData.json' %skinClusterNam

	utils.writeJsonFile(vertexDict, filePath)

def importWeightData():
	selectGeoData = utils.geoInfo(shape = True, skinC = True)
	geoName = selectGeoData[0]
	skinClusterNM = selectGeoData[1]
	utils.readJsonFile(filePath)

