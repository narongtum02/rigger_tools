
from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)


# for eyelid up and lo 


	# ============
	# - eyeBall roll up down -
	# ============


part = 'mouth'
side = 'Mid'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
baseBsh = 'mounthMidAll_bsh'
amp = 1
behv = 'Up'
attrName = 'translateY'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mounthUp_bsh'  , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mounthDn_bsh' , 
						positive = False , amp = amp )





behv = 'TurnR'
attrName = 'translateX'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mounthLFT_bsh'  , 
						positive = True , amp = amp )
behv = 'TurnL'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mounthRGT_bsh' , 
						positive = False , amp = amp )






behv = 'TurnR'
attrName = 'translateX'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mounthLFT_bsh'  , 
						positive = True , amp = amp )
behv = 'TurnL'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mounthRGT_bsh' , 
						positive = False , amp = amp )






	# ============
	# - eyeBall roll in out -
	# ============

attrName = 'rotateZ'
amp = 0.1

behv = 'FC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv)  , 
						positive = False , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv) , 
						positive = True , amp = amp )








	# ============
	# - eyeBall roll in out -
	# ============

attrName = 'rotateZ'
amp = 0.1

behv = 'FC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv)  , 
						positive = False , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv) , 
						positive = True , amp = amp )






# ============
	# - add cnr UD -
	# ============

ctrl.addAttribute( longName = 'upLip_mid_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'loLip_mid_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mounth_curl_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mounth_clench_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mounth_pull_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mounth_U_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )







	# ============
	# - eyeBall roll in out -
	# ============

behv = 'MidUp'
part = 'upLip'
attrName = 'upLip_mid_UD'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'MidDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )















	# ============
	# - eyeBall roll in out -
	# ============

behv = 'MidUp'
part = 'loLip'
attrName = 'loLip_mid_UD'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'MidDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )





	# ============
	# - eyeBall roll in out -
	# ============

behv = 'CurlInAll'
part = 'mounth'
attrName = 'mounth_curl_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'CurlOutAll'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )




	# ============
	# - eyeBall roll in out -
	# ============

behv = 'ClenchIn'
part = 'mounth'
attrName = 'mounth_clench_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'ClenchOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )







	# ============
	# - eyeBall roll in out -
	# ============

behv = 'PullIn'
part = 'mounth'
attrName = 'mounth_pull_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'PullOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )









	# ============
	# - eyeBall roll in out -
	# ============

behv = 'PullIn'
part = 'mounth'
attrName = 'mounth_U_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'PullOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )











ctrl.lockHideAttrLst('tz','rx','ry','sx','sy','sz','v')



