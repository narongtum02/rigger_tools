	# ============
	# - nose -
	# ============


part = 'nose'

behv = 'Sq'
attrName = 'translateY'
baseBsh = 'noseAll_bsh'

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = 'noseBsh_ctrl' , part = part, behv = behv,
						attr = attrName, side = '', 
						bshBase = baseBsh , bshMember = 'noseSq_bsh'  , 
						positive = True , amp = amp )


behv = 'St'
attrName = 'translateY'
baseBsh = 'noseAll_bsh'

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = 'noseBsh_ctrl' , part = part, behv = behv,
						attr = attrName, side = '', 
						bshBase = baseBsh , bshMember = 'noseSt_bsh'  , 
						positive = False , amp = amp )



	# ============
	# - another controller for manipulate wing nose -
	# ============

behv = 'Up'
side = 'LFT'
attrName = 'translateY'
baseBsh = 'noseAll%s_bsh' %side
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )


behv = 'Dn'
attrName = 'translateY'
baseBsh = 'noseAll%s_bsh' %side

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = amp )





behv = 'FC'
attrName = 'translateX'
baseBsh = 'noseAll%s_bsh' %side
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )


behv = 'CC'
attrName = 'translateX'
baseBsh = 'noseAll%s_bsh' %side

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = amp )



ctrl.lockHideAttrLst('tz','rx','ry','rz','sx','sy','sz','v')