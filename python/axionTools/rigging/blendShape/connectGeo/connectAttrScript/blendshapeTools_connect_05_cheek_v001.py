
from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)


# for eyelid up and lo 


	# ============
	# - Cheek -
	# ============


part = 'cheek'
side = 'LFT'
# make connect to template controller
ctrl = core.Dag( 'cheekBsh%s_ctrl' %side	)
baseBsh = 'cheekAll%s_bsh' %side
amp = 1

behv = 'LoOut'
attrName = 'translateZ'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = True , amp = amp )
behv = 'LoIn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )












	# ============
	# - eyeBall roll up down -
	# ============


part = 'cheek'

# make connect to template controller
ctrl = core.Dag( 'cheekBsh%s_ctrl' %side	)
baseBsh = 'cheekAll%s_bsh' %side
amp = 1
attrName = 'translateY'
behv = 'UpOut'


if side == 'LFT':
	logicA = 'False'
	logicB = 'True'

elif side == 'RGT':
	logicA = 'True'
	logicB = 'False'

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = logicA , amp = amp )
behv = 'UpIn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = logicB , amp = amp )









	# ============
	# - eyeBall roll up down -
	# ============


part = 'puff'

# make connect to template controller
ctrl = core.Dag( 'cheekBsh%s_ctrl' %side	)
baseBsh = 'cheekAll%s_bsh' %side
amp = 1
attrName = 'translateX'
behv = 'Out'

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )
behv = 'In'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = True , amp = amp )







ctrl.lockHideAttrLst('rx','ry','rz','sx','sy','sz','v')
