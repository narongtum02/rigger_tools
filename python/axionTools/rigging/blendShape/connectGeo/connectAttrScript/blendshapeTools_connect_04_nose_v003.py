	# ============
	# - nose -
	# ============

from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)



# ============
# - all nose this should run once -
# ============


amp = 1
part = 'nose'


behv = 'Sq'
attrName = 'translateY'
baseBsh = 'noseAll_bsh'
side = 'LFT'
ctrl = core.Dag( '%sBsh_ctrl' %part	)

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = 'noseBsh_ctrl' , part = part, behv = behv,
						attr = attrName, side = '', 
						bshBase = baseBsh , bshMember = 'noseSq_bsh'  , 
						positive = False , amp = amp )


behv = 'St'
attrName = 'translateY'
baseBsh = 'noseAll_bsh'

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = 'noseBsh_ctrl' , part = part, behv = behv,
						attr = attrName, side = '', 
						bshBase = baseBsh , bshMember = 'noseSt_bsh'  , 
						positive = True , amp = amp )

# ============
# - adding nose slide left right -
# ============


behv = 'LFT'
attrName = 'translateX'
baseBsh = 'noseAll_bsh'

print '%s %s all going up' %(behv,part)
bshTools.connectCtrlToBsh( 		ctrlNam = 'noseBsh_ctrl' , part = part, behv = behv,
						attr = attrName, side = '', 
						bshBase = baseBsh , bshMember = 'noseLFT_bsh'  , 
						positive = True , amp = amp )


behv = 'RGT'
attrName = 'translateX'
baseBsh = 'noseAll_bsh'

print '%s %s all going up' %(behv,part)
bshTools.connectCtrlToBsh( 		ctrlNam = 'noseBsh_ctrl' , part = part, behv = behv,
						attr = attrName, side = '', 
						bshBase = baseBsh , bshMember = 'noseRGT_bsh'  , 
						positive = False , amp = amp )



ctrl.lockHideAttrLst('tz','rx','ry','rz','sx','sy','sz','v')





# ============
# - all nose this should run twice -
# ============





# ============
# - another controller for manipulate wing nose -
# ============
side = 'RGT'
behv = 'Up'

attrName = 'translateY'
baseBsh = 'noseAll%s_bsh' %side
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )


behv = 'Dn'
attrName = 'translateY'
baseBsh = 'noseAll%s_bsh' %side

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = amp )





behv = 'FC'
attrName = 'translateX'
baseBsh = 'noseAll%s_bsh' %side
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )


behv = 'CC'
attrName = 'translateX'
baseBsh = 'noseAll%s_bsh' %side

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = amp )



ctrl.lockHideAttrLst('tz','rx','ry','rz','sx','sy','sz','v')