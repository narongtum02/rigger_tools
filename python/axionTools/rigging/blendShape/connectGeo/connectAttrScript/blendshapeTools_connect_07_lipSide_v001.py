
from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)


# for eyelid up and lo 


	# ============
	# - lip or mouth roll up down -
	# ============


part = 'lip'
side = 'RGT'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
baseBsh = 'lipAllRGT_bsh'
amp = 1

behv = 'Up'
attrName = 'translateY'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mouthCnr%s%s_bsh' %(behv,side) , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mouthCnr%s%s_bsh' %(behv,side) , 
						positive = False , amp = amp )









	# ============
	# - lip or mouth roll up down -
	# ============


part = 'lip'
side = 'RGT'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
baseBsh = 'lipAllRGT_bsh'
amp = 1

behv = 'Out'
attrName = 'translateX'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mouthCnr%s%s_bsh' %(behv,side) , 
						positive = True , amp = amp )
behv = 'In'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mouthCnr%s%s_bsh' %(behv,side) , 
						positive = False , amp = amp )






	# ============
	# - lip or mouth roll up down -
	# ============


part = 'lip'
side = 'RGT'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
baseBsh = 'lipAllRGT_bsh'
amp = 1

behv = 'Out'
attrName = 'translateZ'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'lipPart%s%s_bsh' %(behv,side) , 
						positive = True , amp = amp )
behv = 'In'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'lipPart%s%s_bsh' %(behv,side) , 
						positive = False , amp = amp )




# ============
	# - add cnr UD -
	# ============

ctrl.addAttribute( longName = 'upLip_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'loLip_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )




part = 'upLip'
side = 'RGT'
# make connect to template controller
ctrl = core.Dag( 'lipBshRGT_ctrl'	)
baseBsh = 'lipAllRGT_bsh'
amp = 1

behv = 'Up'
attrName = 'upLip_UD'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )






part = 'loLip'
side = 'RGT'
# make connect to template controller
baseBsh = 'lipAllRGT_bsh'
amp = 1

behv = 'Up'
attrName = 'loLip_UD'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )





ctrl.lockHideAttrLst('rx','ry','rz','sx','sy','sz','v')



