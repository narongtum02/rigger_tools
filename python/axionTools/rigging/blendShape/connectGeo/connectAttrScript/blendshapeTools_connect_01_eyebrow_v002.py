from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)

from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )


# use this function to run connection between  

# [controller] >> [blendshape]
	# ========================
	# - Connect Value to controller -
	# ========================



# import ctrl and adjust proper prosition
# run this for connect value (sample)





print '''\n
	# ========================
	# - Connect value to controller -
	# ========================
	'''





part = 'eyebrow'
side = 'LFT'



if side == 'RGT':
	logicA = False
	logicB = True
elif side == 'LFT':
	logicA = False
	logicB = True



# make connect to template controller
ctrl = core.Dag( 'eyebrowBsh%s_ctrl' %side)

ctrl.addAttribute( longName = 'inner_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mid_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'outer_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.lockHideAttrLst('rx','ry','rz','sx','sy','sz','v')






	# ============
	# - Eyebrow -
	# ============


part = 'eyebrow'
eyebrowBaseBsh = 'eyebrowAll%s_bsh' %side

behv = 'AllUp'
attrName = 'eyebrow_UD'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = 'translateY', side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = 0.75 )


behv = 'AllDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = 'translateY',side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = 0.75 )




# reverse with side
behv = 'AllIn'
attrName = 'eyebrow_IO'
print '%s eyebrow all rolling up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = 'AllIn',
						attr = 'translateX', side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = logicA , amp = 0.75 )


behv = 'AllOut'
print '%s eyebrow all rolling up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = 'eyebrow', behv = behv,
						attr = 'translateX',side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = logicB , amp = 0.75 )



if side == 'RGT':

	# reverse with side
	behv = 'AllPull'
	attrName = 'All_PP'
	bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
							attr = 'translateZ', side = side, 
							bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
							positive = False , amp = 1 )
	behv = 'AllPush'
	bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
							attr = 'translateZ',side = side, 
							bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
							positive = True , amp = 1 )


else:

	# reverse with side
	behv = 'AllPull'
	attrName = 'All_PP'
	bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
							attr = 'translateZ', side = side, 
							bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
							positive = True , amp = 1 )
	behv = 'AllPush'
	bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
							attr = 'translateZ',side = side, 
							bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
							positive = False , amp = 1 )








behv = 'InnUp'
attrName = 'inner_UD'
print '%s eyebrow all rolling up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv ,
						attr = attrName , side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = 0.1 )
behv = 'InnDn'
print '%s eyebrow all rolling up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = 0.1 )






behv = 'MidUp'
attrName = 'mid_UD'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = 0.1 )

behv = 'MidDn'
print '%s eyebrow all going down' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = 0.1 )
print 'outer eyebrow all going up'







behv = 'OutUp'
attrName = 'outer_UD'
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = 0.1 )
print 'outer all going up'
behv = 'OutDn'
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = 0.1 )







print 'end of %s part'%part
