'''
03 eye lid part
'''

from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)



	# ============
	# - close eye -
	# ============


	# ============
	# - up Lid UD -
	# ============


part = 'upLid'
side = 'RGT'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)

eyebrowBaseBsh = 'eyeLidUpAll%s_bsh' %side
amp = 1
behv = 'Up'
attrName = 'translateY'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = eyebrowBaseBsh , bshMember = 'upLidUp%s_bsh' %side  , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )


behv = 'FC'
attrName = 'translateX'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )



ctrl.addAttribute( longName = 'inner_UD' ,min = 0 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mid_UD' ,min = 0 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'outer_UD' ,min = 0 , max = 10, defaultValue = 0 , keyable = True )

ctrl.lockHideAttrLst('tz','rx','ry','rz','sx','sy','sz','v')





	# ============
	# - insert inn mid outer -
	# ============


# behv = 'InnDn'
# attrName = 'inner_UD'

#behv = 'MidDn'
#attrName = 'mid_UD'


#behv = 'OutDn'
#attrName = 'outer_UD'


eyebrowBaseBsh = 'eyeLidUpAll%s_bsh' %side
behv_Lst = [ 'InnDn' , 'MidDn' , 'OutDn' ]
attrName_Lst = ['inner_UD' , 'mid_UD' , 'outer_UD' ]

for i in range(len(behv_Lst)):
	print behv_Lst[i]
	print attrName_Lst[i]

	print '%s eyebrow all going up' %behv
	bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name, part = part, behv = behv_Lst[i],
							attr = attrName_Lst[i], side = side, 
							bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv_Lst[i],side)  , 
							positive = True , amp = amp )










	# ============
	# - lo Lid UD -
	# ============

part = 'loLid'
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
eyebrowBaseBsh = 'eyeLidLoAll%s_bsh' %side
behv = 'Up'
attrName = 'translateY'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )


attrName = 'translateX'
behv = 'FC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )


ctrl.addAttribute( longName = 'inner_UD' ,min = 0 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mid_UD' ,min = 0 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'outer_UD' ,min = 0 , max = 10, defaultValue = 0 , keyable = True )

ctrl.lockHideAttrLst('tz','rx','ry','rz','sx','sy','sz','v')





	# ============
	# - insert inn mid outer For loLid
	# ============


#behv = 'InnUp'
#attrName = 'inner_UD'

#behv = 'MidUp'
#attrName = 'mid_UD'


behv = 'OutUp'
attrName = 'outer_UD'





eyebrowBaseBsh = 'eyeLidLoAll%s_bsh' %side
behv_Lst = [ 'InnUp' , 'MidUp' , 'OutUp' ]
attrName_Lst = ['inner_UD' , 'mid_UD' , 'outer_UD' ]

for i in range(len(behv_Lst)):
	print behv_Lst[i]
	print attrName_Lst[i]

	print '%s eyebrow all going up' %behv
	bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name, part = part, behv = behv_Lst[i],
							attr = attrName_Lst[i], side = side, 
							bshBase = eyebrowBaseBsh , bshMember = '%s%s%s_bsh' %(part,behv_Lst[i],side)  , 
							positive = True , amp = amp )