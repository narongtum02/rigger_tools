
from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)


# for eyelid up and lo 


	# ============
	# - eyeBall roll up down -
	# ============


part = 'eyeBall'
side = 'RGT'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
baseBsh = 'eyeBallAll%s_bsh' %side
amp = 1
behv = 'Up'
attrName = 'translateY'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )





	# ============
	# - eyeBall roll in out -
	# ============

attrName = 'translateX'

behv = 'In'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'Out'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )







	# ============
	# - eyeBall roll in out -
	# ============

attrName = 'rotateZ'
amp = 0.01
behv = 'FC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = True , amp = amp )





	# ============
	# - add cnr UD -
	# ============

ctrl.addAttribute( longName = 'cnr_inner_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'cnr_outer_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )



behv = 'InnUp'
part = 'eyeCnr'
attrName = 'cnr_inner_UD'
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'InnDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )



	# ============
	# - add cnr Out UD -
	# ============

behv = 'OutUp'
part = 'eyeCnr'
attrName = 'cnr_outer_UD'
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'OutDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )





ctrl.lockHideAttrLst('tz','rx','ry','rz','sx','sy','sz','v')



