
from axionTools.rigging.autoRig.base import core
reload(core)

from axionTools.rigging.autoRig.base import rigTools
reload( rigTools )

from axionTools.rigging.blendShape import blendshapeTools as bshTools
reload(bshTools)


# for eyelid up and lo 


	# ============
	# - mouth roll up down -
	# ============


part = 'mouth'
side = 'Mid'
# make connect to template controller
ctrl = core.Dag( '%sBsh%s_ctrl' %(part,side)	)
baseBsh = 'mouthMidAll_bsh'
amp = 1
behv = 'Up'
attrName = 'translateY'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mouthUp_bsh'  , 
						positive = True , amp = amp )
behv = 'Dn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mouthDn_bsh' , 
						positive = False , amp = amp )





behv = 'TurnR'
attrName = 'translateX'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mouthLFT_bsh'  , 
						positive = True , amp = amp )
behv = 'TurnL'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mouthRGT_bsh' , 
						positive = False , amp = amp )




'''

behv = 'TurnR'
attrName = 'translateX'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = 'mouthLFT_bsh'  , 
						positive = True , amp = amp )
behv = 'TurnL'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = 'mouthRGT_bsh' , 
						positive = False , amp = amp )


'''



	# ============
	# - mouth roll in out -
	# ============

attrName = 'rotateZ'
amp = 0.1

behv = 'FC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv)  , 
						positive = False , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv) , 
						positive = True , amp = amp )





'''


	# ============
	# - mouth roll in out -
	# ============

attrName = 'rotateZ'
amp = 0.1

behv = 'FC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv)  , 
						positive = False , amp = amp )
behv = 'CC'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s_bsh' %(part,behv) , 
						positive = True , amp = amp )


'''



# ============
	# - add cnr UD -
	# ============

ctrl.addAttribute( longName = 'upLip_mid_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'loLip_mid_UD' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mouth_curl_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mouth_clench_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mouth_pull_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mouth_U_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )
ctrl.addAttribute( longName = 'mouth_AU_IO' ,min = -10 , max = 10, defaultValue = 0 , keyable = True )






	# ============
	# - mouth roll in out -
	# ============

behv = 'MidUp'
part = 'upLip'
attrName = 'upLip_mid_UD'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'MidDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )















	# ============
	# - mouth roll in out -
	# ============

behv = 'MidUp'
part = 'loLip'
attrName = 'loLip_mid_UD'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'MidDn'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )





	# ============
	# - mouth roll in out -
	# ============

behv = 'CurlIn'
part = 'mouth'
attrName = 'mouth_curl_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'CurlOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )


	# ============
	# - mouth roll in out -
	# ============

behv = 'ClenchIn'
part = 'mouth'
attrName = 'mouth_clench_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'ClenchOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )







	# ============
	# - mouth roll in out -
	# ============

behv = 'PullIn'
part = 'mouth'
# attrName = 'mouth_pull_IO'
attrName = 'translateZ'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = False , amp = amp )
behv = 'PullOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = True , amp = amp )









	# ============
	# - mouth U
	# ============

behv = 'UIn'
part = 'mouth'
attrName = 'mouth_U_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'UOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )




	# ============
	# - mouth AU
	# ============


behv = 'AUIn'
part = 'mouth'
attrName = 'mouth_AU_IO'
side = ''
amp = 1

print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName, side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side)  , 
						positive = True , amp = amp )
behv = 'AUOut'
print '%s eyebrow all going up' %behv
bshTools.connectCtrlToBsh( 		ctrlNam = ctrl.name , part = part, behv = behv,
						attr = attrName,side = side, 
						bshBase = baseBsh , bshMember = '%s%s%s_bsh' %(part,behv,side) , 
						positive = False , amp = amp )





ctrl.lockHideAttrLst('rx','ry','sx','sy','sz','v')



