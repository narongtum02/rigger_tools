import maya.cmds as mc



 # D:\myJob\all\asset\2563\Q1\Aisha_script



			
facial_dict =   {   'facialRegion'  :   'facial'    ,
						'posiBlock' : (1,1)         ,
						
						'name':[	'eyeBlinkLeft',
									 'eyeBlinkLeft_btw',
									 'eyeLookDownLeft',
									 'eyeLookInLeft',
									 'eyeLookOutLeft',
									 'eyeLookUpLeft',
									 'eyeSquintLeft',
									 'eyeWideLeft',
									 'eyeBlinkRight',
									 'eyeBlinkRight_btw',
									 'eyeLookDownRight',
									 'eyeLookInRight',
									 'eyeLookOutRight',
									 'eyeLookUpRight',
									 'eyeSquintRight',
									 'eyeWideRight',
									 'jawForward',
									 'jawLeft',
									 'jawRight',
									 'jawOpen',
									 'mouthClose',
									 'mouthFunnel',
									 'mouthPucker',
									 'mouthRight',
									 'mouthLeft',
									 'mouthSmileLeft',
									 'mouthSmileRight',
									 'mouthFrownRight',
									 'mouthFrownLeft',
									 'mouthDimpleLeft',
									 'mouthDimpleRight',
									 'mouthStretchLeft',
									 'mouthStretchRight',
									 'mouthRollLower',
									 'mouthRollUpper',
									 'mouthShrugLower',
									 'mouthShrugUpper',
									 'mouthPressLeft',
									 'mouthPressRight',
									 'mouthLowerDownLeft',
									 'mouthLowerDownRight',
									 'mouthUpperUpLeft',
									 'mouthUpperUpRight',
									 'browDownLeft',
									 'browDownRight',
									 'browInnerUp',
									 'browOuterUpLeft',
									 'browOuterUpRight',
									 'cheekPuff',
									 'cheekSquintLeft',
									 'cheekSquintRight',
									 'noseSneerLeft',
									 'noseSneerRight',
									 'tongueOut']                  }






'''				
facial_dict =   {   'facialRegion'  :   'facial'    ,
						'posiBlock' : (1,1)         ,
						
						'name':[	u'eyeLookDownLeft',
									 u'eyeLookInLeft',
									 u'eyeLookOutLeft',
									 u'eyeLookUpLeft',
									 u'eyeLookDownRight']                  }									 
'''

allBshLst = facial_dict['name']




 
if mc.objExists('facialBsh_grp'):
	mc.setAttr( 'facialBsh_grp.translateX' , 0)
	 

 
for each in allBshLst:
	if mc.objExists(each):

		# Reset transfromation
		mc.setAttr( each + '.translateX' , 0)
		mc.setAttr( each + '.translateY' , 0)

		# List child
		member = mc.listRelatives( each ,children=1)    
		print (member[0])

		# Combine
		mc.polyUnite( member[0], member[1], member[2], member[3], member[4], member[5],ch = False , n='result' )
		#mc.delete(each)
		mc.rename('result' , each)
	else:
		print ('%s blendshape not found' %each)
		continue


'''
each = allBshLst[0]
# Reset transfromation
mc.setAttr( each + '.translateX' , 0)
mc.setAttr( each + '.translateY' , 0)

# List child
member = mc.listRelatives( each ,children=1)    
print member[0]

# Combine
mc.polyUnite( member[0], member[1], member[2], member[3], member[4], member[5],ch = False , n='result' )
#mc.delete(each)
mc.rename('result' , each)
'''
