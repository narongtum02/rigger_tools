# README #
This modular, multilingual, object-oriented Python script empowers you to take control of your rigging and animation workflow. Its intuitive architecture allows you to decompose intricate rigs into manageable parts, fostering rapid development and effortless customization. Leverage the power of Python's vast library ecosystem to tackle any rigging challenge, from crafting character rigs with unparalleled ease to designing custom solutions for niche artistic needs.

If you have any suggestion or comment, please contact me.


## TRUE AXION MENU
Store Maya Python and Mel Script Tools for Artist


## Repository
https://bitbucket.org/narongtum02/rigger_tools/src/develop/


### REQUIREMENTS:
These Tools have been made to work with Maya 2022, it but should be working with older and newer versions.

### INSTALL:

1. Please clone this repository to your local "D:" drive "D:\True_Axion\Tools\mayaTools"

2. Copy folder "D:\True_Axion\Tools\mayaTools\maya\scripts" to your maya version Documents , for example "C:\Users\Admin\Documents\maya\2018\scripts"

3. Start Maya. You will see the "axion menu" at the top right at the Maya main menu.

4. You can see the information of each tool can do at the helpline.

## More detail here ##
https://truegamestudio.atlassian.net/wiki/spaces/CG/pages/498073608/How+to+Install+Maya+Python+and+Mel+Script+Tools+for+Artist


